package cz.muni.fi.pa165.bands.utils;

import cz.muni.fi.pa165.common.data.dto.band.BandCreateDto;
import cz.muni.fi.pa165.common.data.dto.band.BandDetailDto;
import cz.muni.fi.pa165.common.data.dto.band.BandUpdateDto;
import cz.muni.fi.pa165.common.data.dto.enums.MusicStyle;
import org.springframework.stereotype.Component;

import static cz.muni.fi.pa165.bands.utils.BandDataFactory.TEST_BAND_ID;
import static cz.muni.fi.pa165.bands.utils.BandDataFactory.MANAGER_ID;

@Component
public class BandDtoFactory {

    public static BandCreateDto createBandCreateDto() {
        BandCreateDto dto = new BandCreateDto();
        dto.setName("Band Name");
        dto.setDescription("Test Description");
        dto.setMusicStyle(MusicStyle.POP);
        return dto;
    }

    public static BandDetailDto createBandDetailDto() {
        BandDetailDto dto = new BandDetailDto();
        dto.setId(TEST_BAND_ID);
        dto.setName("Band Name");
        dto.setManagerId(MANAGER_ID);
        dto.setDescription("Test Description");
        dto.setMusicStyle(MusicStyle.POP);
        return dto;
    }

    public static BandUpdateDto createBandUpdateDto() {
        BandUpdateDto dto = new BandUpdateDto();
        dto.setName("Updated Band Name");
        dto.setDescription("Updated Test Description");
        dto.setMusicStyle(MusicStyle.ROCK);
        return dto;
    }

    public static BandDetailDto createBandUpdatedDetailDto() {
        BandDetailDto dto = new BandDetailDto();
        dto.setId(TEST_BAND_ID);
        dto.setManagerId(MANAGER_ID);
        dto.setName("Updated Band Name");
        dto.setDescription("Updated Test Description");
        dto.setMusicStyle(MusicStyle.ROCK);
        return dto;
    }
}
