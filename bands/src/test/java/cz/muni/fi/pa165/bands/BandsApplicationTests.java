package cz.muni.fi.pa165.bands;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = BandsApplication.class)
class BandsApplicationTests {

    @Test
    void contextLoads() {
    }
}
