package cz.muni.fi.pa165.bands.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.bands.data.model.Band;
import cz.muni.fi.pa165.bands.repository.BandRepository;
import cz.muni.fi.pa165.bands.security.Context;
import cz.muni.fi.pa165.bands.utils.BandDataFactory;
import cz.muni.fi.pa165.bands.utils.BandDtoFactory;
import cz.muni.fi.pa165.common.data.dto.band.BandCreateDto;
import cz.muni.fi.pa165.common.data.dto.band.BandDetailDto;
import cz.muni.fi.pa165.common.data.dto.band.BandUpdateDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class BandControllerIT {
    public static final Band TEST_BAND = BandDataFactory.createBand();
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private BandRepository bandRepository;

    @MockBean
    private Context context;

    @BeforeEach
    public void setUp() {
        bandRepository.save(TEST_BAND);
        when(context.getLoggedInUserId()).thenReturn(TEST_BAND.getManagerId());
    }

    @Test
    public void createBand_validInput_returnsBand() throws Exception {
        BandCreateDto bandCreateDto = BandDtoFactory.createBandCreateDto();

        String response = mockMvc.perform(post("/bands")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(bandCreateDto)))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        BandDetailDto bandDetailDto = objectMapper.readValue(response, BandDetailDto.class);

        assertThat(bandDetailDto.getName()).isEqualTo(bandCreateDto.getName());
        assertThat(bandDetailDto.getDescription()).isEqualTo(bandCreateDto.getDescription());
    }

    @Test
    public void createBand_invalidInput_returnsBadRequest() throws Exception {
        mockMvc.perform(post("/bands")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void findById_bandFound_returnsBand() throws Exception {

        String result = mockMvc.perform(get("/bands/{id}", TEST_BAND.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        BandDetailDto response = objectMapper.readValue(result, BandDetailDto.class);

        assertThat(response.getId()).isEqualTo(TEST_BAND.getId());
        assertThat(response.getName()).isEqualTo(TEST_BAND.getName());
        assertThat(response.getDescription()).isEqualTo(TEST_BAND.getDescription());
    }

    @Test
    public void findById_bandNotFound_returnsNotFound() throws Exception {
        mockMvc.perform(get("/bands/{id}", "dummy-id")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void findMine_bandsFound_returnsListOfBandDetailDto() throws Exception {
        String result = mockMvc.perform(get("/bands")
                        .param("managerId", TEST_BAND.getManagerId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        BandDetailDto response = objectMapper.readValue(result, new TypeReference<List<BandDetailDto>>() {}).get(0);

        assertThat(response.getId()).isEqualTo(TEST_BAND.getId());
        assertThat(response.getName()).isEqualTo(TEST_BAND.getName());
        assertThat(response.getDescription()).isEqualTo(TEST_BAND.getDescription());
    }

    @Test
    public void findMine_bandsNotFound_returnsNotFound() throws Exception {
        when(context.getLoggedInUserId()).thenReturn("dummy-id");
        String response = mockMvc.perform(get("/bands")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        assertThat(response).isEqualTo("[]");
    }

    @Test
    public void updateBand_validInput_returnsBand() throws Exception {
        BandUpdateDto bandUpdateDto = BandDtoFactory.createBandUpdateDto();

        String response = mockMvc.perform(put("/bands/{id}", TEST_BAND.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(bandUpdateDto)))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        BandDetailDto bandDetailDto = objectMapper.readValue(response, BandDetailDto.class);

        assertThat(bandDetailDto.getName()).isEqualTo(bandUpdateDto.getName());
        assertThat(bandDetailDto.getDescription()).isEqualTo(bandUpdateDto.getDescription());
    }

    @Test
    public void updateBand_invalidInput_returnsBadRequest() throws Exception {
        mockMvc.perform(put("/bands/{id}", TEST_BAND.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void deleteBand_bandFound_returnsNoContent() throws Exception {
        mockMvc.perform(delete("/bands/{id}", TEST_BAND.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void deleteBand_bandNotFound_returnsNotFound() throws Exception {
        mockMvc.perform(delete("/bands/{id}", "dummy-id")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}