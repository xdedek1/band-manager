package cz.muni.fi.pa165.bands.service;

import cz.muni.fi.pa165.bands.data.model.Band;
import cz.muni.fi.pa165.bands.repository.BandRepository;
import cz.muni.fi.pa165.bands.security.Context;
import cz.muni.fi.pa165.bands.utils.BandDataFactory;
import cz.muni.fi.pa165.common.exception.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static cz.muni.fi.pa165.bands.utils.BandDataFactory.MANAGER_ID;
import static cz.muni.fi.pa165.bands.utils.BandDataFactory.TEST_BAND_ID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class BandServiceTest {

    private final Band TEST_BAND = BandDataFactory.createBand();

    @Mock
    private BandRepository bandRepository;

    @Mock
    private Context context;

    @InjectMocks
    private BandService bandService;

    @Test
    public void findById_existingId_returnsBand() {
        // Arrange
        Band mockBand = TEST_BAND;
        mockBand.setId(TEST_BAND_ID);
        when(bandRepository.findById(TEST_BAND_ID)).thenReturn(Optional.of(mockBand));

        // Act
        Band foundBand = bandService.findById(TEST_BAND_ID);

        // Assert
        assertNotNull(foundBand);
        assertThat(foundBand).isEqualTo(TEST_BAND);
    }

    @Test
    public void findById_nonExistingId_throwsException() {
        // Arrange
        String id = "non-existing-id";
        when(bandRepository.findById(id)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(ResourceNotFoundException.class, () -> bandService.findById(id));
    }

    @Test
    public void findMine_validManagerId_returnsListOfBands() {
        // Arrange
        List<Band> mockBands = new ArrayList<>();
        mockBands.add(new Band());
        mockBands.add(new Band());
        when(bandRepository.findByManagerId(MANAGER_ID)).thenReturn(mockBands);
        when(context.getLoggedInUserId()).thenReturn(MANAGER_ID);

        // Act
        List<Band> foundBands = bandService.findMine();

        // Assert
        assertEquals(mockBands.size(), foundBands.size());
    }

    @Test
    public void findMine_bandsNotFound_returnsEmptyList() {
        // Arrange
        List<Band> emptyList = Collections.emptyList();

        when(bandRepository.findByManagerId(MANAGER_ID)).thenReturn(emptyList);
        when(context.getLoggedInUserId()).thenReturn(MANAGER_ID);

        // Act
        List<Band> foundBands = bandService.findMine();

        // Assert
        assertThat(foundBands).isEmpty();
    }

    @Test
    public void create_validBand_returnsCreatedBand() {
        // Arrange
        Band toCreate = TEST_BAND;
        when(bandRepository.save(toCreate)).thenReturn(toCreate);

        // Act
        Band createdBand = bandService.create(toCreate);

        // Assert
        assertNotNull(createdBand);
        assertEquals(toCreate, createdBand);
    }

    @Test
    void create_loggedUserIdIsNull_returnsNull() {
        // Arrange
        when(context.getLoggedInUserId()).thenReturn(null);

        // Act
        Band createdBand = bandService.create(TEST_BAND);

        // Assert
        assertThat(createdBand).isNull();
    }

    @Test
    public void update_idExists_returnsUpdatedBand() {
        // Arrange
        Band existingBand = TEST_BAND;
        Band updatedBand = TEST_BAND;
        updatedBand.setDescription("Updated Desscription");
        when(bandRepository.findById(TEST_BAND_ID)).thenReturn(Optional.of(existingBand));
        when(bandRepository.save(updatedBand)).thenReturn(updatedBand);

        // Act
        Band result = bandService.update(TEST_BAND_ID, updatedBand);

        // Assert
        assertNotNull(result);
        assertEquals(updatedBand, result);
    }

    @Test
    public void update_idNotExists_throwsException() {
        // Arrange
        String id ="non-existing-id";
        Band toUpdate = new Band();
        when(bandRepository.findById(id)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(ResourceNotFoundException.class, () -> bandService.update(id, toUpdate));
    }

    @Test
    public void delete_idExists_deletesBand() {
        // Arrange
        Band existingBand = TEST_BAND;
        when(bandRepository.findById(TEST_BAND_ID)).thenReturn(Optional.of(existingBand));

        // Act
        bandService.delete(TEST_BAND_ID);

        // Assert
        verify(bandRepository, times(1)).delete(existingBand);
    }

    @Test
    public void delete_idNotExists_deletesBand_throwsException() {
        // Arrange
        String id = "non-existing-id";
        when(bandRepository.findById(id)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(ResourceNotFoundException.class, () -> bandService.delete(id));
    }
}
