package cz.muni.fi.pa165.bands.facade;

import cz.muni.fi.pa165.bands.data.model.Band;
import cz.muni.fi.pa165.bands.mappers.BandMapper;
import cz.muni.fi.pa165.bands.service.BandService;
import cz.muni.fi.pa165.bands.utils.BandDataFactory;
import cz.muni.fi.pa165.bands.utils.BandDtoFactory;
import cz.muni.fi.pa165.common.data.dto.band.BandCreateDto;
import cz.muni.fi.pa165.common.data.dto.band.BandDetailDto;
import cz.muni.fi.pa165.common.data.dto.band.BandUpdateDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static cz.muni.fi.pa165.bands.utils.BandDataFactory.MANAGER_ID;
import static cz.muni.fi.pa165.bands.utils.BandDataFactory.TEST_BAND_ID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class BandFacadeTest {

    private final Band TEST_BAND = BandDataFactory.createBand();
    private final BandDetailDto TEST_DETAIL_DTO = BandDtoFactory.createBandDetailDto();
    private final BandCreateDto TEST_CREATE_DTO = BandDtoFactory.createBandCreateDto();
    private final BandUpdateDto TEST_UPDATE_DTO = BandDtoFactory.createBandUpdateDto();
    private final BandDetailDto TEST_UPDATED_DETAIL_DTO = BandDtoFactory.createBandUpdatedDetailDto();

    @Mock
    private BandService bandService;

    @Mock
    private BandMapper bandMapper;

    @InjectMocks
    private BandFacade bandFacade;

    @Test
    public void findById_validId_returnsBandDetailDto() {
        // Arrange
        Band mockBand = TEST_BAND;
        BandDetailDto expectedDto = TEST_DETAIL_DTO;

        when(bandService.findById(TEST_BAND_ID)).thenReturn(mockBand);
        when(bandMapper.mapToDetailDto(mockBand)).thenReturn(expectedDto);

        // Act
        BandDetailDto result = bandFacade.findById(TEST_BAND_ID);

        // Assert
        assertNotNull(result);
        assertEquals(expectedDto.getId(), result.getId());
        assertEquals(expectedDto.getManagerId(), result.getManagerId());
        assertEquals(expectedDto.getName(), result.getName());
        assertEquals(expectedDto.getDescription(), result.getDescription());
        assertEquals(expectedDto.getMusicStyle(), result.getMusicStyle());
    }

    @Test
    public void findMine_validManagerId_returnsListOfBandDetailDto() {
        // Arrange
        List<Band> mockBands = List.of(TEST_BAND);

        when(bandService.findMine()).thenReturn(mockBands);
        when(bandMapper.mapToDetailDto(TEST_BAND)).thenReturn(TEST_DETAIL_DTO);

        // Act
        List<BandDetailDto> result = bandFacade.findMine();

        // Assert
        assertNotNull(result);
        assertThat(result).containsExactly(TEST_DETAIL_DTO);
    }

    @Test
    public void create_validCreateDto_returnsCreatedBandDetailDto() {
        // Arrange
        BandCreateDto createDto = TEST_CREATE_DTO;
        Band mockBand = TEST_BAND;
        BandDetailDto expectedDto = TEST_DETAIL_DTO;

        when(bandMapper.mapToEntity(createDto)).thenReturn(mockBand);
        when(bandService.create(mockBand)).thenReturn(mockBand);
        when(bandMapper.mapToDetailDto(mockBand)).thenReturn(expectedDto);

        // Act
        BandDetailDto result = bandFacade.create(createDto);

        // Assert
        assertNotNull(result);
        assertEquals(expectedDto.getId(), result.getId());
        assertEquals(expectedDto.getManagerId(), result.getManagerId());
        assertEquals(expectedDto.getName(), result.getName());
        assertEquals(expectedDto.getDescription(), result.getDescription());
        assertEquals(expectedDto.getMusicStyle(), result.getMusicStyle());
    }

    @Test
    public void update_validId_returnsUpdatedBandDetailDto() {
        // Arrange
        BandUpdateDto updateDto = TEST_UPDATE_DTO;
        Band mockBand = TEST_BAND;
        BandDetailDto expectedDto = TEST_UPDATED_DETAIL_DTO;

        when(bandMapper.mapToEntity(updateDto)).thenReturn(mockBand);
        when(bandService.update(TEST_BAND_ID, mockBand)).thenReturn(mockBand);
        when(bandMapper.mapToDetailDto(mockBand)).thenReturn(expectedDto);

        // Act
        BandDetailDto result = bandFacade.update(TEST_BAND_ID, updateDto);

        // Assert
        assertNotNull(result);
        assertEquals(expectedDto.getId(), result.getId());
        assertEquals(expectedDto.getManagerId(), result.getManagerId());
        assertEquals(expectedDto.getName(), result.getName());
        assertEquals(expectedDto.getDescription(), result.getDescription());
        assertEquals(expectedDto.getMusicStyle(), result.getMusicStyle());
    }

    @Test
    public void delete_validId_deletesBand() {
        // Act
        bandFacade.delete(MANAGER_ID);

        // Assert
        verify(bandService, times(1)).delete(MANAGER_ID);
    }
}
