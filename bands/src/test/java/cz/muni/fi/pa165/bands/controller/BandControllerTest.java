package cz.muni.fi.pa165.bands.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.bands.facade.BandFacade;
import cz.muni.fi.pa165.bands.utils.BandDtoFactory;
import cz.muni.fi.pa165.common.data.dto.band.BandCreateDto;
import cz.muni.fi.pa165.common.data.dto.band.BandDetailDto;
import cz.muni.fi.pa165.common.data.dto.band.BandUpdateDto;
import cz.muni.fi.pa165.common.exception.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static cz.muni.fi.pa165.bands.utils.BandDataFactory.MANAGER_ID;
import static cz.muni.fi.pa165.bands.utils.BandDataFactory.TEST_BAND_ID;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class BandControllerTest {

    private final BandDetailDto TEST_DETAIL_DTO = BandDtoFactory.createBandDetailDto();
    private final BandCreateDto TEST_CREATE_DTO = BandDtoFactory.createBandCreateDto();
    private final BandUpdateDto TEST_UPDATE_DTO = BandDtoFactory.createBandUpdateDto();
    private final BandDetailDto TEST_UPDATED_DETAIL_DTO = BandDtoFactory.createBandUpdatedDetailDto();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BandFacade bandFacade;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void findById_bandFound_returnsBandDetailDto() throws Exception {
        // Arrange
        BandDetailDto expectedDto = TEST_DETAIL_DTO;

        when(bandFacade.findById(TEST_BAND_ID)).thenReturn(expectedDto);

        // Act
        ResultActions result = mockMvc.perform(get("/bands/{id}", TEST_BAND_ID).contentType(MediaType.APPLICATION_JSON));

        // Assert
        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(expectedDto.getId()))
                .andExpect(jsonPath("$.managerId").value(expectedDto.getManagerId()))
                .andExpect(jsonPath("$.name").value(expectedDto.getName()))
                .andExpect(jsonPath("$.description").value(expectedDto.getDescription()))
                .andExpect(jsonPath("$.musicStyle").value(expectedDto.getMusicStyle().toString()));
    }

    @Test
    public void findById_bandNotFound_returnsNotFound() throws Exception {
        String nonExistingId = "non-existent-id";

        when(bandFacade.findById(nonExistingId)).thenThrow(ResourceNotFoundException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/bands/{id}", nonExistingId))
                .andExpect(status().isNotFound());
    }

    @Test
    public void findMine_bandsFound_returnsListOfBandDetailDto() throws Exception {
        // Arrange
        List<BandDetailDto> expectedDtos = List.of(TEST_DETAIL_DTO);
        when(bandFacade.findMine()).thenReturn(expectedDtos);

        // Act
        ResultActions resultActions = mockMvc.perform(get("/bands").contentType(MediaType.APPLICATION_JSON));

        // Assert
        resultActions.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(expectedDtos.get(0).getId()))
                .andExpect(jsonPath("$[0].managerId").value(expectedDtos.get(0).getManagerId()))
                .andExpect(jsonPath("$[0].name").value(expectedDtos.get(0).getName()))
                .andExpect(jsonPath("$[0].description").value(expectedDtos.get(0).getDescription()))
                .andExpect(jsonPath("$[0].musicStyle").value(expectedDtos.get(0).getMusicStyle().toString()));
    }

    @Test
    public void create_ReturnsBandDetailDto() throws Exception {
        // Arrange
        BandCreateDto createDto = TEST_CREATE_DTO;
        BandDetailDto expectedDto = TEST_DETAIL_DTO;

        when(bandFacade.create(createDto)).thenReturn(expectedDto);

        // Act
        ResultActions resultActions = mockMvc.perform(post("/bands")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{" +
                        "\"name\":\"" + createDto.getName() + "\", " +
                        "\"description\":\"" + createDto.getDescription() + "\", " +
                        "\"musicStyle\":\"" + createDto.getMusicStyle() + "\"" +
                        "}")
        );

        // Assert
        resultActions.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(expectedDto.getId()))
                .andExpect(jsonPath("$.managerId").value(expectedDto.getManagerId()))
                .andExpect(jsonPath("$.name").value(expectedDto.getName()))
                .andExpect(jsonPath("$.description").value(expectedDto.getDescription()))
                .andExpect(jsonPath("$.musicStyle").value(expectedDto.getMusicStyle().toString()));
    }

    @Test
    public void create_invalidInput_returnsBadRequest() throws Exception {
        // Arrange
        when(bandFacade.create(any()))
                .thenReturn(null);

        // Act & Assert
        mockMvc.perform(post("/bands")
                        .param("bandId", "test-band-id")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void update_validInput_returnsUpdatedBandDetailDto() throws Exception {
        // Arrange
        BandUpdateDto updateDto = TEST_UPDATE_DTO;
        BandDetailDto expectedDto = TEST_UPDATED_DETAIL_DTO;

        when(bandFacade.update(TEST_BAND_ID, updateDto)).thenReturn(expectedDto);

        // Act
        ResultActions resultActions = mockMvc.perform(put("/bands/{id}", TEST_BAND_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{" +
                        "\"name\":\"" + updateDto.getName() + "\", " +
                        "\"description\":\"" + updateDto.getDescription() + "\", " +
                        "\"musicStyle\":\"" + updateDto.getMusicStyle() + "\"" +
                        "}")
        );

        // Assert
        resultActions.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(expectedDto.getId()))
                .andExpect(jsonPath("$.managerId").value(expectedDto.getManagerId()))
                .andExpect(jsonPath("$.name").value(expectedDto.getName()))
                .andExpect(jsonPath("$.description").value(expectedDto.getDescription()))
                .andExpect(jsonPath("$.musicStyle").value(expectedDto.getMusicStyle().toString()));
    }
    @Test
    public void update_invalidInput_returnsBadRequest() throws Exception {
        // Arrange
        BandUpdateDto invalidUpdateDto = new BandUpdateDto();
        invalidUpdateDto.setName("");

        when(bandFacade.update(any(), any())).
                thenReturn(TEST_UPDATED_DETAIL_DTO);

        // Act & Assert
        mockMvc.perform(put("/bands/{id}", TEST_UPDATED_DETAIL_DTO.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(invalidUpdateDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void delete_bandFound_returnsNoContent() throws Exception {
        // Arrange
        String id = TEST_BAND_ID;

        // Act
        ResultActions resultActions = mockMvc.perform(delete("/bands/{id}", id));

        // Assert
        resultActions.andExpect(status().isNoContent());
        verify(bandFacade, times(1)).delete(id);
    }

    @Test
    public void delete_bandNotFound_returnsNotFound() throws Exception {
        // Arrange
        String nonExistingId = "invalid-id";

        doThrow(ResourceNotFoundException.class).when(bandFacade).delete(nonExistingId);

        // Act & Assert
        mockMvc.perform(delete("/bands/{id}", nonExistingId))
                .andExpect(status().isNotFound());
    }
}
