package cz.muni.fi.pa165.bands.utils;

import cz.muni.fi.pa165.bands.data.model.Band;
import cz.muni.fi.pa165.common.data.dto.enums.MusicStyle;
import org.springframework.stereotype.Component;

@Component
public class BandDataFactory {

    public static final String TEST_BAND_ID = "396f5976-13e9-45d9-8efd-1e5a990cfd9e";
    public static final String MANAGER_ID = "605d9cb0-7898-4d99-adae-17d47e82aeac";

    public static Band createBand() {
        Band band = new Band();
        band.setId("396f5976-13e9-45d9-8efd-1e5a990cfd9e");
        band.setManagerId("605d9cb0-7898-4d99-adae-17d47e82aeac");
        band.setName("Test Band");
        band.setDescription("Test Description");
        band.setMusicStyle(MusicStyle.POP);
        return band;
    }
}
