package cz.muni.fi.pa165.bands.data.model;

import cz.muni.fi.pa165.common.data.BasicEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "bandFollower")
public class BandFollower extends BasicEntity {

    @Schema(description = "The unique ID of the band", example = "5c89f390-18ea-48c7-9b4f-addab2b25d50")
    @Column(name = "band_id")
    private String bandId;

    @Schema(description = "The unique ID of the user", example = "5c89f390-18ea-48c7-9b4f-addab2b25d50")
    @Column(name = "user_id")
    private String userId;
}
