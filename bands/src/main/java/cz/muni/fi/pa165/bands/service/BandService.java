package cz.muni.fi.pa165.bands.service;

import cz.muni.fi.pa165.bands.data.model.Band;
import cz.muni.fi.pa165.bands.data.model.BandFollower;
import cz.muni.fi.pa165.bands.repository.BandFollowerRepository;
import cz.muni.fi.pa165.bands.repository.BandRepository;
import cz.muni.fi.pa165.bands.security.Context;
import cz.muni.fi.pa165.common.exception.OwnerValidationFailedException;
import cz.muni.fi.pa165.common.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BandService {

    private final BandRepository bandRepository;
    private final BandFollowerRepository bandFollowerRepository;

    private final Context context;
    
    @Autowired
    public BandService(BandRepository bandRepository, BandFollowerRepository bandFollowerRepository, Context context) {
        this.bandRepository = bandRepository;
        this.bandFollowerRepository = bandFollowerRepository;
        this.context = context;
    }

    @Transactional(readOnly = true)
    public Band findById(String id) {
        return bandRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Activity with id: " + id + " was not found."));
    }

    @Transactional(readOnly = true)
    public List<Band> findMine() {
        String managerId = context.getLoggedInUserId();
        return bandRepository.findByManagerId(managerId);
    }

    @Transactional
    public Band create(Band toCreate) {
        String userId = context.getLoggedInUserId();
        toCreate.setManagerId(userId);
        return bandRepository.save(toCreate);
    }

    @Transactional
    public Band update(String id, Band toUpdate) {
        findById(id);
        toUpdate.setId(id);
        return bandRepository.save(toUpdate);
    }

    @Transactional
    public void delete(String id) {
        Band band = findById(id);
        bandRepository.delete(band);
    }

    @Transactional(readOnly = true)
    public boolean isLoggedInUserBandManager(String bandId) {
        String userId = context.getLoggedInUserId();
        Band band = findById(bandId);
        return band.getManagerId().equals(userId);
    }

    @Transactional
    public void createBandFollower(String id) {
        bandRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Activity with id: " + id + " was not found."));
        String userId = context.getLoggedInUserId();

        BandFollower toCreate = new BandFollower();
        toCreate.setBandId(id);
        toCreate.setUserId(userId);

        bandFollowerRepository.save(toCreate);
    }

    @Transactional(readOnly = true)
    public List<BandFollower> findFollowers(String id) {
        return bandFollowerRepository.findBandFollowersByBandId(id);
    }

    @Transactional(readOnly = true)
    public List<Band> findFollowedBands(String userId) {
        List<BandFollower> followers = bandFollowerRepository.findBandsFollowedByUser(userId);
        List<String> bandIds = followers.stream()
                .map(BandFollower::getBandId)
                .collect(Collectors.toList());
        return bandRepository.findAllById(bandIds);
    }

    public void validateManager(Band band) {
        String userId = context.getLoggedInUserId();
        if (!band.getManagerId().equals(userId)) {
            throw new OwnerValidationFailedException("User is not manager of the band");
        }
    }
}
