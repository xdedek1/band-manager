package cz.muni.fi.pa165.bands.controller;

import cz.muni.fi.pa165.bands.facade.BandFacade;
import cz.muni.fi.pa165.common.data.dto.band.BandCreateDto;
import cz.muni.fi.pa165.common.data.dto.band.BandDetailDto;
import cz.muni.fi.pa165.common.data.dto.band.BandUpdateDto;
import cz.muni.fi.pa165.common.exception.OwnerValidationFailedException;
import cz.muni.fi.pa165.common.exception.ResourceNotFoundException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/bands")
public class BandController {

    private final BandFacade bandFacade;

    @Autowired
    public BandController(BandFacade bandFacade) {
        this.bandFacade = bandFacade;
    }

    @Operation(summary = "Get a band by its ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the band"),
            @ApiResponse(responseCode = "404", description = "Band not found"),
    })
    @GetMapping(path = "/{id}")
    public ResponseEntity<BandDetailDto> findById(@PathVariable("id") String id) {
        return ResponseEntity.ok(bandFacade.findById(id));
    }

    @Operation(summary = "Get all bands of a manager")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the bands"),
            @ApiResponse(responseCode = "404", description = "Bands not found"),
    })
    @GetMapping()
    public ResponseEntity<List<BandDetailDto>> findMine() {
        return ResponseEntity.ok(bandFacade.findMine());
    }

    @Operation(summary = "Create a new band")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Created the band"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
    })
    @PostMapping()
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<BandDetailDto> create(@Valid @RequestBody BandCreateDto createDto) {
        return ResponseEntity.ok(bandFacade.create(createDto));
    }

    @Operation(summary = "Update an existing band")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated the band"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
            @ApiResponse(responseCode = "404", description = "Band not found"),
    })
    @PutMapping(path = "/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<BandDetailDto> update(@PathVariable(value = "id") String id, @Valid @RequestBody BandUpdateDto updateDto) {
        return ResponseEntity.ok(bandFacade.update(id, updateDto));
    }

    @Operation(summary = "Delete a band")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Deleted the band"),
            @ApiResponse(responseCode = "404", description = "Band not found"),
    })
    @DeleteMapping(path = "/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<Void> delete(@PathVariable(value = "id") String id) {
        bandFacade.delete(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Returns if the logged in user is the manager of the band with the given ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the band"),
            @ApiResponse(responseCode = "404", description = "Band not found"),
    })
    @GetMapping(path = "/{bandId}/is-manager")
    public ResponseEntity<Boolean> isLoggedInUserBandManager(@PathVariable("bandId") String bandId) {
        return ResponseEntity.ok(bandFacade.isLoggedInUserBandManager(bandId));
    }

    @Operation(summary = "Create a new band follower")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Created the band follower"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
    })
    @PostMapping("/{id}/follow")
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<Void> createBandFollower(@PathVariable String id) {
        bandFacade.createBandFollower(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Get all followers of the band")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the bands"),
            @ApiResponse(responseCode = "404", description = "Bands not found"),
    })
    @GetMapping("/{id}/followers")
    public ResponseEntity<List<String>> findFollowers(@PathVariable String id) {
        return ResponseEntity.ok(bandFacade.findFollowers(id));
    }

    @Operation(summary = "Get all band followed by user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the bands"),
            @ApiResponse(responseCode = "404", description = "Bands not found"),
    })
    @GetMapping("/following/{userId}")
    public ResponseEntity<List<BandDetailDto>> findFollowedBands(@PathVariable String userId) {
        return ResponseEntity.ok(bandFacade.findFollowedBands(userId));
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<Void> handleResourceNotFoundException() {
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler(OwnerValidationFailedException.class)
    public ResponseEntity<Void> handleOwnerValidationFailedException() {
        return ResponseEntity.badRequest().build();
    }
}