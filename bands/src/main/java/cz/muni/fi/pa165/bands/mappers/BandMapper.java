package cz.muni.fi.pa165.bands.mappers;

import cz.muni.fi.pa165.common.data.dto.band.BandCreateDto;
import cz.muni.fi.pa165.common.data.dto.band.BandDetailDto;
import cz.muni.fi.pa165.common.data.dto.band.BandUpdateDto;
import cz.muni.fi.pa165.bands.data.model.Band;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface BandMapper {

    BandDetailDto mapToDetailDto(Band band);
    Band mapToEntity(BandCreateDto createDto);
    Band mapToEntity(BandUpdateDto updateDto);
}
