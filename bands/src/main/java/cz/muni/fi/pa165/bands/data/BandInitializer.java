package cz.muni.fi.pa165.bands.data;

import cz.muni.fi.pa165.common.data.dto.enums.MusicStyle;
import cz.muni.fi.pa165.bands.data.model.Band;
import cz.muni.fi.pa165.bands.repository.BandRepository;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class BandInitializer {

    private final BandRepository bandRepository;

    @Autowired
    public BandInitializer(BandRepository bandRepository) {
        this.bandRepository = bandRepository;
    }

    @PostConstruct
    public void insertDummyData() {
        initializedDummyBand();
    }

    private void initializedDummyBand() {
        Band band = new Band();

        band.setId("246e121e-a676-4ce2-be59-0864c074736d");
        band.setManagerId("subject-id1@pa165.cz");
        band.setName("Dummy band");
        band.setDescription("This is a dummy band");
        band.setMusicStyle(MusicStyle.DISCO);

        bandRepository.save(band);
    }
}
