package cz.muni.fi.pa165.bands;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class BandsApplication {

    public static void main(String[] args) {
        SpringApplication.run(BandsApplication.class, args);
    }

}
