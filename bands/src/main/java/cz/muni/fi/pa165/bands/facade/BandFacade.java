package cz.muni.fi.pa165.bands.facade;


import cz.muni.fi.pa165.bands.data.model.BandFollower;
import cz.muni.fi.pa165.common.data.dto.band.BandCreateDto;
import cz.muni.fi.pa165.common.data.dto.band.BandDetailDto;
import cz.muni.fi.pa165.common.data.dto.band.BandUpdateDto;
import cz.muni.fi.pa165.bands.data.model.Band;
import cz.muni.fi.pa165.bands.mappers.BandMapper;
import cz.muni.fi.pa165.bands.service.BandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class BandFacade {

    private final BandService service;
    private final BandMapper mapper;

    @Autowired
    public BandFacade(BandService bandService, BandMapper bandMapper) {
        this.service = bandService;
        this.mapper = bandMapper;
    }

    @Transactional(readOnly = true)
    public BandDetailDto findById(String id) {
        return mapper.mapToDetailDto(service.findById(id));
    }

    @Transactional(readOnly = true)
    public List<BandDetailDto> findMine() {
        return service.findMine().stream()
                .map(mapper::mapToDetailDto)
                .toList();
    }

    @Transactional
    public BandDetailDto create(BandCreateDto createDto) {
        Band toCreate = mapper.mapToEntity(createDto);
        return mapper.mapToDetailDto(service.create(toCreate));
    }

    @Transactional
    public BandDetailDto update(String id, BandUpdateDto updateDto) {
        Band toUpdate = mapper.mapToEntity(updateDto);
        return mapper.mapToDetailDto(service.update(id, toUpdate));
    }

    @Transactional
    public void createBandFollower(String bandId) {
        service.createBandFollower(bandId);
    }

    @Transactional(readOnly = true)
    public List<String> findFollowers(String id) {
        return service.findFollowers(id).stream()
                .map(BandFollower::getUserId)
                .toList();
    }

    @Transactional(readOnly = true)
    public List<BandDetailDto> findFollowedBands(String id) {
        return service.findFollowedBands(id).stream()
                .map(mapper::mapToDetailDto)
                .toList();
    }

    @Transactional
    public void delete(String id) {
        service.delete(id);
    }

    @Transactional(readOnly = true)
    public boolean isLoggedInUserBandManager(String bandId) {
        return service.isLoggedInUserBandManager(bandId);
    }
}
