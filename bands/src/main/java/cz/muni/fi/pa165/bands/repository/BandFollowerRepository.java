package cz.muni.fi.pa165.bands.repository;

import cz.muni.fi.pa165.bands.data.model.BandFollower;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BandFollowerRepository extends JpaRepository<BandFollower, String> {
    @Query("SELECT bf FROM BandFollower bf WHERE bf.bandId = :bandId")
    List<BandFollower> findBandFollowersByBandId(@Param("bandId") String bandId);

    @Query("SELECT bf FROM BandFollower bf WHERE bf.userId = :userId")
    List<BandFollower> findBandsFollowedByUser(@Param("userId") String userId);
}
