package cz.muni.fi.pa165.bands.repository;

import cz.muni.fi.pa165.bands.data.model.Band;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BandRepository extends JpaRepository<Band, String> {

    @Query("SELECT a FROM Band a WHERE a.managerId = :managerId")
    List<Band> findByManagerId(@Param("managerId") String managerId);
}
