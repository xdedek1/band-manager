package cz.muni.fi.pa165.bands.data.model;

import cz.muni.fi.pa165.common.data.dto.enums.MusicStyle;
import cz.muni.fi.pa165.common.data.BasicEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "band")
public class Band extends BasicEntity {

    @Schema(description = "The unique ID of the band manager", example = "5c89f390-18ea-48c7-9b4f-addab2b25d50")
    @Column(name = "manager_id")
    private String managerId;

    @Schema(description = "The name of the band", example = "The Beatles")
    @Column(name = "name", length = 50)
    private String name;

    @Schema(description = "The description of the band", example = "The Beatles were an English rock band formed in Liverpool in 1960.")
    @Column(name = "description", length = 200)
    private String description;

    @Schema(description = "The music style of the band", example = "ROCK")
    @Column(name = "music_style")
    @Enumerated(EnumType.STRING)
    private MusicStyle musicStyle;
}
