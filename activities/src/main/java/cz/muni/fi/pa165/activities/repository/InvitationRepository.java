package cz.muni.fi.pa165.activities.repository;

import cz.muni.fi.pa165.activities.data.model.Activity;
import cz.muni.fi.pa165.activities.data.model.Invitation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InvitationRepository extends JpaRepository<Invitation, String> {
    @Query("SELECT a FROM Invitation a WHERE a.receiverId = :receiverId")
    List<Invitation> findByReceiver(@Param("receiverId") String receiverId);
}
