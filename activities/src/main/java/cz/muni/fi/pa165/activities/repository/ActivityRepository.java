package cz.muni.fi.pa165.activities.repository;

import cz.muni.fi.pa165.activities.data.model.Activity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ActivityRepository extends JpaRepository<Activity, String> {

    @Query("SELECT a FROM Activity a WHERE a.ownerId = :ownerId")
    List<Activity> findByOwner(@Param("ownerId") String ownerId);

    @Query("SELECT a FROM Activity a WHERE a.bandId = :bandId")
    List<Activity> findByBand(@Param("bandId") String bandId);
}
