package cz.muni.fi.pa165.activities.service;

import cz.muni.fi.pa165.activities.data.model.Activity;
import cz.muni.fi.pa165.activities.repository.ActivityRepository;
import cz.muni.fi.pa165.activities.security.Context;
import cz.muni.fi.pa165.common.exception.OwnerValidationFailedException;
import cz.muni.fi.pa165.common.exception.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
public class ActivityService {

    private final ActivityRepository activityRepository;

    private final Context context;

    @Autowired
    public ActivityService(ActivityRepository activityRepository, Context context) {
        this.activityRepository = activityRepository;
        this.context = context;
    }

    @Transactional(readOnly = true)
    public Activity findById(String id) {
        return activityRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Activity with id: " + id + " was not found."));
    }

    @Transactional(readOnly = true)
    public List<Activity> findMine() {
        String userId = context.getLoggedUserId();
        return activityRepository.findByOwner(userId);
    }

    @Transactional(readOnly = true)
    public List<Activity> findByBand(String bandId) {
        return activityRepository.findByBand(bandId);
    }

    @Transactional
    public Activity create(Activity toCreate) {
        String userId = context.getLoggedUserId();
        toCreate.setOwnerId(userId);

        return activityRepository.save(toCreate);
    }

    @Transactional
    public Activity update(String id, Activity toUpdate) {
        findById(id);
        toUpdate.setId(id);

        return activityRepository.save(toUpdate);
    }

    @Transactional
    public void delete(String id) {
        Activity found = findById(id);

        activityRepository.delete(found);
    }

    public void validateOwner(Activity activity) {
        String userId = context.getLoggedUserId();
        if (!activity.getOwnerId().equals(userId)) {
            throw new OwnerValidationFailedException("User is not the owner of the activity");
        }
    }

}
