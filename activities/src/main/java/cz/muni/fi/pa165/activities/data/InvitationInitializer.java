package cz.muni.fi.pa165.activities.data;

import cz.muni.fi.pa165.activities.data.model.Activity;
import cz.muni.fi.pa165.activities.data.model.Invitation;
import cz.muni.fi.pa165.activities.repository.ActivityRepository;
import cz.muni.fi.pa165.activities.repository.InvitationRepository;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@Transactional
@ConditionalOnExpression("${invitations.init.data:true}")
public class InvitationInitializer {

    private final InvitationRepository invitationRepository;

    @Autowired
    public InvitationInitializer(InvitationRepository invitationRepository) {
        this.invitationRepository = invitationRepository;
    }

    @PostConstruct
    public void insertDummyData() {
        initializedDummyActivity();
    }

    private void initializedDummyActivity() {

        Invitation activity = new Invitation();
        activity.setId("835fcb1f-0441-4684-ad4f-84c4af88216e");
        activity.setActivityId("835fcb1f-0441-4684-ad4f-84c4af88216e");
        activity.setSenderId("835fcb1f-0441-4684-ad4f-84c4af88216e");
        activity.setReceiverId("835fcb1f-0441-4684-ad4f-84c4af88216e");
        activity.setText("We invite you to our concert in London.");

        invitationRepository.save(activity);
    }
}
