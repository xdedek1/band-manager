package cz.muni.fi.pa165.activities.controller;

import cz.muni.fi.pa165.activities.facade.InvitationFacade;
import cz.muni.fi.pa165.common.data.dto.activity.InvitationCreateDto;
import cz.muni.fi.pa165.common.data.dto.activity.InvitationDetailDto;
import cz.muni.fi.pa165.common.data.dto.activity.InvitationMultipleCreateDto;
import cz.muni.fi.pa165.common.exception.OwnerValidationFailedException;
import cz.muni.fi.pa165.common.exception.ResourceNotFoundException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(path = "/invitations")
public class InvitationController {

    private final InvitationFacade invitationFacade;

    @Autowired
    public InvitationController(InvitationFacade invitationFacade) {
        this.invitationFacade = invitationFacade;
    }


    @Operation(summary = "Create a new activity invitation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Created the invitation"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
    })
    @PostMapping("")
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<InvitationDetailDto> createInvitation(@Valid @RequestBody InvitationCreateDto createDto) {
        return ResponseEntity.ok(invitationFacade.create(createDto));
    }

    @Operation(summary = "Create a new activity invitations")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Created the invitations"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
    })
    @PostMapping("/multiple")
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<Void> createMultipleInvitations(@Valid @RequestBody InvitationMultipleCreateDto createDto) {
        invitationFacade.createMultiple(createDto);
        return ResponseEntity.ok().build();
    }

    @Operation(summary = "Get mine activity invitations")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the activities"),
            @ApiResponse(responseCode = "404", description = "Activities not found"),
    })
    @GetMapping()
    public ResponseEntity<List<InvitationDetailDto>> findMine() {
        return ResponseEntity.ok(invitationFacade.findMine());
    }


    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<Void> handleResourceNotFoundException() {
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler(OwnerValidationFailedException.class)
    public ResponseEntity<Void> handleOwnerValidationFailedException() {
        return ResponseEntity.badRequest().build();
    }
}
