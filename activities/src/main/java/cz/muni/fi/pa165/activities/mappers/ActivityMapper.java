package cz.muni.fi.pa165.activities.mappers;

import cz.muni.fi.pa165.common.data.dto.activity.ActivityCreateDto;
import cz.muni.fi.pa165.common.data.dto.activity.ActivityDetailDto;
import cz.muni.fi.pa165.common.data.dto.activity.ActivityUpdateDto;
import cz.muni.fi.pa165.activities.data.model.Activity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ActivityMapper {

    ActivityDetailDto mapToDetailDto(Activity activity);
    Activity mapToEntity(ActivityCreateDto createDto);
    Activity mapToEntity(ActivityUpdateDto updateDto);
}
