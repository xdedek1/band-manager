package cz.muni.fi.pa165.activities.data.model;

import cz.muni.fi.pa165.common.data.BasicEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "activity")
public class Activity extends BasicEntity {

    @Schema(description = "Owner ID", example = "5c89f390-18ea-48c7-9b4f-addab2b25d50")
    @Column(name = "owner_id")
    private String ownerId;

    @Schema(description = "Band ID", example = "33b84b80-184c-4f0e-b9ce-179e67c06177")
    @Column(name = "band_id")
    private String bandId;

    @Schema(description = "Name of the activity", example = "Concert")
    @Column(name = "name", length = 50)
    private String name;

    @Schema(description = "Description of the activity", example = "The band will play their greatest hits.")
    @Column(name = "description", length = 200)
    private String description;

    @Schema(description = "Start time of the activity", example = "2021-12-24T18:00:00")
    @Column(name = "start_time")
    private LocalDateTime startTime;

    @Schema(description = "End time of the activity", example = "2021-12-24T22:00:00")
    @Column(name = "end_time")
    private LocalDateTime endTime;
}
