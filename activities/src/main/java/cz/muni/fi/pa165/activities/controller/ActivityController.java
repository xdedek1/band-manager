package cz.muni.fi.pa165.activities.controller;

import cz.muni.fi.pa165.activities.facade.ActivityFacade;
import cz.muni.fi.pa165.common.data.dto.activity.ActivityCreateDto;
import cz.muni.fi.pa165.common.data.dto.activity.ActivityDetailDto;
import cz.muni.fi.pa165.common.data.dto.activity.ActivityUpdateDto;
import cz.muni.fi.pa165.common.exception.OwnerValidationFailedException;
import cz.muni.fi.pa165.common.exception.ResourceNotFoundException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping(path = "/activities")
public class ActivityController {

    private final ActivityFacade activityFacade;

    @Autowired
    public ActivityController(ActivityFacade activityFacade) {
        this.activityFacade = activityFacade;
    }

    @Operation(summary = "Get an activity by its ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the activity"),
            @ApiResponse(responseCode = "404", description = "Activity not found"),
    })
    @GetMapping(path = "/{id}")
    public ResponseEntity<ActivityDetailDto> findById(@PathVariable("id") String id) {
        return ResponseEntity.ok(activityFacade.findById(id));
    }

    @Operation(summary = "Get activities by band ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the activities"),
            @ApiResponse(responseCode = "404", description = "Activities not found"),
    })
    @GetMapping(path = "/band/{band-id}")
    public ResponseEntity<List<ActivityDetailDto>> findByBand(@PathVariable("band-id") String bandId) {
        return ResponseEntity.ok(activityFacade.findByBand(bandId));
    }

    @Operation(summary = "Get activities by owner ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the activities"),
            @ApiResponse(responseCode = "404", description = "Activities not found"),
    })
    @GetMapping()
    public ResponseEntity<List<ActivityDetailDto>> findMine() {
        return ResponseEntity.ok(activityFacade.findMine());
    }

    @Operation(summary = "Create a new activity")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Created the activity"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
    })
    @PostMapping()
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<ActivityDetailDto> create(@Valid @RequestBody ActivityCreateDto createDto) {
        return ResponseEntity.ok(activityFacade.create(createDto));
    }

    @Operation(summary = "Update an existing activity")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated the activity"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
            @ApiResponse(responseCode = "404", description = "Activity not found"),
    })
    @PutMapping(path = "/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<ActivityDetailDto> update(@PathVariable(value = "id") String id,
                                                    @Valid @RequestBody ActivityUpdateDto updateDto) {
        return ResponseEntity.ok(activityFacade.update(id, updateDto));
    }

    @Operation(summary = "Delete an activity")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Deleted the activity"),
            @ApiResponse(responseCode = "404", description = "Activity not found"),
    })
    @DeleteMapping(path = "/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<Void> delete(@PathVariable(value = "id") String id) {
        activityFacade.delete(id);
        return ResponseEntity.noContent().build();
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<Void> handleResourceNotFoundException() {
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler(OwnerValidationFailedException.class)
    public ResponseEntity<Void> handleOwnerValidationFailedException() {
        return ResponseEntity.badRequest().build();
    }
}
