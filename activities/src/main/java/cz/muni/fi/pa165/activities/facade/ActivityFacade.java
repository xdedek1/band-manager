package cz.muni.fi.pa165.activities.facade;

import cz.muni.fi.pa165.activities.data.model.Activity;
import cz.muni.fi.pa165.activities.mappers.ActivityMapper;
import cz.muni.fi.pa165.activities.service.ActivityService;
import cz.muni.fi.pa165.common.data.dto.activity.ActivityCreateDto;
import cz.muni.fi.pa165.common.data.dto.activity.ActivityDetailDto;
import cz.muni.fi.pa165.common.data.dto.activity.ActivityUpdateDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ActivityFacade {

    private final ActivityService service;
    private final ActivityMapper mapper;

    @Autowired
    public ActivityFacade(ActivityService activityService, ActivityMapper activityMapper) {
        this.service = activityService;
        this.mapper = activityMapper;
    }

    @Transactional(readOnly = true)
    public ActivityDetailDto findById(String id) {
        return mapper.mapToDetailDto(service.findById(id));
    }

    @Transactional(readOnly = true)
    public List<ActivityDetailDto> findMine() {
        return service.findMine().stream()
                .map(mapper::mapToDetailDto)
                .toList();
    }

    @Transactional(readOnly = true)
    public List<ActivityDetailDto> findByBand(String bandId) {
        return service.findByBand(bandId).stream()
                .map(mapper::mapToDetailDto)
                .toList();
    }

    @Transactional
    public ActivityDetailDto create(ActivityCreateDto createDto) {
        Activity toCreate = mapper.mapToEntity(createDto);
        return mapper.mapToDetailDto(service.create(toCreate));
    }

    @Transactional
    public ActivityDetailDto update(String id, ActivityUpdateDto updateDto) {
        Activity toUpdate = mapper.mapToEntity(updateDto);
        return mapper.mapToDetailDto(service.update(id, toUpdate));
    }

    @Transactional
    public void delete(String id) {
        service.delete(id);
    }
}
