package cz.muni.fi.pa165.activities.mappers;

import cz.muni.fi.pa165.activities.data.model.Invitation;
import cz.muni.fi.pa165.common.data.dto.activity.InvitationCreateDto;
import cz.muni.fi.pa165.common.data.dto.activity.InvitationDetailDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface InvitationMapper {
    InvitationDetailDto mapToDetailDto(Invitation invitation);
    Invitation mapToEntity(InvitationCreateDto createDto);
}
