package cz.muni.fi.pa165.activities.data.model;

import cz.muni.fi.pa165.common.data.BasicEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "invitation")
public class Invitation extends BasicEntity {
    @Schema(description = "Activity ID", example = "5c89f390-18ea-48c7-9b4f-addab2b25d50")
    @Column(name = "activity_id")
    private String activityId;

    @Schema(description = "Sender ID", example = "5c89f390-18ea-48c7-9b4f-addab2b25d50")
    @Column(name = "sender_id")
    private String senderId;

    @Schema(description = "Receiver ID", example = "5c89f390-18ea-48c7-9b4f-addab2b25d50")
    @Column(name = "receiver_id")
    private String receiverId;

    @Schema(description = "Invitation text", example = "We invite you to our concert in London.")
    @Column(name = "text", length = 300)
    private String text;
}
