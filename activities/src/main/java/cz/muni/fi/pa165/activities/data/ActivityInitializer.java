package cz.muni.fi.pa165.activities.data;

import cz.muni.fi.pa165.activities.data.model.Activity;
import cz.muni.fi.pa165.activities.repository.ActivityRepository;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@Transactional
@ConditionalOnExpression("${activities.init.data:true}")
public class ActivityInitializer {

    private final ActivityRepository activityRepository;

    @Autowired
    public ActivityInitializer(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    @PostConstruct
    public void insertDummyData() {
        initializedDummyActivity();
    }

    private void initializedDummyActivity() {
        LocalDateTime startTime = LocalDateTime.of(2024, 1, 1, 12, 0, 0);

        Activity activity = new Activity();
        activity.setId("835fcb1f-0441-4684-ad4f-84c4af88216e");
        activity.setOwnerId("subject-id1@pa165.cz");
        activity.setName("Dummy activity");
        activity.setDescription("This is a dummy activity");
        activity.setStartTime(startTime);
        activity.setEndTime(startTime.plusHours(1));

        activityRepository.save(activity);
    }
}
