package cz.muni.fi.pa165.activities.facade;

import cz.muni.fi.pa165.activities.data.model.Invitation;
import cz.muni.fi.pa165.activities.mappers.InvitationMapper;
import cz.muni.fi.pa165.activities.service.InvitationService;
import cz.muni.fi.pa165.common.data.dto.activity.InvitationCreateDto;
import cz.muni.fi.pa165.common.data.dto.activity.InvitationDetailDto;
import cz.muni.fi.pa165.common.data.dto.activity.InvitationMultipleCreateDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class InvitationFacade {

    private final InvitationService service;
    private final InvitationMapper mapper;

    @Autowired
    public InvitationFacade(InvitationService service, InvitationMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @Transactional
    public InvitationDetailDto create(InvitationCreateDto createDto) {
        Invitation toCreate = mapper.mapToEntity(createDto);
        return mapper.mapToDetailDto(service.create(toCreate));
    }

    @Transactional
    public void createMultiple(InvitationMultipleCreateDto createDto) {
        service.createMultiple(createDto);
    }

    @Transactional(readOnly = true)
    public List<InvitationDetailDto> findMine() {
        return service.findMine().stream()
                .map(mapper::mapToDetailDto)
                .toList();
    }
}
