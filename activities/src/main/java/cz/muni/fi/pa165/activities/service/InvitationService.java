package cz.muni.fi.pa165.activities.service;

import cz.muni.fi.pa165.activities.data.model.Activity;
import cz.muni.fi.pa165.activities.data.model.Invitation;
import cz.muni.fi.pa165.activities.repository.InvitationRepository;
import cz.muni.fi.pa165.activities.security.Context;
import cz.muni.fi.pa165.common.data.dto.activity.InvitationMultipleCreateDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
public class InvitationService {

    private final InvitationRepository invitationRepository;
    private final ActivityService activityService;

    private final Context context;

    @Autowired
    public InvitationService(InvitationRepository invitationRepository, ActivityService activityService, Context context) {
        this.invitationRepository = invitationRepository;
        this.activityService = activityService;
        this.context = context;
    }

    @Transactional
    public Invitation create(Invitation toCreate) {
        Activity found = activityService.findById(toCreate.getActivityId());
        activityService.validateOwner(found);

        String userId = context.getLoggedUserId();
        toCreate.setSenderId(userId);

        return invitationRepository.save(toCreate);
    }

    @Transactional
    public void createMultiple(InvitationMultipleCreateDto createDto) {
        Activity found = activityService.findById(createDto.getActivityId());
        activityService.validateOwner(found);

        String userId = context.getLoggedUserId();

        List<Invitation> invitations = createDto.getReceiversIds().stream()
                .map(receiver -> {

                    Invitation invitation = new Invitation();
                    invitation.setActivityId(createDto.getActivityId());
                    invitation.setSenderId(userId);
                    invitation.setReceiverId(receiver);
                    invitation.setText(createDto.getText());
                    return invitation;
                })
                .toList();

        invitationRepository.saveAll(invitations);
    }

    @Transactional(readOnly = true)
    public List<Invitation> findMine() {
        String userId = context.getLoggedUserId();
        return invitationRepository.findByReceiver(userId);
    }
}
