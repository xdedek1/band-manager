package cz.muni.fi.pa165.activities.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.activities.data.model.Activity;
import cz.muni.fi.pa165.activities.repository.ActivityRepository;
import cz.muni.fi.pa165.activities.security.Context;
import cz.muni.fi.pa165.activities.utils.ActivityDataFactory;
import cz.muni.fi.pa165.common.data.dto.activity.ActivityCreateDto;
import cz.muni.fi.pa165.common.data.dto.activity.ActivityDetailDto;
import cz.muni.fi.pa165.common.data.dto.activity.ActivityUpdateDto;
import cz.muni.fi.pa165.common.data.factory.ActivityDtoFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.time.temporal.ChronoUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class ActivityControllerIT {
    public static final Activity TEST_ACTIVITY = ActivityDataFactory.createActivity();
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ActivityRepository activityRepository;

    @MockBean
    private Context context;


    @BeforeEach
    public void setUp() {
        activityRepository.save(TEST_ACTIVITY);
        when(context.getLoggedUserId()).thenReturn(TEST_ACTIVITY.getOwnerId());
    }

    @Test
    public void createActivity_validInput_returnsActivity() throws Exception {
        ActivityCreateDto activityCreateDto = ActivityDtoFactory.createActivityCreateDto();

        String response = mockMvc.perform(post("/activities")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(activityCreateDto)))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        ActivityDetailDto activityDetailDto = objectMapper.readValue(response, ActivityDetailDto.class);

        assertThat(activityDetailDto.getBandId()).isEqualTo(activityCreateDto.getBandId());
        assertThat(activityDetailDto.getOwnerId()).isEqualTo("1");
        assertThat(activityDetailDto.getName()).isEqualTo(activityCreateDto.getName());
        assertThat(activityDetailDto.getDescription()).isEqualTo(activityCreateDto.getDescription());
        assertThat(activityDetailDto.getStartTime()).isEqualTo(activityCreateDto.getStartTime());
    }

    @Test
    public void createActivity_invalidInput_returnsActivity() throws Exception {
        mockMvc.perform(post("/activities")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void findById_activityFound_returnsActivity() throws Exception {

        String result = mockMvc.perform(get("/activities/{id}", TEST_ACTIVITY.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        ActivityDetailDto response = objectMapper.readValue(result, ActivityDetailDto.class);

        assertThat(response.getId()).isEqualTo(TEST_ACTIVITY.getId());
        assertThat(response.getName()).isEqualTo(TEST_ACTIVITY.getName());
        assertThat(response.getDescription()).isEqualTo(TEST_ACTIVITY.getDescription());
        assertThat(response.getBandId()).isEqualTo(TEST_ACTIVITY.getBandId());
        assertThat(response.getOwnerId()).isEqualTo(TEST_ACTIVITY.getOwnerId());
        assertThat(response.getStartTime()).isCloseTo(TEST_ACTIVITY.getStartTime(), within(1, ChronoUnit.MILLIS));
        assertThat(response.getEndTime()).isCloseTo(TEST_ACTIVITY.getEndTime(), within(1, ChronoUnit.MILLIS));
    }

    @Test
    public void findById_activityNotFound_returnsNotFound() throws Exception {
        mockMvc.perform(get("/activities/{id}", "dummy-id")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateActivity_validInput_returnsActivity() throws Exception {
        ActivityUpdateDto activityUpdateDto = ActivityDtoFactory.createActivityUpdateDto();

        String response = mockMvc.perform(put("/activities/{id}", TEST_ACTIVITY.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(activityUpdateDto)))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        ActivityDetailDto activityDetailDto = objectMapper.readValue(response, ActivityDetailDto.class);

        assertThat(activityDetailDto.getName()).isEqualTo(activityUpdateDto.getName());
        assertThat(activityDetailDto.getDescription()).isEqualTo(activityUpdateDto.getDescription());
        assertThat(activityDetailDto.getStartTime()).isEqualTo(activityUpdateDto.getStartTime());
    }

    @Test
    public void updateActivity_invalidInput_returnsActivity() throws Exception {
        mockMvc.perform(put("/activities/{id}", TEST_ACTIVITY.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void deleteActivity_activityFound_returnsNoContent() throws Exception {
        mockMvc.perform(delete("/activities/{id}", TEST_ACTIVITY.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void deleteActivity_activityNotFound_returnsNotFound() throws Exception {
        mockMvc.perform(delete("/activities/{id}", "dummy-id")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

}
