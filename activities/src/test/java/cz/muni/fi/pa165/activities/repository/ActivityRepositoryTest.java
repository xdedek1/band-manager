package cz.muni.fi.pa165.activities.repository;

import cz.muni.fi.pa165.activities.data.model.Activity;
import cz.muni.fi.pa165.activities.utils.ActivityDataFactory;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@DataJpaTest
@EnableJpaRepositories
@ExtendWith(MockitoExtension.class)
public class ActivityRepositoryTest {

    @Mock
    private ActivityRepository activityRepository;

    @Test
    void findById_ActivityExists_ReturnsOptionalOfActivity() {
        // Arrange
        String activityId = "1";
        Activity activity = ActivityDataFactory.createActivity();
        when(activityRepository.findById(activityId)).thenReturn(Optional.of(activity));

        // Act
        Optional<Activity> result = activityRepository.findById(activityId);

        // Assert
        assertThat(result).isPresent();
        assertThat(result.get()).isEqualTo(activity);
    }

    @Test
    void findById_ActivityDoesNotExist_ReturnsEmptyOptional() {
        // Arrange
        String activityId = "1";
        when(activityRepository.findById(activityId)).thenReturn(Optional.empty());

        // Act
        Optional<Activity> result = activityRepository.findById(activityId);

        // Assert
        assertThat(result).isEmpty();
    }

    @Test
    void findByOwner_ActivitiesFound_ReturnsListOfActivities() {
        // Arrange
        String ownerId = "ownerId";
        List<Activity> activities = List.of(ActivityDataFactory.createActivity());
        when(activityRepository.findByOwner(ownerId)).thenReturn(activities);

        // Act
        List<Activity> result = activityRepository.findByOwner(ownerId);

        // Assert
        assertThat(result).hasSize(1);
        assertThat(result.getFirst()).isEqualTo(activities.getFirst());
    }

    @Test
    void findByOwner_NoActivitiesFound_ReturnsEmptyList() {
        // Arrange
        String ownerId = "ownerId";
        when(activityRepository.findByOwner(ownerId)).thenReturn(List.of());

        // Act
        List<Activity> result = activityRepository.findByOwner(ownerId);

        // Assert
        assertThat(result).isEmpty();
    }

    @Test
    void findByBand_ActivitiesFound_ReturnsListOfActivities() {
        // Arrange
        String bandId = "bandId";
        List<Activity> activities = List.of(ActivityDataFactory.createActivity());
        when(activityRepository.findByBand(bandId)).thenReturn(activities);

        // Act
        List<Activity> result = activityRepository.findByBand(bandId);

        // Assert
        assertThat(result).hasSize(1);
        assertThat(result.getFirst()).isEqualTo(activities.getFirst());
    }

    @Test
    void findByBand_NoActivitiesFound_ReturnsEmptyList() {
        // Arrange
        String bandId = "bandId";
        when(activityRepository.findByBand(bandId)).thenReturn(List.of());

        // Act
        List<Activity> result = activityRepository.findByBand(bandId);

        // Assert
        assertThat(result).isEmpty();
    }
}