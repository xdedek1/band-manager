package cz.muni.fi.pa165.activities.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.activities.facade.ActivityFacade;
import cz.muni.fi.pa165.common.data.dto.activity.ActivityCreateDto;
import cz.muni.fi.pa165.common.data.dto.activity.ActivityDetailDto;
import cz.muni.fi.pa165.common.data.dto.activity.ActivityUpdateDto;
import cz.muni.fi.pa165.common.data.dto.album.AlbumDetailDto;
import cz.muni.fi.pa165.common.data.dto.album.AlbumUpdateDto;
import cz.muni.fi.pa165.common.data.factory.ActivityDtoFactory;
import cz.muni.fi.pa165.common.exception.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class ActivityControllerTest {

    private final ActivityDetailDto TEST_DETAIL_DTO = ActivityDtoFactory.createActivityDetailDto();
    private final ActivityCreateDto TEST_CREATE_DTO = ActivityDtoFactory.createActivityCreateDto();
    private final ActivityUpdateDto TEST_UPDATE_DTO = ActivityDtoFactory.createActivityUpdateDto();
    private final ActivityDetailDto TEST_UPDATED_DETAIL_DTO = ActivityDtoFactory.createActivityDetailDto();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ActivityFacade activityFacade;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void create_validInput_returnsActivity() throws Exception {
        // Arrange
        ActivityCreateDto createDto = TEST_CREATE_DTO;
        ActivityDetailDto expectedDto = TEST_DETAIL_DTO;

        when(activityFacade.create(createDto)).thenReturn(expectedDto);

        // Act & Assert
        mockMvc.perform(post("/activities")
                        .param("userId", "test-user-id")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(TEST_CREATE_DTO)))
                .andExpect(status().isOk());
    }

    @Test
    public void create_invalidInput_returnsBadRequest() throws Exception {
        // Arrange
        when(activityFacade.create(any()))
                .thenReturn(null);

        // Act & Assert
        mockMvc.perform(post("/activities")
                        .param("activityId", "test-activity-id")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void findById_activityFound_returnsActivity() throws Exception {
        // Arrange
        ActivityDetailDto expectedDto = TEST_DETAIL_DTO;

        when(activityFacade.findById(TEST_DETAIL_DTO.getId())).thenReturn(expectedDto);

        // Act
        ResultActions result = mockMvc.perform(get("/activities/{id}", TEST_DETAIL_DTO.getId()).contentType(MediaType.APPLICATION_JSON));

        // Assert
        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(expectedDto.getId()))
                .andExpect(jsonPath("$.name").value(expectedDto.getName()))
                .andExpect(jsonPath("$.ownerId").value(expectedDto.getOwnerId()))
                .andExpect(jsonPath("$.bandId").value(expectedDto.getBandId()))
                .andExpect(jsonPath("$.description").value(expectedDto.getDescription()));
    }

    @Test
    public void findById_activityNotFound_returnsNotFound() throws Exception {
        String nonExistingId = "non-existent-id";

        when(activityFacade.findById(nonExistingId)).thenThrow(ResourceNotFoundException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/activities/{id}", nonExistingId))
                .andExpect(status().isNotFound());
    }

    @Test
    public void findByBand_activitiesFound_returnsActivities() throws Exception {
        // Arrange
        List<ActivityDetailDto> expectedActivities = List.of(TEST_DETAIL_DTO);
        when(activityFacade.findByBand(TEST_DETAIL_DTO.getBandId())).thenReturn(expectedActivities);

        // Act
        ResultActions result = mockMvc.perform(get("/activities/band/{id}", TEST_DETAIL_DTO.getBandId()).contentType(MediaType.APPLICATION_JSON));
        // Assert
        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(expectedActivities.getFirst().getId()))
                .andExpect(jsonPath("$[0].name").value(expectedActivities.getFirst().getName()))
                .andExpect(jsonPath("$[0].bandId").value(expectedActivities.getFirst().getBandId()))
                .andExpect(jsonPath("$[0].ownerId").value(expectedActivities.getFirst().getOwnerId()));
    }

    @Test
    public void findByBand_activitiesNotFound_returnsNotFound() throws Exception {
        String nonExistingId = "non-existent-id";

        when(activityFacade.findByBand(nonExistingId)).thenThrow(ResourceNotFoundException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/activities/band/{id}", nonExistingId))
                .andExpect(status().isNotFound());
    }

    @Test
    public void findMine_activitiesFound_returnsActivities() throws Exception {
        // Arrange
        List<ActivityDetailDto> expectedActivities = List.of(TEST_DETAIL_DTO);

        when(activityFacade.findMine()).thenReturn(expectedActivities);

        // Act
        ResultActions result = mockMvc.perform(get("/activities")
                .contentType(MediaType.APPLICATION_JSON));

        // Assert
        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(expectedActivities.getFirst().getId()))
                .andExpect(jsonPath("$[0].name").value(expectedActivities.getFirst().getName()))
                .andExpect(jsonPath("$[0].bandId").value(expectedActivities.getFirst().getBandId()))
                .andExpect(jsonPath("$[0].ownerId").value(expectedActivities.getFirst().getOwnerId()));
    }

    @Test
    public void update_validInput_returnsActivity() throws Exception {
        // Arrange
        ActivityDetailDto expectedDto = TEST_UPDATED_DETAIL_DTO;

        when(activityFacade.update(expectedDto.getId(), TEST_UPDATE_DTO)).thenReturn(expectedDto);

        // Act
        ResultActions result = mockMvc.perform(put("/activities/{id}", expectedDto.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content("{" +
                        "\"name\":\"" + TEST_UPDATE_DTO.getName() + "\", " +
                        "\"description\":\"" + TEST_UPDATE_DTO.getDescription() + "\", " +
                        "\"startTime\":\"" + TEST_UPDATE_DTO.getStartTime() + "\", " +
                        "\"endTime\":\"" + TEST_UPDATE_DTO.getEndTime() + "\"" +
                        "}")
        );

        // Assert
        result.andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(expectedDto.getName()))
                .andExpect(jsonPath("$.description").value(expectedDto.getDescription()));
    }

    @Test
    public void update_invalidInput_returnsBadRequest() throws Exception {
        // Arrange
        ActivityUpdateDto invalidUpdateDto = new ActivityUpdateDto();
        invalidUpdateDto.setName("");

        when(activityFacade.update(any(), any())).
                thenReturn(TEST_UPDATED_DETAIL_DTO);

        // Act & Assert
        mockMvc.perform(put("/activities/{id}", TEST_UPDATED_DETAIL_DTO.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(invalidUpdateDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void update_activityNotFound_returnsNotFound() throws Exception {
        // Arrange
        String nonExistingId = "invalid-id";
        ActivityUpdateDto updateDto = TEST_UPDATE_DTO;

        when(activityFacade.update(nonExistingId, updateDto)).thenThrow(ResourceNotFoundException.class);

        // Act & Assert
        mockMvc.perform(put("/activities/{id}", nonExistingId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void delete_activityFound_returnsNoContent() throws Exception {
        // Arrange
        String id = TEST_DETAIL_DTO.getId();

        // Act
        ResultActions resultActions = mockMvc.perform(delete("/activities/{id}", id));

        // Assert
        resultActions.andExpect(status().isNoContent());
        verify(activityFacade, times(1)).delete(id);
    }

    @Test
    public void delete_activityNotFound_returnsNotFound() throws Exception {
        // Arrange
        String nonExistingId = "invalid-id";

        doThrow(ResourceNotFoundException.class).when(activityFacade).delete(nonExistingId);

        // Act & Assert
        mockMvc.perform(delete("/activities/{id}", nonExistingId))
                .andExpect(status().isNotFound());
    }
}