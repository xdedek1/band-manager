package cz.muni.fi.pa165.activities;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = ActivitiesApplication.class)
class ActivitiesApplicationTests {
    @Test
    void contextLoads() {
    }

}
