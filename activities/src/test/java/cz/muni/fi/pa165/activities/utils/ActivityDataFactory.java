package cz.muni.fi.pa165.activities.utils;

import cz.muni.fi.pa165.activities.data.model.Activity;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class ActivityDataFactory {

    public static Activity createActivity() {
        Activity activity = new Activity();
        activity.setId("1");
        activity.setDescription("This is an example activity.");
        activity.setName("Name");
        activity.setBandId("1");
        activity.setOwnerId("1");
        activity.setStartTime(LocalDateTime.now());
        activity.setEndTime(LocalDateTime.now().plusHours(1));
        return activity;
    }
}
