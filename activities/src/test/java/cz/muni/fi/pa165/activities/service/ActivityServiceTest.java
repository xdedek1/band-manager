package cz.muni.fi.pa165.activities.service;

import cz.muni.fi.pa165.activities.data.model.Activity;
import cz.muni.fi.pa165.activities.repository.ActivityRepository;
import cz.muni.fi.pa165.activities.security.Context;
import cz.muni.fi.pa165.activities.utils.ActivityDataFactory;
import cz.muni.fi.pa165.common.exception.OwnerValidationFailedException;
import cz.muni.fi.pa165.common.exception.ResourceNotFoundException;
import cz.muni.fi.pa165.common.exception.OwnerValidationFailedException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionAuthenticatedPrincipal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ActivityServiceTest {
    @Mock
    private ActivityRepository activityRepository;

    @Mock
    private Context context;

    @InjectMocks
    private ActivityService activityService;

    private final Activity TEST_ACTIVITY = ActivityDataFactory.createActivity();

    @Test
    void findById_activityFound_returnsActivity() {
        // Arrange
        when(activityRepository.findById(TEST_ACTIVITY.getId())).thenReturn(Optional.of(TEST_ACTIVITY));

        // Act
        Activity foundActivity = activityService.findById(TEST_ACTIVITY.getId());

        // Assert
        assertThat(foundActivity).isEqualTo(TEST_ACTIVITY);
    }

    @Test
    void findById_activityNotFound_throwsResourceNotFoundException() {
        // Arrange
        when(activityRepository.findById(TEST_ACTIVITY.getId())).thenReturn(Optional.empty());
//        when(context.getCurrentPrincipal().getSubject()).thenReturn(TEST_ACTIVITY.getOwnerId());

        // Act & Assert
        assertThrows(ResourceNotFoundException.class, () -> activityService.findById(TEST_ACTIVITY.getId()));
    }

    @Test
    void findMine_activitiesFound_returnsActivities() {
        // Arrange
        List<Activity> activities = new ArrayList<>();
        activities.add(TEST_ACTIVITY);

        when(activityRepository.findByOwner(TEST_ACTIVITY.getOwnerId())).thenReturn(activities);
        when(context.getLoggedUserId()).thenReturn(TEST_ACTIVITY.getOwnerId());

        // Act
        List<Activity> foundActivities = activityService.findMine();

        // Assert
        assertThat(foundActivities).isEqualTo(activities);
    }

    @Test
    void findMine_activitiesNotFound_returnsEmptyList() {
        // Arrange
        List<Activity> emptyList = Collections.emptyList();

        when(activityRepository.findByOwner(TEST_ACTIVITY.getOwnerId())).thenReturn(emptyList);
        when(context.getLoggedUserId()).thenReturn(TEST_ACTIVITY.getOwnerId());

        // Act
        List<Activity> foundActivities = activityService.findMine();

        // Assert
        assertThat(foundActivities).isEmpty();
    }

    @Test
    void findByBand_activitiesFound_returnsActivities() {
        // Arrange
        List<Activity> activities = new ArrayList<>();
        activities.add(TEST_ACTIVITY);

        when(activityRepository.findByBand(TEST_ACTIVITY.getBandId())).thenReturn(activities);

        // Act
        List<Activity> foundActivities = activityService.findByBand(TEST_ACTIVITY.getBandId());

        // Assert
        assertThat(foundActivities).isEqualTo(activities);
    }

    @Test
    void findByBand_activitiesNotFound_returnsEmptyList() {
        // Arrange
        List<Activity> emptyList = Collections.emptyList();

        when(activityRepository.findByBand(TEST_ACTIVITY.getBandId())).thenReturn(emptyList);

        // Act
        List<Activity> foundActivities = activityService.findByBand(TEST_ACTIVITY.getBandId());

        // Assert
        assertThat(foundActivities).isEmpty();
    }

    @Test
    void create_activityCreated_returnsCreatedActivity() {
        // Arrange
        when(activityRepository.save(TEST_ACTIVITY)).thenReturn(TEST_ACTIVITY);

        // Act
        Activity createdActivity = activityService.create(TEST_ACTIVITY);

        // Assert
        assertThat(createdActivity).isEqualTo(TEST_ACTIVITY);
    }

    @Test
    void create_loggedUserIdIsNull_returnsNull() {
        // Arrange
        when(context.getLoggedUserId()).thenReturn(null);

        // Act
        Activity createdActivity = activityService.create(TEST_ACTIVITY);

        // Assert
        assertThat(createdActivity).isNull();
    }

    @Test
    void update_activityExists_returnsUpdatedActivity() {
        // Arrange
        when(activityRepository.findById(TEST_ACTIVITY.getId())).thenReturn(Optional.of(TEST_ACTIVITY));
        when(activityRepository.save(TEST_ACTIVITY)).thenReturn(TEST_ACTIVITY);

        // Act
        Activity updatedActivity = activityService.update(TEST_ACTIVITY.getId(), TEST_ACTIVITY);

        // Assert
        assertThat(updatedActivity).isEqualTo(TEST_ACTIVITY);
    }

    @Test
    void update_activityNotExists_throwsResourceNotFoundException() {
        // Arrange
        when(activityRepository.findById(TEST_ACTIVITY.getId())).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(ResourceNotFoundException.class, () -> activityService.update(TEST_ACTIVITY.getId(), TEST_ACTIVITY));
    }

    @Test
    void delete_activityExists_deletesActivity() {
        // Arrange
        when(activityRepository.findById(TEST_ACTIVITY.getId())).thenReturn(Optional.of(TEST_ACTIVITY));

        // Act
        activityService.delete(TEST_ACTIVITY.getId());

        // Assert
        verify(activityRepository).delete(TEST_ACTIVITY);
    }

    @Test
    void delete_activityNotExists_throwsResourceNotFoundException() {
        // Arrange
        when(activityRepository.findById(TEST_ACTIVITY.getId())).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(ResourceNotFoundException.class, () -> activityService.delete(TEST_ACTIVITY.getId()));
    }

    @Test
    void validateOwner_validOwner_noExceptionThrown() {
        // Arrange
        when(context.getLoggedUserId()).thenReturn(TEST_ACTIVITY.getOwnerId());

        // Act & Assert
        assertDoesNotThrow(() -> activityService.validateOwner(TEST_ACTIVITY));
    }

    @Test
    void validateOwner_invalidOwner_throwsOwnerValidationFailedException() {
        // Arrange
        when(context.getLoggedUserId()).thenReturn("anotherUser");

        // Act & Assert
        assertThrows(OwnerValidationFailedException.class, () -> activityService.validateOwner(TEST_ACTIVITY));
    }

    @Test
    void validateOwner_loggedUserIdIsNull_throwsOwnerValidationFailedException() {
        // Arrange
        when(context.getLoggedUserId()).thenReturn(null);

        // Act & Assert
        assertThrows(OwnerValidationFailedException.class, () -> activityService.validateOwner(TEST_ACTIVITY));
    }
}
