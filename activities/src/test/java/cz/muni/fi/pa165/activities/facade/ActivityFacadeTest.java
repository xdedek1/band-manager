package cz.muni.fi.pa165.activities.facade;

import cz.muni.fi.pa165.activities.data.model.Activity;
import cz.muni.fi.pa165.activities.mappers.ActivityMapper;
import cz.muni.fi.pa165.activities.service.ActivityService;
import cz.muni.fi.pa165.activities.utils.ActivityDataFactory;
import cz.muni.fi.pa165.common.data.dto.activity.ActivityCreateDto;
import cz.muni.fi.pa165.common.data.dto.activity.ActivityDetailDto;
import cz.muni.fi.pa165.common.data.dto.activity.ActivityUpdateDto;
import cz.muni.fi.pa165.common.data.factory.ActivityDtoFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ActivityFacadeTest {

    @Mock
    private ActivityService activityService;

    @Mock
    private ActivityMapper activityMapper;

    @InjectMocks
    private ActivityFacade activityFacade;
    
    private final Activity TEST_ACTIVITY = ActivityDataFactory.createActivity();
    private final ActivityDetailDto TEST_DETAIL_DTO = ActivityDtoFactory.createActivityDetailDto();
    private final ActivityCreateDto TEST_CREATE_DTO = ActivityDtoFactory.createActivityCreateDto();
    private final ActivityUpdateDto TEST_UPDATE_DTO = ActivityDtoFactory.createActivityUpdateDto();
    

    @Test
    void findById_activityFound_returnsActivityDetailDto() {
        // Arrange
        when(activityService.findById(TEST_ACTIVITY.getId())).thenReturn(TEST_ACTIVITY);
        when(activityMapper.mapToDetailDto(TEST_ACTIVITY)).thenReturn(TEST_DETAIL_DTO);

        // Act
        ActivityDetailDto foundDto = activityFacade.findById(TEST_ACTIVITY.getId());

        // Assert
        assertThat(foundDto).isEqualTo(TEST_DETAIL_DTO);
    }

    @Test
    void findMine_activitiesFound_returnsListOfActivityDetailDto() {
        // Arrange
        List<Activity> activities = new ArrayList<>();
        activities.add(TEST_ACTIVITY);
        when(activityService.findMine()).thenReturn(activities);
        when(activityMapper.mapToDetailDto(TEST_ACTIVITY)).thenReturn(TEST_DETAIL_DTO);

        // Act
        List<ActivityDetailDto> foundDtos = activityFacade.findMine();

        // Assert
        assertThat(foundDtos).containsExactly(TEST_DETAIL_DTO);
    }

    @Test
    void findByBand_activitiesFound_returnsListOfActivityDetailDto() {
        // Arrange
        List<Activity> activities = new ArrayList<>();
        activities.add(TEST_ACTIVITY);
        when(activityService.findByBand(TEST_ACTIVITY.getBandId())).thenReturn(activities);
        when(activityMapper.mapToDetailDto(TEST_ACTIVITY)).thenReturn(TEST_DETAIL_DTO);

        // Act
        List<ActivityDetailDto> foundDtos = activityFacade.findByBand(TEST_ACTIVITY.getBandId());

        // Assert
        assertThat(foundDtos).containsExactly(TEST_DETAIL_DTO);
    }

    @Test
    void create_activityCreated_returnsActivityDetailDto() {
        // Arrange
        when(activityMapper.mapToEntity(TEST_CREATE_DTO)).thenReturn(TEST_ACTIVITY);
        when(activityService.create(TEST_ACTIVITY)).thenReturn(TEST_ACTIVITY);
        when(activityMapper.mapToDetailDto(TEST_ACTIVITY)).thenReturn(TEST_DETAIL_DTO);

        // Act
        ActivityDetailDto createdDto = activityFacade.create(TEST_CREATE_DTO);

        // Assert
        assertThat(createdDto).isEqualTo(TEST_DETAIL_DTO);
    }

    @Test
    void update_activityUpdated_returnsActivityDetailDto() {
        // Arrange
        when(activityMapper.mapToEntity(TEST_UPDATE_DTO)).thenReturn(TEST_ACTIVITY);
        when(activityService.update(TEST_ACTIVITY.getId(), TEST_ACTIVITY)).thenReturn(TEST_ACTIVITY);
        when(activityMapper.mapToDetailDto(TEST_ACTIVITY)).thenReturn(TEST_DETAIL_DTO);

        // Act
        ActivityDetailDto updatedDto = activityFacade.update(TEST_ACTIVITY.getId(), TEST_UPDATE_DTO);

        // Assert
        assertThat(updatedDto).isEqualTo(TEST_DETAIL_DTO);
    }

    @Test
    void delete_activityDeleted() {
        // Act
        activityFacade.delete(TEST_ACTIVITY.getId());

        // Assert
        verify(activityService).delete(TEST_ACTIVITY.getId());
    }
}

