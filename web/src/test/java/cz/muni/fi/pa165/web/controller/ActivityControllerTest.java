package cz.muni.fi.pa165.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.common.data.dto.activity.ActivityCreateDto;
import cz.muni.fi.pa165.common.data.dto.activity.ActivityDetailDto;
import cz.muni.fi.pa165.common.data.dto.activity.ActivityUpdateDto;
import cz.muni.fi.pa165.common.data.factory.ActivityDtoFactory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class ActivityControllerTest {

    private final ActivityDetailDto TEST_DETAIL_DTO = ActivityDtoFactory.createActivityDetailDto();
    private final ActivityCreateDto TEST_CREATE_DTO = ActivityDtoFactory.createActivityCreateDto();
    private final ActivityUpdateDto TEST_UPDATE_DTO = ActivityDtoFactory.createActivityUpdateDto();
    private final ActivityDetailDto TEST_UPDATED_DETAIL_DTO = ActivityDtoFactory.createActivityDetailDto();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ActivityController activityController;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void createActivity_validInput_returnsActivity() throws Exception {
        // Arrange
        when(activityController.createActivity(any()))
                .thenReturn(ResponseEntity.ok(TEST_DETAIL_DTO));

        // Act & Assert
        mockMvc.perform(post("/activities")
                        .param("userId", "test-user-id")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(TEST_CREATE_DTO)))
                .andExpect(status().isOk());
    }

    @Test
    public void createActivity_invalidInput_returnsBadRequest() throws Exception {
        // Arrange
        when(activityController.createActivity(any()))
                .thenReturn(ResponseEntity.badRequest().build());

        // Act & Assert
        mockMvc.perform(post("/activities")
                        .param("userId", "test-user-id")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void findById_activityFound_returnsActivity() throws Exception {
        // Arrange
        ActivityDetailDto expectedDto = TEST_DETAIL_DTO;

        when(activityController.findById(TEST_DETAIL_DTO.getId())).thenReturn(ResponseEntity.ok(TEST_DETAIL_DTO));

        // Act
        ResultActions result = mockMvc.perform(get("/activities/{id}", TEST_DETAIL_DTO.getId()).contentType(MediaType.APPLICATION_JSON));

        // Assert
        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(expectedDto.getId()))
                .andExpect(jsonPath("$.name").value(expectedDto.getName()))
                .andExpect(jsonPath("$.ownerId").value(expectedDto.getOwnerId()))
                .andExpect(jsonPath("$.bandId").value(expectedDto.getBandId()))
                .andExpect(jsonPath("$.description").value(expectedDto.getDescription()));
    }

    @Test
    public void findById_activityNotFound_returnsNotFound() throws Exception {
        // Arrange
        String nonExistentId = "non-existent-id";

        when(activityController.findById(nonExistentId)).thenReturn(ResponseEntity.notFound().build());

        // Act
        ResultActions result = mockMvc.perform(get("/activities/{id}", nonExistentId).contentType(MediaType.APPLICATION_JSON));

        // Assert
        result.andExpect(status().isNotFound());
    }

    @Test
    public void findByBand_activitiesFound_returnsActivities() throws Exception {
        // Arrange
        List<ActivityDetailDto> expectedActivities = List.of(TEST_DETAIL_DTO);

        when(activityController.findByBand(TEST_DETAIL_DTO.getBandId())).thenReturn(ResponseEntity.ok(expectedActivities));

        // Act
        ResultActions result = mockMvc.perform(get("/activities/band/{band-id}", TEST_DETAIL_DTO.getBandId()).contentType(MediaType.APPLICATION_JSON));

        // Assert
        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(expectedActivities.getFirst().getId()))
                .andExpect(jsonPath("$[0].name").value(expectedActivities.getFirst().getName()))
                .andExpect(jsonPath("$[0].bandId").value(expectedActivities.getFirst().getBandId()))
                .andExpect(jsonPath("$[0].ownerId").value(expectedActivities.getFirst().getOwnerId()));
    }

    @Test
    public void findByBand_activitiesNotFound_returnsNotFound() throws Exception {
        // Arrange
        String nonExistentBandId = "non-existent-band-id";

        when(activityController.findByBand(nonExistentBandId)).thenReturn(ResponseEntity.notFound().build());

        // Act
        ResultActions result = mockMvc.perform(get("/activities/band/{band-id}", nonExistentBandId).contentType(MediaType.APPLICATION_JSON));

        // Assert
        result.andExpect(status().isNotFound());
    }

    @Test
    public void findMine_activitiesFound_returnsActivities() throws Exception {
        // Arrange
        List<ActivityDetailDto> expectedActivities = List.of(TEST_DETAIL_DTO);

        when(activityController.findMine()).thenReturn(ResponseEntity.ok(expectedActivities));

        // Act
        ResultActions result = mockMvc.perform(get("/activities")
                .contentType(MediaType.APPLICATION_JSON));

        // Assert
        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(expectedActivities.getFirst().getId()))
                .andExpect(jsonPath("$[0].name").value(expectedActivities.getFirst().getName()))
                .andExpect(jsonPath("$[0].bandId").value(expectedActivities.getFirst().getBandId()))
                .andExpect(jsonPath("$[0].ownerId").value(expectedActivities.getFirst().getOwnerId()));
    }

    @Test
    public void updateActivity_validInput_returnsActivity() throws Exception {
        // Arrange
        ActivityDetailDto expectedDto = TEST_UPDATED_DETAIL_DTO;

        when(activityController.update(expectedDto.getId(), TEST_UPDATE_DTO)).thenReturn(ResponseEntity.ok(expectedDto));

        // Act
        ResultActions result = mockMvc.perform(put("/activities/{id}", expectedDto.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content("{" +
                        "\"name\":\"" + TEST_UPDATE_DTO.getName() + "\", " +
                        "\"description\":\"" + TEST_UPDATE_DTO.getDescription() + "\", " +
                        "\"startTime\":\"" + TEST_UPDATE_DTO.getStartTime() + "\", " +
                        "\"endTime\":\"" + TEST_UPDATE_DTO.getEndTime() + "\"" +
                        "}")
        );

        // Assert
        result.andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(expectedDto.getName()))
                .andExpect(jsonPath("$.description").value(expectedDto.getDescription()));
    }

    @Test
    public void updateActivity_invalidInput_returnsBadRequest() throws Exception {
        // Arrange
        ActivityUpdateDto invalidUpdateDto = new ActivityUpdateDto();
        invalidUpdateDto.setName("");

        when(activityController.update(any(), any())).
                thenReturn(ResponseEntity.ok(TEST_UPDATED_DETAIL_DTO));

        // Act & Assert
        mockMvc.perform(put("/activities/{id}", TEST_UPDATED_DETAIL_DTO.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(invalidUpdateDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void deleteActivity_activityFound_returnsNoContent() throws Exception {
        // Arrange
        when(activityController.delete(any())).thenReturn(ResponseEntity.noContent().build());

        // Act & Assert
        mockMvc.perform(delete("/activities/{id}", TEST_DETAIL_DTO.getId()))
                .andExpect(status().isNoContent());
    }

    @Test
    public void deleteActivity_activityNotFound_returnsNotFound() throws Exception {
        // Arrange
        when(activityController.delete(any()))
                .thenReturn(ResponseEntity.notFound().build());

        // Act & Assert
        mockMvc.perform(delete("/activities/nonexistent-id"))
                .andExpect(status().isNotFound());
    }
}