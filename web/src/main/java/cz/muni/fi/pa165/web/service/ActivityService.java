package cz.muni.fi.pa165.web.service;

import cz.muni.fi.pa165.common.client.IntegrationClient;
import cz.muni.fi.pa165.common.data.dto.activity.ActivityCreateDto;
import cz.muni.fi.pa165.common.data.dto.activity.ActivityDetailDto;
import cz.muni.fi.pa165.common.data.dto.activity.ActivityUpdateDto;
import cz.muni.fi.pa165.common.exception.ResourceNotFoundException;
import cz.muni.fi.pa165.web.utils.ValidationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;

import static cz.muni.fi.pa165.common.client.IntegrationClientConfiguration.ACTIVITIES;

@Service
public class ActivityService {

    private final IntegrationClient activitiesClient;

    private final ValidationUtils validator;

    @Autowired
    public ActivityService(@Qualifier(ACTIVITIES) IntegrationClient activitiesClient, ValidationUtils validator) {
        this.activitiesClient = activitiesClient;
        this.validator = validator;
    }

    public ActivityDetailDto createMyActivity(ActivityCreateDto activityCreateDto) {
        validator.validateBandManager(activityCreateDto.getBandId());
        return activitiesClient.doPost("/activities", new ParameterizedTypeReference<>() {}, activityCreateDto);
    }

    public ActivityDetailDto findById(String id) {
        try {
            return activitiesClient.doGet("/activities/" + id, new ParameterizedTypeReference<>() {
            });
        } catch (HttpClientErrorException.NotFound e) {
            throw new ResourceNotFoundException("Activity with id " + id + " not found");
        }
    }

    public List<ActivityDetailDto> findByBand(String bandId) {
        return activitiesClient.doGet("/activities/band/" + bandId, new ParameterizedTypeReference<>() {});
    }

    public List<ActivityDetailDto> findMine() {
        MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();
        return activitiesClient.doGet("/activities", new ParameterizedTypeReference<>() {}, queryParams);
    }

    public ActivityDetailDto update(String id, ActivityUpdateDto activity) {
        ActivityDetailDto found = activitiesClient.doGet("/activities/" + id, new ParameterizedTypeReference<>() {});
        if (found != null) {
            validator.validateBandManager(found.getBandId());
        }
        return activitiesClient.doPut("/activities/" + id, ActivityDetailDto.class, activity);
    }

    public Void delete(String id) {
        ActivityDetailDto activity = activitiesClient.doGet("/activities/" + id, new ParameterizedTypeReference<>() {});
        if (activity != null) {
            validator.validateBandManager(activity.getBandId());
        }
        return activitiesClient.doDelete("/activities/" + id, Void.class);
    }
}
