package cz.muni.fi.pa165.web.controller;

import cz.muni.fi.pa165.common.data.dto.band.BandCreateDto;
import cz.muni.fi.pa165.common.data.dto.band.BandDetailDto;
import cz.muni.fi.pa165.common.data.dto.band.BandUpdateDto;
import cz.muni.fi.pa165.common.exception.ExceptionDto;
import cz.muni.fi.pa165.common.exception.OwnerValidationFailedException;
import cz.muni.fi.pa165.common.exception.ResourceNotFoundException;
import cz.muni.fi.pa165.web.service.BandService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static cz.muni.fi.pa165.web.security.Scopes.READ;
import static cz.muni.fi.pa165.web.security.Scopes.WRITE;

@RestController
@RequestMapping("/bands")
public class BandController {

    private final BandService bandService;

    @Autowired
    public BandController(BandService bandService) {
        this.bandService = bandService;
    }

    @Operation(summary = "Get a band by its ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the band"),
            @ApiResponse(responseCode = "404", description = "Band not found"),
    })
    @GetMapping(path = "/{id}")
    @PreAuthorize("hasAuthority('" + READ + "')")
    public ResponseEntity<BandDetailDto> findById(@PathVariable("id") String id) {
        return ResponseEntity.ok(bandService.findById(id));
    }

    @Operation(summary = "Get all bands of a manager")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the bands"),
            @ApiResponse(responseCode = "404", description = "Bands not found"),
    })
    @GetMapping()
    @PreAuthorize("hasAuthority('" + READ + "')")
    public ResponseEntity<List<BandDetailDto>> findMine() {
        return ResponseEntity.ok(bandService.findMine());
    }

    @Operation(summary = "Create a new band")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Created the band"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
    })
    @PostMapping()
    @ResponseStatus(value = HttpStatus.OK)
    @PreAuthorize("hasAuthority('" + WRITE + "')")
    public ResponseEntity<BandDetailDto> create(@Valid @RequestBody BandCreateDto createDto) {
        return ResponseEntity.ok(bandService.create(createDto));
    }

    @Operation(summary = "Update an existing band")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated the band"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
            @ApiResponse(responseCode = "404", description = "Band not found"),
    })
    @PutMapping(path = "/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    @PreAuthorize("hasAuthority('" + WRITE + "')")
    public ResponseEntity<BandDetailDto> update(@PathVariable(value = "id") String id, @Valid @RequestBody BandUpdateDto updateDto) {
        return ResponseEntity.ok(bandService.update(id, updateDto));
    }

    @Operation(summary = "Delete a band")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Deleted the band"),
            @ApiResponse(responseCode = "404", description = "Band not found"),
    })
    @DeleteMapping(path = "/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    @PreAuthorize("hasAuthority('" + WRITE + "')")
    public ResponseEntity<Void> delete(@PathVariable(value = "id") String id) {
        return ResponseEntity.ok(bandService.delete(id));
    }

    @Operation(summary = "Follow a band")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Followed the band"),
            @ApiResponse(responseCode = "404", description = "Band not found"),
    })
    @PostMapping(path = "/{id}/follow")
    @ResponseStatus(value = HttpStatus.OK)
    @PreAuthorize("hasAuthority('" + WRITE + "')")
    public ResponseEntity<Void> followBand(@PathVariable(value = "id") String id) {
        return ResponseEntity.ok(bandService.followBand(id));
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ExceptionDto> handleResourceNotFoundException(HttpServletRequest req, Exception ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new ExceptionDto(ex.getMessage(), ex.getClass(), null, HttpStatus.NOT_FOUND));
    }

    @ExceptionHandler(OwnerValidationFailedException.class)
    public ResponseEntity<ExceptionDto> handleOwnerValidationFailedException(HttpServletRequest req, Exception ex) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN)
                .body(new ExceptionDto(ex.getMessage(), ex.getClass(), null, HttpStatus.FORBIDDEN));
    }
}
