package cz.muni.fi.pa165.web.controller;

import cz.muni.fi.pa165.common.data.dto.activity.*;
import cz.muni.fi.pa165.common.exception.ExceptionDto;
import cz.muni.fi.pa165.common.exception.OwnerValidationFailedException;
import cz.muni.fi.pa165.common.exception.ResourceNotFoundException;
import cz.muni.fi.pa165.web.service.ActivityService;
import cz.muni.fi.pa165.web.service.InvitationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static cz.muni.fi.pa165.web.security.Scopes.READ;
import static cz.muni.fi.pa165.web.security.Scopes.WRITE;

@RestController
@RequestMapping("/activities")
public class ActivityController {

    private final ActivityService activityService;
    private final InvitationService invitationService;
    
    @Autowired
    public ActivityController(ActivityService activityService, InvitationService invitationService) {
        this.activityService = activityService;
        this.invitationService = invitationService;
    }

    @Operation(summary = "Create a new activity for a activity")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Created the activity"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
    })
    @PostMapping
    @PreAuthorize("hasAuthority('" + WRITE + "')")
    public ResponseEntity<ActivityDetailDto> createActivity(@Valid @RequestBody ActivityCreateDto dto) {
        return ResponseEntity.ok(activityService.createMyActivity(dto));
    }

    @Operation(summary = "Get a activity by its ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the activity"),
            @ApiResponse(responseCode = "404", description = "activity not found"),
    })
    @GetMapping(path = "/{id}")
    @PreAuthorize("hasAuthority('" + READ + "')")
    public ResponseEntity<ActivityDetailDto> findById(@PathVariable(value = "id") String id) {
        return ResponseEntity.ok(activityService.findById(id));
    }

    @Operation(summary = "Get activities by band ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the activities"),
            @ApiResponse(responseCode = "404", description = "Activities not found"),
    })
    @GetMapping(path = "/band/{band-id}")
    @PreAuthorize("hasAuthority('" + READ + "')")
    public ResponseEntity<List<ActivityDetailDto>> findByBand(@PathVariable("band-id") String bandId) {
        return ResponseEntity.ok(activityService.findByBand(bandId));
    }

    @Operation(summary = "Get activities by owner ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the activities"),
            @ApiResponse(responseCode = "404", description = "Activities not found"),
    })
    @GetMapping()
    @PreAuthorize("hasAuthority('" + READ + "')")
    public ResponseEntity<List<ActivityDetailDto>> findMine() {
        return ResponseEntity.ok(activityService.findMine());
    }

    @Operation(summary = "Update an existing activity")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated the activity"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
            @ApiResponse(responseCode = "404", description = "activity not found"),
    })
    @PutMapping(path = "/{id}")
    @PreAuthorize("hasAuthority('" + WRITE + "')")
    public ResponseEntity<ActivityDetailDto> update(@PathVariable(value = "id") String id, @RequestBody @Valid ActivityUpdateDto activity) {
        return ResponseEntity.ok(activityService.update(id, activity));
    }

    @Operation(summary = "Delete a activity")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Deleted the activity"),
            @ApiResponse(responseCode = "404", description = "activity not found"),
    })
    @DeleteMapping(path = "/{id}")
    @PreAuthorize("hasAuthority('" + WRITE + "')")
    public ResponseEntity<Void> delete(@PathVariable(value = "id") String id) {
        return ResponseEntity.ok(activityService.delete(id));
    }

    @Operation(summary = "Create invitations for an activity")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Invitations created"),
            @ApiResponse(responseCode = "404", description = "Activity not found"),
    })
    @PostMapping(path = "/{id}/invitations")
    @ResponseStatus(value = HttpStatus.OK)
    @PreAuthorize("hasAuthority('" + WRITE + "')")
    public void createInvitations(@PathVariable(value = "id") String activityId, @RequestBody @Valid InvitationTextDto dto) {
        invitationService.createInvitationForActivity(activityId, dto.getText());
    }

    @Operation(summary = "Get invitations for an currently logged in user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Invitations found"),
    })
    @GetMapping(path = "/invitations")
    @ResponseStatus(value = HttpStatus.OK)
    @PreAuthorize("hasAuthority('" + READ + "')")
    public List<InvitationDetailDto> findMyInvitations() {
        return invitationService.findMyInvitations();
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ExceptionDto> handleResourceNotFoundException(HttpServletRequest req, Exception ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new ExceptionDto(ex.getMessage(), ex.getClass(), null, HttpStatus.NOT_FOUND));
    }

    @ExceptionHandler(OwnerValidationFailedException.class)
    public ResponseEntity<ExceptionDto> handleOwnerValidationFailedException(HttpServletRequest req, Exception ex) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN)
                .body(new ExceptionDto(ex.getMessage(), ex.getClass(), null, HttpStatus.FORBIDDEN));
    }
}
