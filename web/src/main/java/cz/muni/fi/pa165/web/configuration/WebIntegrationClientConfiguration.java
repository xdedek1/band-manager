package cz.muni.fi.pa165.web.configuration;

import cz.muni.fi.pa165.common.client.IntegrationClientConfiguration;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebIntegrationClientConfiguration extends IntegrationClientConfiguration {
}
