package cz.muni.fi.pa165.web.controller;

import cz.muni.fi.pa165.common.data.dto.album.AlbumCreateDto;
import cz.muni.fi.pa165.common.data.dto.album.AlbumDetailDto;
import cz.muni.fi.pa165.common.data.dto.album.AlbumUpdateDto;
import cz.muni.fi.pa165.common.exception.ExceptionDto;
import cz.muni.fi.pa165.common.exception.OwnerValidationFailedException;
import cz.muni.fi.pa165.common.exception.ResourceNotFoundException;
import cz.muni.fi.pa165.web.service.AlbumService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static cz.muni.fi.pa165.web.security.Scopes.READ;
import static cz.muni.fi.pa165.web.security.Scopes.WRITE;

@RestController
@RequestMapping("/albums")
public class AlbumController {

    private final AlbumService albumService;
    @Autowired
    public AlbumController(AlbumService albumService) {
        this.albumService = albumService;
    }

    @Operation(summary = "Get an album by its ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the album"),
            @ApiResponse(responseCode = "404", description = "Album not found"),
    })
    @GetMapping(path = "/{id}")
    @PreAuthorize("hasAuthority('" + READ + "')")
    public ResponseEntity<AlbumDetailDto> findById(@PathVariable(value = "id") String id) {
        return ResponseEntity.ok(albumService.findById(id));
    }

    @Operation(summary = "Get all albums by band ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the album"),
    })
    @GetMapping(path = "/band/{band-id}")
    @PreAuthorize("hasAuthority('" + READ + "')")
    public ResponseEntity<List<AlbumDetailDto>> findByBand(@PathVariable("band-id") String bandId) {
        return ResponseEntity.ok(albumService.findByBand(bandId));
    }

    @Operation(summary = "Create a new album")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Created the album"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
    })
    @PostMapping
    @PreAuthorize("hasAuthority('" + WRITE + "')")
    public ResponseEntity<AlbumDetailDto> create(@RequestBody @Valid AlbumCreateDto album) {
        return ResponseEntity.ok(albumService.create(album));
    }

    @Operation(summary = "Update an existing album")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated the album"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
            @ApiResponse(responseCode = "404", description = "Album not found"),
    })
    @PutMapping(path = "/{id}")
    @PreAuthorize("hasAuthority('" + WRITE + "')")
    public ResponseEntity<AlbumDetailDto> update(@PathVariable(value = "id") String id, @RequestBody @Valid AlbumUpdateDto album) {
        return ResponseEntity.ok(albumService.update(id, album));
    }

    @Operation(summary = "Delete an album")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Deleted the album"),
            @ApiResponse(responseCode = "404", description = "Album not found"),
    })
    @DeleteMapping(path = "/{id}")
    @PreAuthorize("hasAuthority('" + WRITE + "')")
    public ResponseEntity<Void> delete(@PathVariable(value = "id") String id) {
        return ResponseEntity.ok(albumService.delete(id));
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ExceptionDto> handleResourceNotFoundException(HttpServletRequest req, Exception ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new ExceptionDto(ex.getMessage(), ex.getClass(), null, HttpStatus.NOT_FOUND));
    }

    @ExceptionHandler(OwnerValidationFailedException.class)
    public ResponseEntity<ExceptionDto> handleOwnerValidationFailedException(HttpServletRequest req, Exception ex) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN)
                .body(new ExceptionDto(ex.getMessage(), ex.getClass(), null, HttpStatus.FORBIDDEN));
    }
}
