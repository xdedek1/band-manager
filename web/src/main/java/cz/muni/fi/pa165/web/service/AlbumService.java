package cz.muni.fi.pa165.web.service;

import cz.muni.fi.pa165.common.client.IntegrationClient;
import cz.muni.fi.pa165.common.data.dto.album.AlbumCreateDto;
import cz.muni.fi.pa165.common.data.dto.album.AlbumDetailDto;
import cz.muni.fi.pa165.common.data.dto.album.AlbumUpdateDto;
import cz.muni.fi.pa165.common.exception.ResourceNotFoundException;
import cz.muni.fi.pa165.web.utils.ValidationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;

import static cz.muni.fi.pa165.common.client.IntegrationClientConfiguration.ALBUMS;

@Service
public class AlbumService {

    private final IntegrationClient albumsClient;
    private final ValidationUtils validator;

    @Autowired
    public AlbumService(@Qualifier(ALBUMS) IntegrationClient albumsClient, ValidationUtils validator) {
        this.albumsClient = albumsClient;
        this.validator = validator;
    }

    public AlbumDetailDto findById(String id) {
        try {
            return albumsClient.doGet("/albums/" + id, new ParameterizedTypeReference<>() {
            });
        } catch (HttpClientErrorException.NotFound e) {
            throw new ResourceNotFoundException("Album with id " + id + " not found");
        }
    }

    public List<AlbumDetailDto> findByBand(String bandId) {
        return albumsClient.doGet("/albums/band/" + bandId, new ParameterizedTypeReference<>() {});
    }

    public AlbumDetailDto create(AlbumCreateDto album) {
        validator.validateBandManager(album.getBandId());
        return albumsClient.doPost("/albums", new ParameterizedTypeReference<>() {}, album);
    }

    public AlbumDetailDto update(String id, AlbumUpdateDto album) {
        AlbumDetailDto found = albumsClient.doGet("/albums/" + id, new ParameterizedTypeReference<>() {});
        if (found != null) {
            validator.validateBandManager(found.getBandId());
        }
        return albumsClient.doPut("/albums/" + id, AlbumDetailDto.class, album);
    }

    public Void delete(String id) {
        AlbumDetailDto found = albumsClient.doGet("/albums/" + id, new ParameterizedTypeReference<>() {});
        if (found != null) {
            validator.validateBandManager(found.getBandId());
        }
        return albumsClient.doDelete("/albums/" + id, Void.class);
    }
}
