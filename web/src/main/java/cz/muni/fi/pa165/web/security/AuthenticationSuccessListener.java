package cz.muni.fi.pa165.web.security;

import cz.muni.fi.pa165.common.data.dto.user.UserCreateDto;
import cz.muni.fi.pa165.common.exception.ResourceNotFoundException;
import cz.muni.fi.pa165.web.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class AuthenticationSuccessListener implements ApplicationListener<AuthenticationSuccessEvent> {
    private final UserService userService;

    public void onApplicationEvent(AuthenticationSuccessEvent event) {
        OidcUser oidcUser = (OidcUser) event.getAuthentication().getPrincipal();

        SecurityContextHolder.getContext().setAuthentication(event.getAuthentication());

        try {
           userService.findByEmail(oidcUser.getEmail());
        } catch (ResourceNotFoundException e) {
            UserCreateDto userCreateDto = new UserCreateDto();
            userCreateDto.setEmail(oidcUser.getEmail());
            userCreateDto.setFirstName(oidcUser.getGivenName());
            userCreateDto.setLastName(oidcUser.getFamilyName());
            userCreateDto.setId(oidcUser.getSubject());

            userService.register(userCreateDto);
        }
    }
}
