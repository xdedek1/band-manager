package cz.muni.fi.pa165.web.security;

import cz.muni.fi.pa165.common.data.dto.user.UserDetailDto;
import cz.muni.fi.pa165.web.service.UserService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Slf4j
@Controller
public class LoginController {

    public final UserService userService;

    @Autowired
    public LoginController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Home page accessible even to non-authenticated users. Displays user personal data.
     */
    @GetMapping("/")
    public String index(Model model, @AuthenticationPrincipal OidcUser user, HttpServletRequest request, Authentication authentication) {
        model.addAttribute("user", user);
        if (user != null) {
            model.addAttribute("issuerName", "MUNI");
            model.addAttribute("subjectIdentifier", user.getSubject());
            UserDetailDto userDetailDto = userService.findByEmail(user.getEmail());
            if (userDetailDto != null) {
                model.addAttribute("userDetail", userDetailDto);
            }
        }

        if (authentication != null && authentication.isAuthenticated()) {
            model.addAttribute("token", request.getRequestedSessionId());

        }


        return "index";
    }
}
