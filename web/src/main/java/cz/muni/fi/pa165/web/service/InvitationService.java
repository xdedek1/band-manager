package cz.muni.fi.pa165.web.service;

import cz.muni.fi.pa165.common.client.IntegrationClient;
import cz.muni.fi.pa165.common.data.dto.activity.ActivityDetailDto;
import cz.muni.fi.pa165.common.data.dto.activity.InvitationDetailDto;
import cz.muni.fi.pa165.common.data.dto.activity.InvitationMultipleCreateDto;
import cz.muni.fi.pa165.common.exception.ResourceNotFoundException;
import cz.muni.fi.pa165.web.utils.ValidationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;

import static cz.muni.fi.pa165.common.client.IntegrationClientConfiguration.ACTIVITIES;
import static cz.muni.fi.pa165.common.client.IntegrationClientConfiguration.BANDS;

@Service
public class InvitationService {

    private final IntegrationClient activitiesClient;
    private final IntegrationClient bandsClient;
    private final ValidationUtils validator;

    @Autowired
    public InvitationService(@Qualifier(ACTIVITIES) IntegrationClient activitiesClient, @Qualifier(BANDS) IntegrationClient bandsClient, ValidationUtils validator) {
        this.activitiesClient = activitiesClient;
        this.bandsClient = bandsClient;
        this.validator = validator;
    }

    public void createInvitationForActivity(String activityId, String text) {
        validator.validateUserManagerRole();
        ActivityDetailDto activity;
        try {
            activity = activitiesClient.doGet("/activities/" + activityId, new ParameterizedTypeReference<>() {});
        } catch (HttpClientErrorException.NotFound e) {
            throw new ResourceNotFoundException("Activity with id: " + activityId + " was not found.");
        }
        List<String> followersIds =  bandsClient.doGet("/bands/" + activity.getBandId() + "/followers", new ParameterizedTypeReference<>() {});

        if (followersIds.isEmpty()) {
            throw new ResourceNotFoundException("Band with id: " + activity.getBandId() + " has no followers.");
        }

        InvitationMultipleCreateDto createDto = new InvitationMultipleCreateDto();
        createDto.setActivityId(activityId);
        createDto.setReceiversIds(followersIds);
        createDto.setText(text);

        activitiesClient.doPost("/invitations/multiple", new ParameterizedTypeReference<>() {}, createDto);
    }

    public List<InvitationDetailDto> findMyInvitations() {
        return activitiesClient.doGet("/invitations", new ParameterizedTypeReference<>() {});
    }
}
