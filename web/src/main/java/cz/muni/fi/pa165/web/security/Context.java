package cz.muni.fi.pa165.web.security;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Component;

@Component
public class Context {

    public OidcUser getCurrentPrincipal() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof OidcUser) {
            return (OidcUser) principal;
        }

        throw new IllegalStateException("The current user is not a SecurityOidcUser");
    }
}
