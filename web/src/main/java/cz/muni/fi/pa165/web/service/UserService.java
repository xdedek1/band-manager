package cz.muni.fi.pa165.web.service;

import cz.muni.fi.pa165.common.client.IntegrationClient;
import cz.muni.fi.pa165.common.data.dto.user.UserCreateDto;
import cz.muni.fi.pa165.common.data.dto.user.UserDetailDto;
import cz.muni.fi.pa165.common.data.dto.user.UserUpdateDto;
import cz.muni.fi.pa165.common.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import static cz.muni.fi.pa165.common.client.IntegrationClientConfiguration.USERS;

@Service
public class UserService {

    private final IntegrationClient usersClient;

    @Autowired
    public UserService(@Qualifier(USERS) IntegrationClient usersClient) {
        this.usersClient = usersClient;
    }

    public UserDetailDto register(UserCreateDto user) {
        return usersClient.doPost("/users", new ParameterizedTypeReference<>() {}, user);
    }

    public Void markAsManager(String id) {
        return usersClient.doPut("/users/" + id + "/mark-as-manager", Void.class, null);
    }

    public UserDetailDto findById(String id) {
        try {
            return usersClient.doGet("/users/" + id, new ParameterizedTypeReference<>() {
            });
        } catch (HttpClientErrorException.NotFound e) {
            throw new ResourceNotFoundException("User with id " + id + " not found");
        }
    }

    public UserDetailDto findByEmail(String email) {
        try {
            return usersClient.doGet("/users/email/" + email, new ParameterizedTypeReference<>() {});
        } catch (HttpClientErrorException.NotFound e) {
            throw new ResourceNotFoundException("User with email " + email + " not found");
        }
    }

    public UserDetailDto update(UserUpdateDto user) {
        return usersClient.doPut("/users", UserDetailDto.class, user);
    }

    public Void delete() {
        return usersClient.doDelete("/users", Void.class);
    }
}
