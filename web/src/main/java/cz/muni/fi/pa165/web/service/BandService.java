package cz.muni.fi.pa165.web.service;

import cz.muni.fi.pa165.common.client.IntegrationClient;
import cz.muni.fi.pa165.common.data.dto.band.BandCreateDto;
import cz.muni.fi.pa165.common.data.dto.band.BandDetailDto;
import cz.muni.fi.pa165.common.data.dto.band.BandUpdateDto;
import cz.muni.fi.pa165.common.exception.ResourceNotFoundException;
import cz.muni.fi.pa165.web.utils.ValidationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;

import static cz.muni.fi.pa165.common.client.IntegrationClientConfiguration.BANDS;

@Service
public class BandService {

    private final IntegrationClient bandsClient;
    private final ValidationUtils validator;

    @Autowired
    public BandService(@Qualifier(BANDS) IntegrationClient bandsClient, ValidationUtils validator) {
        this.bandsClient = bandsClient;
        this.validator = validator;
    }

    public BandDetailDto findById(String id) {
        return bandsClient.doGet("/bands/" + id, new ParameterizedTypeReference<>() {});
    }

    public List<BandDetailDto> findMine() {
        return bandsClient.doGet("/bands", new ParameterizedTypeReference<>() {});
    }

    public BandDetailDto create(BandCreateDto createDto) {
        validator.validateUserManagerRole();
        return bandsClient.doPost("/bands", new ParameterizedTypeReference<>() {}, createDto);
    }

    public BandDetailDto update(String id,BandUpdateDto updateDto) {
        validator.validateBandManager(id);
        return bandsClient.doPut("/bands/" + id, BandDetailDto.class, updateDto);
    }

    public Void followBand(String bandId) {
        try {
            return bandsClient.doPost("/bands/" + bandId + "/follow", new ParameterizedTypeReference<>() {
            }, null);
        } catch (HttpClientErrorException.NotFound e) {
            throw new ResourceNotFoundException("Band with id: " + bandId + " was not found.");
        }
    }

    public Void delete(String id) {
        validator.validateBandManager(id);
        return bandsClient.doDelete("/bands/" + id, Void.class);
    }
}
