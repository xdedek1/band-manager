package cz.muni.fi.pa165.web.security;

import cz.muni.fi.pa165.common.client.AuthenticationProvider;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.client.authentication.OAuth2LoginAuthenticationToken;
import org.springframework.stereotype.Component;

@Component
public class Oauth2BearerTokenProvider implements AuthenticationProvider {

    private final OAuth2AuthorizedClientService authorizedClientService;

    public Oauth2BearerTokenProvider(OAuth2AuthorizedClientService authorizedClientService) {
        this.authorizedClientService = authorizedClientService;
    }

    @Override
    public HttpHeaders getAuthenticationHeaders() {
        HttpHeaders headers = new HttpHeaders();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication instanceof OAuth2LoginAuthenticationToken oauthToken) {
            headers.setBearerAuth(oauthToken.getAccessToken().getTokenValue());
        }

        if (authentication instanceof OAuth2AuthenticationToken) {
            OAuth2AuthorizedClient authorizedClient = authorizedClientService.loadAuthorizedClient(
                    "muni",
                    authentication.getName()
            );

            if (authorizedClient != null) {
                String accessToken = authorizedClient.getAccessToken().getTokenValue();
                headers.setBearerAuth(accessToken);
            }
        }

        return headers;
    }
}
