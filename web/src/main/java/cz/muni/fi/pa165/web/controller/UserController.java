package cz.muni.fi.pa165.web.controller;

import cz.muni.fi.pa165.common.data.dto.user.UserCreateDto;
import cz.muni.fi.pa165.common.data.dto.user.UserDetailDto;
import cz.muni.fi.pa165.common.data.dto.user.UserUpdateDto;
import cz.muni.fi.pa165.common.exception.ExceptionDto;
import cz.muni.fi.pa165.common.exception.OwnerValidationFailedException;
import cz.muni.fi.pa165.common.exception.ResourceNotFoundException;
import cz.muni.fi.pa165.web.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static cz.muni.fi.pa165.web.security.Scopes.READ;
import static cz.muni.fi.pa165.web.security.Scopes.WRITE;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @Operation(summary = "Get a user by its ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the user"),
            @ApiResponse(responseCode = "404", description = "User not found"),
    })
    @GetMapping(path = "/{id}")
    @PreAuthorize("hasAuthority('" + READ + "')")
    public ResponseEntity<UserDetailDto> findById(@PathVariable(value = "id") String id) {
        return ResponseEntity.ok(userService.findById(id));
    }

    @Operation(summary = "Register a new user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Created the user"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
    })
    @PostMapping
    @PreAuthorize("hasAuthority('" + WRITE + "')")
    public ResponseEntity<UserDetailDto> register(@RequestBody UserCreateDto user) {
        return ResponseEntity.ok(userService.register(user));
    }

    @Operation(summary = "Update an existing user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated the user"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
            @ApiResponse(responseCode = "404", description = "User not found"),
    })
    @PutMapping
    @PreAuthorize("hasAuthority('" + WRITE + "')")
    public ResponseEntity<UserDetailDto> update(@RequestBody @Valid UserUpdateDto user) {
        return ResponseEntity.ok(userService.update(user));
    }

    @Operation(summary = "Delete a user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Deleted the user"),
            @ApiResponse(responseCode = "404", description = "User not found"),
    })
    @DeleteMapping
    @PreAuthorize("hasAuthority('" + WRITE + "')")
    public ResponseEntity<Void> delete() {
        return ResponseEntity.ok(userService.delete());
    }

    @Operation(summary = "Mark a user as a manager")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Marked the user as a manager"),
            @ApiResponse(responseCode = "404", description = "User not found"),
    })
    @PutMapping(path = "/{id}/mark-as-manager")
    @PreAuthorize("hasAuthority('" + WRITE + "')")
    public ResponseEntity<Void> markAsManager(@PathVariable(value = "id") String id) {
        return ResponseEntity.ok(userService.markAsManager(id));
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ExceptionDto> handleResourceNotFoundException(HttpServletRequest req, Exception ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new ExceptionDto(ex.getMessage(), ex.getClass(), null, HttpStatus.NOT_FOUND));
    }

    @ExceptionHandler(OwnerValidationFailedException.class)
    public ResponseEntity<ExceptionDto> handleOwnerValidationFailedException(HttpServletRequest req, Exception ex) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN)
                .body(new ExceptionDto(ex.getMessage(), ex.getClass(), null, HttpStatus.FORBIDDEN));
    }
}
