package cz.muni.fi.pa165.web.utils;

import cz.muni.fi.pa165.common.client.IntegrationClient;
import cz.muni.fi.pa165.common.exception.OwnerValidationFailedException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

import static cz.muni.fi.pa165.common.client.IntegrationClientConfiguration.BANDS;
import static cz.muni.fi.pa165.common.client.IntegrationClientConfiguration.USERS;

@Service
public class ValidationUtils {

    private final IntegrationClient usersClient;
    private final IntegrationClient bandsClient;

    public ValidationUtils(@Qualifier(USERS)IntegrationClient usersClient,
                           @Qualifier(BANDS) IntegrationClient bandsClient) {
        this.usersClient = usersClient;
        this.bandsClient = bandsClient;
    }

    public void validateBandManager(String bandId) {
        Boolean isBandManager = bandsClient.doGet("/bands/" + bandId + "/is-manager", new ParameterizedTypeReference<>() {});
        if (Boolean.FALSE.equals(isBandManager)) {
            throw new OwnerValidationFailedException("User is not a manager of this band");
        }
    }

    public void validateUserManagerRole() {
        Boolean isBandManager = usersClient.doGet("/users/is-logged-in-manager", new ParameterizedTypeReference<>() {});
        if (Boolean.FALSE.equals(isBandManager)) {
            throw new OwnerValidationFailedException("User is not a manager");
        }
    }
}
