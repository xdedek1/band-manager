package cz.muni.fi.pa165.web.security;

public class Scopes {
    public static final String READ = "SCOPE_test_read";
    public static final String WRITE = "SCOPE_test_write";
}
