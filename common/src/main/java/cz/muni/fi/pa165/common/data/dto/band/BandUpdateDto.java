package cz.muni.fi.pa165.common.data.dto.band;

import cz.muni.fi.pa165.common.data.dto.enums.MusicStyle;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Schema(description = "The DTO for band update")
@EqualsAndHashCode
@Getter
@Setter
public class BandUpdateDto {

    @Schema(description = "The unique ID of the band", example = "5c89f390-18ea-48c7-9b4f-addab2b25d50")
    @NotNull
    private String name;

    @Schema(description = "The description of the band", example = "The Beatles were an English rock band formed in Liverpool in 1960.")
    private String description;

    @Schema(description = "The music style of the band", example = "ROCK")
    @NotNull
    private MusicStyle musicStyle;
}