package cz.muni.fi.pa165.common.data.dto.enums;

public enum UserRole {
    USER,
    MANAGER;

    /**
     * Return Spring-security-like string representation of this role as an authority
     */
    public String getAuthority() {
        return "ROLE_" + name();
    }

    /**
     * String values of {@link UserRole}, can be used i.e. in {@link PreAuthorize} annotations
     */
    public static class Value {
        public static final String USER = UserRole.USER.name();

        public static final String MANAGER = UserRole.MANAGER.name();
    }
}
