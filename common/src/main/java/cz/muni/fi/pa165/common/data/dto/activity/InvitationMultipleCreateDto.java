package cz.muni.fi.pa165.common.data.dto.activity;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Schema(description = "Dto for creating multiple invitations")
@EqualsAndHashCode
@Getter
@Setter
public class InvitationMultipleCreateDto {
    @Schema(description = "Activity ID", example = "33b84b80-184c-4f0e-b9ce-179e67c06177")
    @NotNull
    private String activityId;

    @Schema(description = "Receivers IDs", example = "[ 33b84b80-184c-4f0e-b9ce-179e67c06177, 33b84b80-184c-4f0e-b9ce-179e67c06178 ]")
    @NotEmpty
    private List<String> receiversIds;

    @Schema(description = "Invitation text", example = "We invite you to our concert in London.")
    @NotNull
    private String text;
}
