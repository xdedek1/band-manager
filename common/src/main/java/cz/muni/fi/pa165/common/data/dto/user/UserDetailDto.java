package cz.muni.fi.pa165.common.data.dto.user;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Set;

@Schema(description = "The DTO for user details")
@Data
public class UserDetailDto {


    @Schema(description = "The ID of the user", example = "5c89f390-18ea-48c7-9b4f-addab2b25d50")
    private String id;

    @Schema(description = "The first name of the user", example = "John")
    private String firstName;

    @Schema(description = "The last name of the user", example = "Doe")
    private String lastName;

    @Schema(description = "The email of the user", example = "em@email.com")
    private String email;

    @Schema(description = "The roles of the user", example = "USER")
    private Set<String> roles;
}
