package cz.muni.fi.pa165.common.client;

import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.function.Function;

@RequiredArgsConstructor
public class IntegrationClient {
    private final RestTemplate restTemplate;
    private final String serverUrl;
    private final AuthenticationProvider authenticationProvider;

    public <RESPONSE> RESPONSE doGet(@NonNull String urlPath, @NonNull ParameterizedTypeReference<RESPONSE> responseType) {
        ClientRequestBuilder<RESPONSE> crb = ClientRequestBuilder.<RESPONSE>get()
                .serverUrl(serverUrl)
                .withDefaultHeaders()
                .responseType(responseType);

        return doExecute(crb, urlPath);
    }

    public <RESPONSE> RESPONSE doGet(@NonNull String urlPath, @NonNull ParameterizedTypeReference<RESPONSE> responseType, @NonNull MultiValueMap<String, String> queryParams) {
        ClientRequestBuilder<RESPONSE> crb = ClientRequestBuilder.<RESPONSE>get()
                .serverUrl(serverUrl)
                .queryParams(queryParams)
                .withDefaultHeaders()
                .responseType(responseType);

        return doExecute(crb, urlPath);
    }

    public <REQUEST, RESPONSE> RESPONSE doPost(@NonNull String urlPath, @NonNull ParameterizedTypeReference<RESPONSE> responseType, @Nullable REQUEST payload) {
        ClientRequestBuilder<RESPONSE> crb = ClientRequestBuilder.<RESPONSE>post()
                .serverUrl(serverUrl)
                .withDefaultHeaders()
                .payload(payload)
                .responseType(responseType);

        return doExecute(crb, urlPath);
    }


    public <REQUEST, RESPONSE> RESPONSE doPut(@NonNull String urlPath, @NonNull Class<RESPONSE> responseType, @Nullable REQUEST payload) {
        ClientRequestBuilder<RESPONSE> crb = ClientRequestBuilder.<RESPONSE>put()
                .serverUrl(serverUrl)
                .withDefaultHeaders()
                .payload(payload)
                .responseType(ParameterizedTypeReference.forType(responseType));

        return doExecute(crb, urlPath);
    }

    public <REQUEST, RESPONSE> RESPONSE doPut(@NonNull String urlPath, @NonNull Class<RESPONSE> responseType, @Nullable REQUEST payload, HttpHeaders headers) {
        ClientRequestBuilder<RESPONSE> crb = ClientRequestBuilder.<RESPONSE>put()
                .serverUrl(serverUrl)
                .withDefaultHeaders()
                .addHeaders(headers)
                .payload(payload)
                .responseType(ParameterizedTypeReference.forType(responseType));

        return doExecute(crb, urlPath);
    }

    public <RESPONSE> RESPONSE doDelete(@NonNull String urlPath, @NonNull Class<RESPONSE> responseType) {
        ClientRequestBuilder<RESPONSE> crb = ClientRequestBuilder.<RESPONSE>delete()
                .serverUrl(serverUrl)
                .responseType(ParameterizedTypeReference.forType(responseType));

        return doExecute(crb, urlPath);
    }

    public <RESPONSE> RESPONSE doDelete(@NonNull String urlPath, @NonNull Class<RESPONSE> responseType, HttpHeaders headers) {
        ClientRequestBuilder<RESPONSE> crb = ClientRequestBuilder.<RESPONSE>delete()
                .serverUrl(serverUrl)
                .addHeaders(headers)
                .responseType(ParameterizedTypeReference.forType(responseType));

        return doExecute(crb, urlPath);
    }

    private <RESPONSE> RESPONSE doExecute(@NonNull ClientRequestBuilder<RESPONSE> requestBuilder, @NonNull String urlPath) {
        requestBuilder.addHeaders(authenticationProvider.getAuthenticationHeaders());
        return requestBuilder.execute(restTemplate, urlPath);
    }

    public <RESPONSE> RESPONSE execute(@NonNull Function<RestTemplate, RESPONSE> callback) {
        return callback.apply(restTemplate);
    }
}
