package cz.muni.fi.pa165.common.data;

import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

/**
 * Base entity class for all entities.
 */
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@MappedSuperclass
public abstract class BasicEntity {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();
}
