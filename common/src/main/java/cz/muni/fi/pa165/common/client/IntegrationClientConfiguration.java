package cz.muni.fi.pa165.common.client;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

public class IntegrationClientConfiguration {
    public static final String BANDS = "BANDS";
    public static final String ACTIVITIES = "ACTIVITIES";
    public static final String USERS = "USERS";
    public static final String ALBUMS = "ALBUMS";

    @Value("${spring.integration.bands.url:false}")
    private String bandsUrl;

    @Value("${spring.integration.activities.url:false}")
    private String activitiesUrl;

    @Value("${spring.integration.users.url:false}")
    private String usersUrl;

    @Value("${spring.integration.albums.url:false}")
    private String albumsUrl;

    @Bean
    @ConditionalOnProperty(name = "spring.integration.bands.url")
    @Qualifier(BANDS)
    public IntegrationClient bandsIntegrationClient(AuthenticationProvider authenticationProvider) {
        RestTemplate restTemplate = new RestTemplate(); //TODO - maybe create bean for RestTemplates
        return new IntegrationClient(restTemplate, bandsUrl, authenticationProvider);
    }

    @Bean
    @ConditionalOnProperty(name = "spring.integration.activities.url")
    @Qualifier(ACTIVITIES)
    public IntegrationClient activitiesIntegrationClient(AuthenticationProvider authenticationProvider) {
        RestTemplate restTemplate = new RestTemplate(); //TODO - maybe create bean for RestTemplates
        return new IntegrationClient(restTemplate, activitiesUrl, authenticationProvider);
    }

    @Bean
    @ConditionalOnProperty(name = "spring.integration.users.url")
    @Qualifier(USERS)
    public IntegrationClient usersIntegrationClient(AuthenticationProvider authenticationProvider) {
        RestTemplate restTemplate = new RestTemplate(); //TODO - maybe create bean for RestTemplates
        return new IntegrationClient(restTemplate, usersUrl, authenticationProvider);
    }

    @Bean
    @ConditionalOnProperty(name = "spring.integration.albums.url")
    @Qualifier(ALBUMS)
    public IntegrationClient albumsIntegrationClient(AuthenticationProvider authenticationProvider) {
        RestTemplate restTemplate = new RestTemplate(); //TODO - maybe create bean for RestTemplates
        return new IntegrationClient(restTemplate, albumsUrl, authenticationProvider);
    }
}
