package cz.muni.fi.pa165.common.data.dto.album.Song;

import cz.muni.fi.pa165.common.data.dto.enums.MusicStyle;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Schema(description = "Dto for song details withing album")
@EqualsAndHashCode
@Getter
@Setter
public class SongDetailDto {

    @Schema(description = "Title of the song", example = "Time")
    private String title;

    @Schema(description = "Duration of the song in seconds", example = "300")
    private int duration;

    @Schema(description = "Genre of the song", example = "ROCK")
    private MusicStyle genre;
}
