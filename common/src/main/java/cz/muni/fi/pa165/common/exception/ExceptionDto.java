package cz.muni.fi.pa165.common.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class ExceptionDto {
    private String message;
    private Class<? extends Throwable> exceptionType;
    private String stackTrace;
    private HttpStatus status;

    public ExceptionDto(String message, Class<? extends Throwable> exceptionType, String stackTrace, HttpStatus status) {
        this.message = message;
        this.exceptionType = exceptionType;
        this.stackTrace = stackTrace;
        this.status = status;
    }
}
