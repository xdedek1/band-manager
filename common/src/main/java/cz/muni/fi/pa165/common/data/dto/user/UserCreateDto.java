package cz.muni.fi.pa165.common.data.dto.user;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Schema(description = "The DTO for user creation")
@Data
public class UserCreateDto {

    @Schema(description = "The ID of an user")
    @NotNull
    private String id;

    @Schema(description = "The first name of the user", example = "John")
    @Size(min = 2, max = 256)
    private String firstName;

    @Schema(description = "The last name of the user", example = "Doe")
    @Size(min = 2, max = 256)
    private String lastName;

    @Schema(description = "The email of the user", example = "em@email.com")
    @Email
    @NotNull
    private String email;
}
