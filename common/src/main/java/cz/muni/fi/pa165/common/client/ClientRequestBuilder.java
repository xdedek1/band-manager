package cz.muni.fi.pa165.common.client;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

/**
 * Builder for creating client requests.
 *
 * @param <RESPONSE> type of the response
 */
public class ClientRequestBuilder<RESPONSE> {
    private final HttpMethod method;

    /**
     * Server url address.
     */
    private String serverUrl;

    /**
     * Query parameters. Will be appended to {@link #serverUrl}. Use in conjunction with {@link #uriVariables} to set more
     * complex params (i.e. values that need to be escaped for URL).
     */
    private MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();

    /**
     * Payload for the request.
     */
    private Object payload;

    /**
     * Expected response type.
     */
    private ParameterizedTypeReference<RESPONSE> responseType;

    /**
     * Headers for the request.
     */
    private HttpHeaders headers = new HttpHeaders();

    private ClientRequestBuilder(HttpMethod method) {
        this.method = method;
    }

    public static <RESPONSE> ClientRequestBuilder<RESPONSE> get() {
        return new ClientRequestBuilder<>(HttpMethod.GET);
    }

    public static <RESPONSE> ClientRequestBuilder<RESPONSE>  post() {
        return new ClientRequestBuilder<>(HttpMethod.POST);
    }

    public static <RESPONSE> ClientRequestBuilder<RESPONSE>  put() {
        return new ClientRequestBuilder<>(HttpMethod.PUT);
    }

    public static <RESPONSE> ClientRequestBuilder<RESPONSE>  delete() {
        return new ClientRequestBuilder<>(HttpMethod.DELETE);
    }

    public ClientRequestBuilder<RESPONSE> serverUrl(String serverUrl) {
        this.serverUrl = serverUrl;
        return this;
    }

    public ClientRequestBuilder<RESPONSE> queryParam(String key, List<String> value) {
        this.queryParams.put(key, value);
        return this;
    }

    public ClientRequestBuilder<RESPONSE> queryParams(MultiValueMap<String, String> queryParams) {
        this.queryParams.putAll(queryParams);
        return this;
    }

    public ClientRequestBuilder<RESPONSE> payload(Object payload) {
        this.payload = payload;
        return this;
    }

    public ClientRequestBuilder<RESPONSE> responseType(ParameterizedTypeReference<RESPONSE> responseType) {
        this.responseType = responseType;
        return this;
    }

    public ClientRequestBuilder<RESPONSE> withDefaultHeaders() {
        headers.setContentType(MediaType.APPLICATION_JSON);
        return this;
    }

    public ClientRequestBuilder<RESPONSE> addHeaders(HttpHeaders headers) {
        headers.forEach((key, value) -> this.headers.addAll(key, value));
        return this;
    }

    public RESPONSE execute(RestTemplate restTemplate, String serverEndpoint) {
        return executeEntity(restTemplate, serverEndpoint).getBody();
    }

    public ResponseEntity<RESPONSE> executeEntity(RestTemplate restTemplate, String serverEndpoint) {
        String uri = UriComponentsBuilder.newInstance()
                .scheme("http")
                .host(serverUrl)
                .queryParams(queryParams)
                .path(serverEndpoint)
                .build().toUriString();

        HttpEntity<?> request = new HttpEntity<>(payload, headers);

        return restTemplate.exchange(uri, method, request, responseType);
    }
}
