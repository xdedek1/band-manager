package cz.muni.fi.pa165.common.data.dto.album;

import cz.muni.fi.pa165.common.data.dto.album.Song.SongEditDto;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Schema(description = "Dto for updating an album")
@EqualsAndHashCode
@Getter
@Setter
public class AlbumUpdateDto {

    @Schema(description = "Name of the album", example = "The Dark Side of the Moon")
    @NotNull
    private String name;

    @Schema(description = "Description of the album", example = "The Dark Side of the Moon is the eighth studio album by the English rock band Pink Floyd, released on 1 March 1973 by Harvest Records.")
    private String description;

    @Schema(description = "Release date of the album", example = "1973-03-01")
    @NotNull
    private LocalDate releaseDate;

    @Schema(description = "List of songs in the album")
    @NotNull
    private List<SongEditDto> songs;
}
