package cz.muni.fi.pa165.common.exception;

public class OwnerValidationFailedException extends RuntimeException {
    public OwnerValidationFailedException(String string) {
        super(string);
    }

}
