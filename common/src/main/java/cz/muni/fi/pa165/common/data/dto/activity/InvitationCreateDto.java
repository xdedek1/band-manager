package cz.muni.fi.pa165.common.data.dto.activity;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Schema(description = "Dto for creating an invitation")
@EqualsAndHashCode
@Getter
@Setter
public class InvitationCreateDto {
    @Schema(description = "Activity ID", example = "33b84b80-184c-4f0e-b9ce-179e67c06177")
    @NotNull
    private String activityId;

    @Schema(description = "Receiver ID", example = "33b84b80-184c-4f0e-b9ce-179e67c06177")
    @NotNull
    private String receiverId;

    @Schema(description = "Invitation text", example = "We invite you to our concert in London.")
    @NotNull
    private String text;
}