package cz.muni.fi.pa165.common.client;

import org.springframework.http.HttpHeaders;

public interface AuthenticationProvider {

    HttpHeaders getAuthenticationHeaders();
}
