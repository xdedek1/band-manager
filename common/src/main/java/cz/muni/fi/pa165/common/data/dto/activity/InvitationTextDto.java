package cz.muni.fi.pa165.common.data.dto.activity;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Schema(description = "Dto for invitation text")
@EqualsAndHashCode
@Getter
@Setter
public class InvitationTextDto {

    @NotBlank
    private String text;
}
