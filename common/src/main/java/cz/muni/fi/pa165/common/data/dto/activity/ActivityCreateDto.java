package cz.muni.fi.pa165.common.data.dto.activity;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Schema(description = "Dto for creating an activity")
@EqualsAndHashCode
@Getter
@Setter
public class ActivityCreateDto {

    @Schema(description = "Band ID", example = "33b84b80-184c-4f0e-b9ce-179e67c06177")
    @NotNull
    private String bandId;

    @Schema(description = "Name of the activity", example = "Concert")
    @NotNull
    private String name;

    @Schema(description = "Description of the activity", example = "The band will play their greatest hits.")
    private String description;

    @Schema(description = "Start time of the activity", example = "2021-12-24T18:00:00")
    @NotNull
    private LocalDateTime startTime;

    @Schema(description = "End time of the activity", example = "2021-12-24T22:00:00")
    @NotNull
    private LocalDateTime endTime;
}
