package cz.muni.fi.pa165.common.data.dto.band;

import cz.muni.fi.pa165.common.data.dto.enums.MusicStyle;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Schema(description = "The detailed information about the band")
@EqualsAndHashCode
@Getter
@Setter
public class BandDetailDto {

    @Schema(description = "The unique ID of the band", example = "5c89f390-18ea-48c7-9b4f-addab2b25d50")
    private String id;

    @Schema(description = "The unique ID of the band manager", example = "5c89f390-18ea-48c7-9b4f-addab2b25d50")
    private String managerId;

    @Schema(description = "The name of the band", example = "The Beatles")
    private String name;

    @Schema(description = "The description of the band", example = "The Beatles were an English rock band formed in Liverpool in 1960.")
    private String description;

    @Schema(description = "The music style of the band", example = "ROCK")
    private MusicStyle musicStyle;
}
