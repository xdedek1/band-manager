package cz.muni.fi.pa165.common.data.factory;

import cz.muni.fi.pa165.common.data.dto.enums.UserRole;
import cz.muni.fi.pa165.common.data.dto.user.UserCreateDto;
import cz.muni.fi.pa165.common.data.dto.user.UserDetailDto;
import cz.muni.fi.pa165.common.data.dto.user.UserUpdateDto;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class UserDtoFactory {
    public static UserCreateDto createUserCreateDto() {
        UserCreateDto dto = new UserCreateDto();
        dto.setFirstName("First name");
        dto.setLastName("Last name");
        dto.setEmail("example@pa165.example.com");
        dto.setId("subject-id-test");
        return dto;
    }

    public static UserDetailDto createUserDetailDto() {
        UserDetailDto dto = new UserDetailDto();
        dto.setId("1");
        dto.setFirstName("First name");
        dto.setLastName("Last name");
        dto.setEmail("example@example.com");
        dto.setRoles(Set.of(UserRole.Value.USER));
        return dto;
    }

    public static UserUpdateDto createUserUpdateDto() {
        UserUpdateDto dto = new UserUpdateDto();
        dto.setFirstName("Updated User First Name");
        dto.setLastName("Updated User Last Name");
        dto.setEmail("updated@example.com");
        return dto;
    }
}
