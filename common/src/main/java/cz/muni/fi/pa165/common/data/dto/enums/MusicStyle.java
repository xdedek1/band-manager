package cz.muni.fi.pa165.common.data.dto.enums;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Music style")
public enum MusicStyle {
    POP,
    ROCK,
    ALT,
    METAL,
    INDIE,
    ELECTRONIC,
    DISCO,
    COUNTRY,
    FOLK,
    HIPHOP,
}
