package cz.muni.fi.pa165.common.data.dto.activity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Schema(description = "Dto for activity details")
@EqualsAndHashCode
@Getter
@Setter
public class ActivityDetailDto {

    @Schema(description = "ID of the activity", example = "5c89f390-18ea-48c7-9b4f-addab2b25d50")
    private String id;

    @Schema(description = "Owner ID", example = "5c89f390-18ea-48c7-9b4f-addab2b25d50")
    private String ownerId;

    @Schema(description = "Band ID", example = "33b84b80-184c-4f0e-b9ce-179e67c06177")
    private String bandId;

    @Schema(description = "Name of the activity", example = "Concert")
    private String name;

    @Schema(description = "Description of the activity", example = "The band will play their greatest hits.")
    private String description;

    @Schema(description = "Start time of the activity", example = "2021-12-24T18:00:00")
    private LocalDateTime startTime;

    @Schema(description = "End time of the activity", example = "2021-12-24T22:00:00")
    private LocalDateTime endTime;
}
