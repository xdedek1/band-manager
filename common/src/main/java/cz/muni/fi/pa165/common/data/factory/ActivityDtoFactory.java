package cz.muni.fi.pa165.common.data.factory;

import cz.muni.fi.pa165.common.data.dto.activity.ActivityCreateDto;
import cz.muni.fi.pa165.common.data.dto.activity.ActivityDetailDto;
import cz.muni.fi.pa165.common.data.dto.activity.ActivityUpdateDto;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class ActivityDtoFactory {
    public static ActivityCreateDto createActivityCreateDto() {
        ActivityCreateDto dto = new ActivityCreateDto();
        dto.setBandId("bandId");
        dto.setName("Activity Name");
        dto.setDescription("Activity Description");
        dto.setStartTime(LocalDateTime.now());
        dto.setEndTime(LocalDateTime.now().plusHours(1));
        return dto;
    }

    public static ActivityDetailDto createActivityDetailDto() {
        ActivityDetailDto dto = new ActivityDetailDto();
        dto.setId("1");
        dto.setOwnerId("ownerId");
        dto.setBandId("bandId");
        dto.setName("Activity Name");
        dto.setDescription("Activity Description");
        dto.setStartTime(LocalDateTime.now());
        dto.setEndTime(LocalDateTime.now().plusHours(1));
        return dto;
    }

    public static ActivityUpdateDto createActivityUpdateDto() {
        ActivityUpdateDto dto = new ActivityUpdateDto();
        dto.setName("Updated Activity Name");
        dto.setDescription("Updated Activity Description");
        dto.setStartTime(LocalDateTime.now().plusDays(1));
        dto.setEndTime(LocalDateTime.now().plusDays(1).plusHours(1));
        return dto;
    }
}
