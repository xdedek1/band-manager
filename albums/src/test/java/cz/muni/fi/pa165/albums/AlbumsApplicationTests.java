package cz.muni.fi.pa165.albums;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = AlbumsApplication.class)
class AlbumsApplicationTests {

    @Test
    void contextLoads() {
    }

}
