package cz.muni.fi.pa165.albums.utils;

import cz.muni.fi.pa165.common.data.dto.album.AlbumCreateDto;
import cz.muni.fi.pa165.common.data.dto.album.AlbumDetailDto;
import cz.muni.fi.pa165.common.data.dto.album.AlbumUpdateDto;
import cz.muni.fi.pa165.common.data.dto.album.Song.SongEditDto;
import cz.muni.fi.pa165.common.data.dto.enums.MusicStyle;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

import static cz.muni.fi.pa165.albums.utils.AlbumDataFactory.TEST_ALBUM_ID;
import static cz.muni.fi.pa165.albums.utils.AlbumDataFactory.TEST_BAND_ID;
import static cz.muni.fi.pa165.albums.utils.AlbumDataFactory.TEST_MANAGER_ID;

@Component
public class AlbumDtoFactory {

    //Generate dtos for testing
    public static AlbumCreateDto createAlbumCreateDto() {
        AlbumCreateDto dto = new AlbumCreateDto();
        dto.setName("Album Title");
        dto.setReleaseDate(LocalDate.of(2020, 1, 1));
        dto.setBandId(TEST_BAND_ID);
        dto.setDescription("Test Description");

        SongEditDto song = new SongEditDto();
        song.setTitle("Test Song");
        song.setDuration(180);
        song.setGenre(MusicStyle.COUNTRY);
        dto.setSongs(List.of(song));
        return dto;
    }

    public static AlbumUpdateDto createAlbumUpdateDto() {
        AlbumUpdateDto dto = new AlbumUpdateDto();
        dto.setName("Updated Album Title");
        dto.setReleaseDate(LocalDate.of(2021, 1, 1));
        dto.setDescription("Updated Test Description");

        SongEditDto song = new SongEditDto();
        song.setTitle("Test Song");
        song.setDuration(180);
        song.setGenre(MusicStyle.COUNTRY);
        dto.setSongs(List.of(song));
        dto.setSongs(List.of(song));
        return dto;
    }

    public static AlbumDetailDto createAlbumDetailDto() {
        AlbumDetailDto dto = new AlbumDetailDto();
        dto.setId(TEST_ALBUM_ID);
        dto.setOwnerId(TEST_MANAGER_ID);
        dto.setName("Album Title");
        dto.setReleaseDate(LocalDate.of(2020, 1, 1));
        dto.setBandId(TEST_BAND_ID);
        dto.setDescription("Test Description");

        SongEditDto song = new SongEditDto();
        song.setTitle("Test Song");
        song.setDuration(180);
        song.setGenre(MusicStyle.COUNTRY);
        dto.setSongs(List.of(song));
        return dto;
    }

    public static AlbumDetailDto createAlbumUpdatedDetailDto() {
        AlbumDetailDto dto = new AlbumDetailDto();
        dto.setId(TEST_ALBUM_ID);
        dto.setName("Updated Album Title");
        dto.setReleaseDate(LocalDate.of(2021, 1, 1));
        dto.setBandId(TEST_BAND_ID);
        dto.setDescription("Updated Test Description");

        SongEditDto song = new SongEditDto();
        song.setTitle("Test Song");
        song.setDuration(180);
        song.setGenre(MusicStyle.COUNTRY);
        dto.setSongs(List.of(song));
        return dto;
    }
}
