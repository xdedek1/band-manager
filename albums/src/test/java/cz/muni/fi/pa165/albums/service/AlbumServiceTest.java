package cz.muni.fi.pa165.albums.service;

import cz.muni.fi.pa165.albums.data.model.Album;
import cz.muni.fi.pa165.albums.repository.AlbumRepository;
import cz.muni.fi.pa165.albums.security.Context;
import cz.muni.fi.pa165.albums.utils.AlbumDataFactory;
import cz.muni.fi.pa165.common.exception.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static cz.muni.fi.pa165.albums.utils.AlbumDataFactory.TEST_ALBUM_ID;
import static cz.muni.fi.pa165.albums.utils.AlbumDataFactory.TEST_BAND_ID;
import static cz.muni.fi.pa165.albums.utils.AlbumDataFactory.TEST_MANAGER_ID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AlbumServiceTest {

    private final Album TEST_ALBUM = AlbumDataFactory.createAlbum();

    @Mock
    private AlbumRepository albumRepository;
    @Mock
    private Context context;
    @InjectMocks
    private AlbumService albumService;

    @Test
    public void findById_albumFound_returnsAlbum() {
        // Arrange
        Album mockAlbum = TEST_ALBUM;
        mockAlbum.setId(TEST_ALBUM_ID);
        when(albumRepository.findById(TEST_ALBUM_ID)).thenReturn(Optional.of(mockAlbum));

        // Act
        Album foundAlbum = albumService.findById(TEST_ALBUM_ID);

        // Assert
        assertNotNull(foundAlbum);
        assertThat(foundAlbum).isEqualTo(TEST_ALBUM);
    }

    @Test
    public void findById_albumNotFound_throwsResourceNotFoundException() {
        // Arrange
        String id = "non-existing-id";
        when(albumRepository.findById(id)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(ResourceNotFoundException.class, () -> albumService.findById(id));
    }

    @Test
    public void findByBand_albumsFound_returnsAlbums() {
        // Arrange
        List<Album> mockAlbums = new ArrayList<>();
        mockAlbums.add(new Album());
        mockAlbums.add(new Album());
        when(albumRepository.findByBand(TEST_BAND_ID)).thenReturn(mockAlbums);

        // Act
        List<Album> foundAlbums = albumService.findByBand(TEST_BAND_ID);

        // Assert
        assertEquals(mockAlbums.size(), foundAlbums.size());
    }

    @Test
    void findByBand_activitiesNotFound_returnsEmptyList() {
        // Arrange
        List<Album> emptyList = Collections.emptyList();

        when(albumRepository.findByBand(TEST_ALBUM.getBandId())).thenReturn(emptyList);

        // Act
        List<Album> foundActivities = albumService.findByBand(TEST_ALBUM.getBandId());

        // Assert
        assertThat(foundActivities).isEmpty();
    }

    @Test
    public void create_albumCreated_returnsAlbum() {
        // Arrange
        Album toCreate = TEST_ALBUM;
        when(albumRepository.save(toCreate)).thenReturn(toCreate);

        // Act
        Album createdAlbum = albumService.create(toCreate);

        // Assert
        assertNotNull(createdAlbum);
        assertEquals(toCreate, createdAlbum);
    }

    @Test
    void create_loggedUserIdIsNull_returnsNull() {
        // Arrange
        when(context.getLoggedInUserId()).thenReturn(null);

        // Act
        Album createdAlbum = albumService.create(TEST_ALBUM);

        // Assert
        assertThat(createdAlbum).isNull();
    }

    @Test
    public void update_albumExists_returnsUpdatedAlbum() {
        // Arrange
        Album updatedAlbum = TEST_ALBUM;
        updatedAlbum.setDescription("Updated Desscription");
        when(albumRepository.findById(TEST_ALBUM_ID)).thenReturn(Optional.of(TEST_ALBUM));
        when(albumRepository.save(updatedAlbum)).thenReturn(updatedAlbum);

        // Act
        Album result = albumService.update(TEST_ALBUM_ID, updatedAlbum);

        // Assert
        assertNotNull(result);
        assertEquals(updatedAlbum, result);
    }

    @Test
    public void update_albumNotExists_throwsResourceNotFoundException() {
        // Arrange
        String id ="non-existing-id";
        Album toUpdate = new Album();
        when(albumRepository.findById(id)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(ResourceNotFoundException.class, () -> albumService.update(id, toUpdate));
    }

    @Test
    public void delete_albumExists_deletesAlbum() {
        // Arrange
        Album existingAlbum = TEST_ALBUM;
        when(albumRepository.findById(TEST_ALBUM_ID)).thenReturn(Optional.of(existingAlbum));

        // Act
        albumService.delete(TEST_ALBUM_ID);

        // Assert
        verify(albumRepository, times(1)).delete(existingAlbum);
    }

    @Test
    public void delete_albumNotExists_returnsResourceNotFoundException() {
        // Arrange
        String id = "non-existing-id";
        when(albumRepository.findById(id)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(ResourceNotFoundException.class, () -> albumService.delete(id));
    }
}