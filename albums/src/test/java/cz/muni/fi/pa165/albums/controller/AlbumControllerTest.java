package cz.muni.fi.pa165.albums.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.common.data.dto.album.AlbumCreateDto;
import cz.muni.fi.pa165.common.data.dto.album.AlbumDetailDto;
import cz.muni.fi.pa165.common.data.dto.album.AlbumUpdateDto;
import cz.muni.fi.pa165.albums.facade.AlbumFacade;
import cz.muni.fi.pa165.albums.utils.AlbumDtoFactory;
import cz.muni.fi.pa165.common.exception.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static cz.muni.fi.pa165.albums.utils.AlbumDataFactory.TEST_ALBUM_ID;
import static cz.muni.fi.pa165.albums.utils.AlbumDataFactory.TEST_BAND_ID;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class AlbumControllerTest {

    private final AlbumDetailDto TEST_DETAIL_DTO = AlbumDtoFactory.createAlbumDetailDto();
    private final AlbumCreateDto TEST_CREATE_DTO = AlbumDtoFactory.createAlbumCreateDto();
    private final AlbumUpdateDto TEST_UPDATE_DTO = AlbumDtoFactory.createAlbumUpdateDto();
    private final AlbumDetailDto TEST_UPDATED_DETAIL_DTO = AlbumDtoFactory.createAlbumUpdatedDetailDto();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AlbumFacade albumFacade;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void findById_albumFound_returnsAlbumDetailDto() throws Exception {
        // Arrange
        AlbumDetailDto expectedDto = TEST_DETAIL_DTO;

        when(albumFacade.findById(TEST_ALBUM_ID)).thenReturn(expectedDto);

        // Act
        ResultActions result = mockMvc.perform(get("/albums/{id}", TEST_ALBUM_ID).contentType(MediaType.APPLICATION_JSON));

        // Assert
        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(expectedDto.getId()))
                .andExpect(jsonPath("$.name").value(expectedDto.getName()))
                .andExpect(jsonPath("$.description").value(expectedDto.getDescription()))
                .andExpect(jsonPath("$.releaseDate").value(expectedDto.getReleaseDate().toString()))
                .andExpect(jsonPath("$.bandId").value(expectedDto.getBandId()))
                .andExpect(jsonPath("$.songs[0].title").value(expectedDto.getSongs().get(0).getTitle()))
                .andExpect(jsonPath("$.songs[0].duration").value(expectedDto.getSongs().get(0).getDuration()))
                .andExpect(jsonPath("$.songs[0].genre").value(expectedDto.getSongs().get(0).getGenre().toString()));
    }

    @Test
    void findById_albumNotFound_returnsNotFound() throws Exception {
        String nonExistingId = "non-existent-id";

        when(albumFacade.findById(nonExistingId)).thenThrow(ResourceNotFoundException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/albums/{id}", nonExistingId))
                .andExpect(status().isNotFound());
    }

    @Test
    public void findByBand_bandExists_returnsListOfAlbumDetailDto() throws Exception {
        // Arrange
        List<AlbumDetailDto> expectedDtos = List.of(TEST_DETAIL_DTO);
        when(albumFacade.findByBand(TEST_BAND_ID)).thenReturn(expectedDtos);

        // Act
        ResultActions result = mockMvc.perform(get("/albums/band/{id}", TEST_BAND_ID).contentType(MediaType.APPLICATION_JSON));

        // Assert
        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(expectedDtos.get(0).getId()))
                .andExpect(jsonPath("$[0].name").value(expectedDtos.get(0).getName()))
                .andExpect(jsonPath("$[0].description").value(expectedDtos.get(0).getDescription()))
                .andExpect(jsonPath("$[0].releaseDate").value(expectedDtos.get(0).getReleaseDate().toString()))
                .andExpect(jsonPath("$[0].bandId").value(expectedDtos.get(0).getBandId()))
                .andExpect(jsonPath("$[0].songs[0].title").value(expectedDtos.get(0).getSongs().get(0).getTitle()))
                .andExpect(jsonPath("$[0].songs[0].duration").value(expectedDtos.get(0).getSongs().get(0).getDuration()));
    }

    @Test
    void findByBand_bandNotFound_returnsNotFound() throws Exception {
        String nonExistingId = "non-existent-id";

        when(albumFacade.findByBand(nonExistingId)).thenThrow(ResourceNotFoundException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/albums/band/{id}", nonExistingId))
                .andExpect(status().isNotFound());
    }

    @Test
    public void create_albumCreated_returnsAlbumDetailDto() throws Exception {
        // Arrange
        AlbumCreateDto createDto = TEST_CREATE_DTO;
        AlbumDetailDto expectedDto = TEST_DETAIL_DTO;

        when(albumFacade.create(createDto)).thenReturn(expectedDto);

        // Act
        ResultActions result = mockMvc.perform(post("/albums")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{" +
                        "\"name\":\"" + createDto.getName() + "\", " +
                        "\"bandId\":\"" + createDto.getBandId() + "\", " +
                        "\"description\":\"" + createDto.getDescription() + "\", " +
                        "\"releaseDate\":\"" + createDto.getReleaseDate() + "\"," +
                        "\"songs\":[{\"title\":\"" + createDto.getSongs().get(0).getTitle() +
                        "\", \"duration\":\"" + createDto.getSongs().get(0).getDuration() +
                        "\", \"genre\":\"" + createDto.getSongs().get(0).getGenre() +
                        "\"}]" +
                        "}")
        );

        // Assert
        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(expectedDto.getId()))
                .andExpect(jsonPath("$.name").value(expectedDto.getName()))
                .andExpect(jsonPath("$.description").value(expectedDto.getDescription()))
                .andExpect(jsonPath("$.releaseDate").value(expectedDto.getReleaseDate().toString()))
                .andExpect(jsonPath("$.bandId").value(expectedDto.getBandId()))
                .andExpect(jsonPath("$.songs[0].title").value(expectedDto.getSongs().get(0).getTitle()))
                .andExpect(jsonPath("$.songs[0].duration").value(expectedDto.getSongs().get(0).getDuration()))
                .andExpect(jsonPath("$.songs[0].genre").value(expectedDto.getSongs().get(0).getGenre().toString()));
    }

    @Test
    public void create_invalidInput_returnsBadRequest() throws Exception {
        // Arrange
        when(albumFacade.create(any()))
                .thenReturn(null);

        // Act & Assert
        mockMvc.perform(post("/albums")
                        .param("albumId", "test-album-id")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void update_albumExists_returnsUpdatedAlbumDetailDto() throws Exception {
        // Arrange
        AlbumUpdateDto updateDto = TEST_UPDATE_DTO;
        AlbumDetailDto expectedDto = TEST_UPDATED_DETAIL_DTO;

        when(albumFacade.update(TEST_BAND_ID, updateDto)).thenReturn(expectedDto);

        // Act
        ResultActions result = mockMvc.perform(put("/albums/{id}", TEST_BAND_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{" +
                        "\"name\":\"" + updateDto.getName() + "\", " +
                        "\"description\":\"" + updateDto.getDescription() + "\", " +
                        "\"releaseDate\":\"" + updateDto.getReleaseDate() + "\"," +
                        "\"songs\":[{\"title\":\"" + updateDto.getSongs().get(0).getTitle() +
                        "\", \"duration\":\"" + updateDto.getSongs().get(0).getDuration() +
                        "\", \"genre\":\"" + updateDto.getSongs().get(0).getGenre() +
                        "\"}]" +
                        "}")
        );

        // Assert
        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(expectedDto.getId()))
                .andExpect(jsonPath("$.name").value(expectedDto.getName()))
                .andExpect(jsonPath("$.description").value(expectedDto.getDescription()))
                .andExpect(jsonPath("$.releaseDate").value(expectedDto.getReleaseDate().toString()))
                .andExpect(jsonPath("$.bandId").value(expectedDto.getBandId()))
                .andExpect(jsonPath("$.songs[0].title").value(expectedDto.getSongs().get(0).getTitle()))
                .andExpect(jsonPath("$.songs[0].duration").value(expectedDto.getSongs().get(0).getDuration()))
                .andExpect(jsonPath("$.songs[0].genre").value(expectedDto.getSongs().get(0).getGenre().toString()));
    }

    @Test
    public void update_invalidInput_returnsBadRequest() throws Exception {
        // Arrange
        AlbumUpdateDto invalidUpdateDto = new AlbumUpdateDto();
        invalidUpdateDto.setName("");

        when(albumFacade.update(any(), any()))
                .thenReturn(null);

        // Act & Assert
        mockMvc.perform(put("/albums/{id}", TEST_UPDATED_DETAIL_DTO.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(invalidUpdateDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void update_albumNotFound_returnsNotFound() throws Exception {
        // Arrange
        String nonExistingId = "invalid-id";
        AlbumUpdateDto updateDto = TEST_UPDATE_DTO;

        when(albumFacade.update(nonExistingId, updateDto)).thenThrow(ResourceNotFoundException.class);

        // Act & Assert
        mockMvc.perform(put("/albums/{id}", nonExistingId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void delete_albumFound_returnsNoContent() throws Exception {
        // Arrange
        String id = TEST_ALBUM_ID;

        // Act
        ResultActions resultActions = mockMvc.perform(delete("/albums/{id}", id));

        // Assert
        resultActions.andExpect(status().isNoContent());
        verify(albumFacade, times(1)).delete(id);
    }

    @Test
    public void delete_albumNotFound_returnsNotFound() throws Exception {
        // Arrange
        String nonExistingId = "invalid-id";

        doThrow(ResourceNotFoundException.class).when(albumFacade).delete(nonExistingId);

        // Act & Assert
        mockMvc.perform(delete("/albums/{id}", nonExistingId))
                .andExpect(status().isNotFound());
    }
}