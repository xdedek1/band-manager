package cz.muni.fi.pa165.albums.utils;

import cz.muni.fi.pa165.albums.data.model.Album;
import cz.muni.fi.pa165.albums.data.model.Song;
import cz.muni.fi.pa165.common.data.dto.enums.MusicStyle;

import java.time.LocalDate;

public class AlbumDataFactory {
    public static final String TEST_ALBUM_ID = "4a949e11-f99a-43c8-a190-cbf430c37d63";
    public static final String TEST_BAND_ID = "1e5ce1fb-db2e-4e40-9b06-d026a052095f";
    public static final String TEST_SONG_ID = "3b5f4cce-59cf-4b86-a6a7-16abe39ef31b";
    public static final String TEST_MANAGER_ID = "18419f9c-05bb-43c4-8416-3d5708178706";

    public static Album createAlbum() {
        Album album = new Album();
        album.setId(TEST_ALBUM_ID);
        album.setBandId(TEST_BAND_ID);
        album.setOwnerId(TEST_MANAGER_ID);
        album.setName("Test Album");
        album.setDescription("Test Description");
        album.setReleaseDate(LocalDate.of(2020, 1, 1));

        Song song = new Song();
        song.setId(TEST_SONG_ID);
        song.setTitle("Test Song");
        song.setDuration(180);
        song.setGenre(MusicStyle.COUNTRY);

        return album;
    }
}
