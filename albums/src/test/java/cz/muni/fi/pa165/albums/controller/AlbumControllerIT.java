package cz.muni.fi.pa165.albums.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.albums.security.Context;
import cz.muni.fi.pa165.common.data.dto.album.AlbumCreateDto;
import cz.muni.fi.pa165.common.data.dto.album.AlbumDetailDto;
import cz.muni.fi.pa165.common.data.dto.album.AlbumUpdateDto;
import cz.muni.fi.pa165.albums.data.model.Album;
import cz.muni.fi.pa165.albums.repository.AlbumRepository;
import cz.muni.fi.pa165.albums.utils.AlbumDataFactory;
import cz.muni.fi.pa165.albums.utils.AlbumDtoFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.util.List;

import static cz.muni.fi.pa165.albums.utils.AlbumDataFactory.TEST_MANAGER_ID;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class AlbumControllerIT {
    public static final Album TEST_ALBUM = AlbumDataFactory.createAlbum();
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AlbumRepository albumRepository;

    @MockBean
    private Context context;

    @BeforeEach
    public void setUp() {
        albumRepository.save(TEST_ALBUM);
    }

    @Test
    public void createAlbum_validInput_returnsAlbum() throws Exception {
        when(context.getLoggedInUserId()).thenReturn(TEST_MANAGER_ID);

        AlbumCreateDto albumCreateDto = AlbumDtoFactory.createAlbumCreateDto();

        String response = mockMvc.perform(post("/albums")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(albumCreateDto)))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        AlbumDetailDto albumDetailDto = objectMapper.readValue(response, AlbumDetailDto.class);

        assertThat(albumDetailDto.getBandId()).isEqualTo(albumCreateDto.getBandId());
        assertThat(albumDetailDto.getName()).isEqualTo(albumCreateDto.getName());
        assertThat(albumDetailDto.getDescription()).isEqualTo(albumCreateDto.getDescription());
    }

    @Test
    public void createAlbum_invalidInput_returnsBadRequest() throws Exception {
        mockMvc.perform(post("/albums")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void findById_albumFound_returnsAlbum() throws Exception {
        String result = mockMvc.perform(get("/albums/{id}", TEST_ALBUM.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        AlbumDetailDto response = objectMapper.readValue(result, AlbumDetailDto.class);

        assertThat(response.getId()).isEqualTo(TEST_ALBUM.getId());
        assertThat(response.getName()).isEqualTo(TEST_ALBUM.getName());
        assertThat(response.getDescription()).isEqualTo(TEST_ALBUM.getDescription());
        assertThat(response.getBandId()).isEqualTo(TEST_ALBUM.getBandId());
    }

    @Test
    public void findById_albumNotFound_returnsNotFound() throws Exception {
        mockMvc.perform(get("/albums/{id}", "dummy-id")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void findByBand_albumsFound_returnsAlbums() throws Exception {
        String response = mockMvc.perform(get("/albums/band/{band-id}", TEST_ALBUM.getBandId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        AlbumDetailDto responseAlbum = objectMapper.readValue(response, new TypeReference<List<AlbumDetailDto>>() {}).get(0);

        assertThat(responseAlbum.getId()).isEqualTo(TEST_ALBUM.getId());
        assertThat(responseAlbum.getName()).isEqualTo(TEST_ALBUM.getName());
        assertThat(responseAlbum.getDescription()).isEqualTo(TEST_ALBUM.getDescription());
        assertThat(responseAlbum.getBandId()).isEqualTo(TEST_ALBUM.getBandId());
    }

    @Test
    public void findByBand_noAlbumsFound_returnsEmptyList() throws Exception {
        String response = mockMvc.perform(get("/albums/band/{band-id}", "dummy-id")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        assertThat(response).isEqualTo("[]");
    }

    @Test
    public void updateAlbum_validInput_returnsAlbum() throws Exception {
        when(context.getLoggedInUserId()).thenReturn(TEST_MANAGER_ID);
        AlbumUpdateDto albumUpdateDto = AlbumDtoFactory.createAlbumUpdateDto();

        String response = mockMvc.perform(put("/albums/{id}", TEST_ALBUM.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(albumUpdateDto)))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        AlbumDetailDto albumDetailDto = objectMapper.readValue(response, AlbumDetailDto.class);

        assertThat(albumDetailDto.getName()).isEqualTo(albumUpdateDto.getName());
        assertThat(albumDetailDto.getDescription()).isEqualTo(albumUpdateDto.getDescription());
    }

    @Test
    public void updateAlbum_invalidInput_returnsBadRequest() throws Exception {
        mockMvc.perform(put("/albums/{id}", TEST_ALBUM.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void deleteAlbum_albumFound_returnsNoContent() throws Exception {
        when(context.getLoggedInUserId()).thenReturn(TEST_MANAGER_ID);

        mockMvc.perform(delete("/albums/{id}", TEST_ALBUM.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void deleteAlbum_albumNotFound_returnsNotFound() throws Exception {
        mockMvc.perform(delete("/albums/{id}", "dummy-id")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}
