package cz.muni.fi.pa165.albums.facade;

import cz.muni.fi.pa165.common.data.dto.album.AlbumCreateDto;
import cz.muni.fi.pa165.common.data.dto.album.AlbumDetailDto;
import cz.muni.fi.pa165.common.data.dto.album.AlbumUpdateDto;
import cz.muni.fi.pa165.albums.data.model.Album;
import cz.muni.fi.pa165.albums.mappers.AlbumMapper;
import cz.muni.fi.pa165.albums.service.AlbumService;
import cz.muni.fi.pa165.albums.utils.AlbumDataFactory;
import cz.muni.fi.pa165.albums.utils.AlbumDtoFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static cz.muni.fi.pa165.albums.utils.AlbumDataFactory.TEST_ALBUM_ID;
import static cz.muni.fi.pa165.albums.utils.AlbumDataFactory.TEST_BAND_ID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AlbumFacadeTest {

    private final Album TEST_ALBUM = AlbumDataFactory.createAlbum();
    private final AlbumDetailDto TEST_DETAIL_DTO = AlbumDtoFactory.createAlbumDetailDto();
    private final AlbumCreateDto TEST_CREATE_DTO = AlbumDtoFactory.createAlbumCreateDto();
    private final AlbumUpdateDto TEST_UPDATE_DTO = AlbumDtoFactory.createAlbumUpdateDto();
    private final AlbumDetailDto TEST_UPDATED_DETAIL_DTO = AlbumDtoFactory.createAlbumUpdatedDetailDto();

    @Mock
    private AlbumService albumService;

    @Mock
    private AlbumMapper albumMapper;

    @InjectMocks
    private AlbumFacade albumFacade;

    @Test
    public void findById_validId_returnsAlbumDetailDto() {
        // Arrange
        Album mockAlbum = TEST_ALBUM;
        AlbumDetailDto expectedDto = TEST_DETAIL_DTO;

        when(albumService.findById(TEST_ALBUM_ID)).thenReturn(mockAlbum);
        when(albumMapper.mapToDetailDto(mockAlbum)).thenReturn(expectedDto);

        // Act
        AlbumDetailDto result = albumFacade.findById(TEST_ALBUM_ID);

        // Assert
        assertNotNull(result);
        assertEquals(expectedDto.getId(), result.getId());
        assertEquals(expectedDto.getBandId(), result.getBandId());
        assertEquals(expectedDto.getName(), result.getName());
        assertEquals(expectedDto.getDescription(), result.getDescription());
        assertEquals(expectedDto.getReleaseDate(), result.getReleaseDate());
    }

    @Test
    public void findByBand_validBandId_returnsListOfAlbumDetailDto() {
        // Arrange
        List<Album> mockAlbums = List.of(TEST_ALBUM);

        when(albumService.findByBand(TEST_BAND_ID)).thenReturn(mockAlbums);
        when(albumMapper.mapToDetailDto(TEST_ALBUM)).thenReturn(TEST_DETAIL_DTO);

        // Act
        List<AlbumDetailDto> result = albumFacade.findByBand(TEST_BAND_ID);

        // Assert
        assertNotNull(result);
        assertThat(result).containsExactly(TEST_DETAIL_DTO);
    }

    @Test
    public void create_validCreateDto_returnsCreatedAlbumDetailDto() {
        // Arrange
        AlbumCreateDto createDto = TEST_CREATE_DTO;
        Album mockAlbum = TEST_ALBUM;
        AlbumDetailDto expectedDto = TEST_DETAIL_DTO;

        when(albumMapper.mapToEntity(createDto)).thenReturn(mockAlbum);
        when(albumService.create(mockAlbum)).thenReturn(mockAlbum);
        when(albumMapper.mapToDetailDto(mockAlbum)).thenReturn(expectedDto);

        // Act
        AlbumDetailDto result = albumFacade.create(createDto);

        // Assert
        assertNotNull(result);
        assertEquals(expectedDto.getId(), result.getId());
        assertEquals(expectedDto.getBandId(), result.getBandId());
        assertEquals(expectedDto.getName(), result.getName());
        assertEquals(expectedDto.getDescription(), result.getDescription());
        assertEquals(expectedDto.getReleaseDate(), result.getReleaseDate());
    }

    @Test
    public void update_validId_returnsUpdatedAlbumDetailDto() {
        // Arrange
        AlbumUpdateDto updateDto = TEST_UPDATE_DTO;
        Album mockAlbum = TEST_ALBUM;
        AlbumDetailDto expectedDto = TEST_UPDATED_DETAIL_DTO;

        when(albumMapper.mapToEntity(updateDto)).thenReturn(mockAlbum);
        when(albumService.update(TEST_ALBUM_ID, mockAlbum)).thenReturn(mockAlbum);
        when(albumMapper.mapToDetailDto(mockAlbum)).thenReturn(expectedDto);

        // Act
        AlbumDetailDto result = albumFacade.update(TEST_ALBUM_ID, updateDto);

        // Assert
        assertNotNull(result);
        assertEquals(expectedDto.getId(), result.getId());
        assertEquals(expectedDto.getBandId(), result.getBandId());
        assertEquals(expectedDto.getName(), result.getName());
        assertEquals(expectedDto.getDescription(), result.getDescription());
        assertEquals(expectedDto.getReleaseDate(), result.getReleaseDate());
    }

    @Test
    public void delete_validId_deletesAlbum() {
        // Act
        albumFacade.delete(TEST_ALBUM_ID);

        // Assert
        verify(albumService, times(1)).delete(TEST_ALBUM_ID);
    }
}