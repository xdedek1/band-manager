package cz.muni.fi.pa165.albums.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionAuthenticatedPrincipal;
import org.springframework.stereotype.Component;

@Component
public class Context {

    public String getLoggedInUserId() {
        return getCurrentPrincipal().getSubject();
    }

    public OAuth2IntrospectionAuthenticatedPrincipal getCurrentPrincipal() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication.getPrincipal() instanceof OAuth2IntrospectionAuthenticatedPrincipal) {
            return (OAuth2IntrospectionAuthenticatedPrincipal) authentication.getPrincipal();
        }

        throw new IllegalStateException("The current principal is not an OAuth2IntrospectionAuthenticatedPrincipal");
    }
}
