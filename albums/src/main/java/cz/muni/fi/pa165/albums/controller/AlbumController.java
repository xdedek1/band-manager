package cz.muni.fi.pa165.albums.controller;

import cz.muni.fi.pa165.common.data.dto.album.AlbumCreateDto;
import cz.muni.fi.pa165.common.data.dto.album.AlbumDetailDto;
import cz.muni.fi.pa165.common.data.dto.album.AlbumUpdateDto;
import cz.muni.fi.pa165.albums.facade.AlbumFacade;
import cz.muni.fi.pa165.common.exception.OwnerValidationFailedException;
import cz.muni.fi.pa165.common.exception.ResourceNotFoundException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/albums")
public class AlbumController {

    private final AlbumFacade albumFacade;

    @Autowired
    public AlbumController(AlbumFacade albumFacade) {
        this.albumFacade = albumFacade;
    }

    @Operation(summary = "Get an album by its ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the album"),
            @ApiResponse(responseCode = "404", description = "Album not found"),
    })
    @GetMapping(path = "/{id}")
    public ResponseEntity<AlbumDetailDto> findById(@PathVariable(value = "id") String id) {
        return ResponseEntity.ok(albumFacade.findById(id));
    }

    @Operation(summary = "Get all albums by band ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the album"),
            @ApiResponse(responseCode = "404", description = "Album not found"),
    })
    @GetMapping(path = "/band/{band-id}")
    public ResponseEntity<List<AlbumDetailDto>> findByBand(@PathVariable("band-id") String bandId) {
        return ResponseEntity.ok(albumFacade.findByBand(bandId));
    }

    @Operation(summary = "Create a new album")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Created the album"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
    })
    @PostMapping
    public ResponseEntity<AlbumDetailDto> create(@RequestBody @Valid AlbumCreateDto album) {
        return ResponseEntity.ok(albumFacade.create(album));
    }

    @Operation(summary = "Update an existing album")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated the album"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
            @ApiResponse(responseCode = "404", description = "Album not found"),
    })
    @PutMapping(path = "/{id}")
    public ResponseEntity<AlbumDetailDto> update(@PathVariable(value = "id") String id, @RequestBody @Valid AlbumUpdateDto album) {
        return ResponseEntity.ok(albumFacade.update(id, album));
    }

    @Operation(summary = "Delete an album")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Deleted the album"),
            @ApiResponse(responseCode = "404", description = "Album not found"),
    })
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable(value = "id") String id) {
        albumFacade.delete(id);
        return ResponseEntity.noContent().build();
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<Void> handleResourceNotFoundException() {
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler(OwnerValidationFailedException.class)
    public ResponseEntity<Void> handleOwnerValidationFailedException() {
        return ResponseEntity.badRequest().build();
    }
}
