package cz.muni.fi.pa165.albums.repository;

import cz.muni.fi.pa165.albums.data.model.Album;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AlbumRepository extends JpaRepository<Album, String> {

    @Query("SELECT a FROM Album a WHERE a.bandId = :bandId")
    List<Album> findByBand(@Param("bandId") String bandId);

}
