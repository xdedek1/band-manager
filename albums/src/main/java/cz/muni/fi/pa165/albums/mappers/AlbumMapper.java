package cz.muni.fi.pa165.albums.mappers;

import cz.muni.fi.pa165.common.data.dto.album.AlbumCreateDto;
import cz.muni.fi.pa165.common.data.dto.album.AlbumDetailDto;
import cz.muni.fi.pa165.common.data.dto.album.AlbumUpdateDto;
import cz.muni.fi.pa165.albums.data.model.Album;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AlbumMapper {
    AlbumDetailDto mapToDetailDto(Album activity);
    Album mapToEntity(AlbumCreateDto createDto);
    Album mapToEntity(AlbumUpdateDto updateDto);

}
