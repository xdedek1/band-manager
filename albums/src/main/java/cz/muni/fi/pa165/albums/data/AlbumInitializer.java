package cz.muni.fi.pa165.albums.data;

import cz.muni.fi.pa165.common.data.dto.enums.MusicStyle;
import cz.muni.fi.pa165.albums.data.model.Album;
import cz.muni.fi.pa165.albums.data.model.Song;
import cz.muni.fi.pa165.albums.repository.AlbumRepository;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

@Service
@Transactional
public class AlbumInitializer {

    private final AlbumRepository albumRepository;

    @Autowired
    public AlbumInitializer(AlbumRepository albumRepository) {
        this.albumRepository = albumRepository;
    }

    @PostConstruct
    public void insertDummyData() {
        initializedDummyAlbum();
    }

    private void initializedDummyAlbum() {
        LocalDate releaseDate = LocalDate.of(2024, 1, 1);

        Album album = new Album();

        album.setId("49a5121b-2c00-4fb3-8a59-11ebaaea138b");
        album.setName("Dummy album");
        album.setDescription("This is a dummy album");
        album.setReleaseDate(releaseDate);

        Song song = new Song();

        song.setId("c11b2e33-d9be-43db-ae7c-2dda74529604");
        song.setDuration(180);
        song.setGenre(MusicStyle.HIPHOP);
        song.setTitle("AKM");

        albumRepository.save(album);
    }
}
