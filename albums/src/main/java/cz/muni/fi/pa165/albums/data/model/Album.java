package cz.muni.fi.pa165.albums.data.model;

import cz.muni.fi.pa165.common.data.BasicEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "album")
public class Album extends BasicEntity {

    @Schema(description = "Band ID", example = "1")
    @Column(name = "band_id")
    private String bandId;

    @Schema(description = "Owner ID", example = "1")
    @Column(name = "owner_id")
    private String ownerId;

    @Schema(description = "Name of the album", example = "The Dark Side of the Moon")
    @Column(name = "name", length = 50)
    private String name;

    @Schema(description = "Description of the album", example = "The Dark Side of the Moon is the eighth studio album by the English rock band Pink Floyd, released on 1 March 1973 by Harvest Records.")
    @Column(name = "description", length = 200)
    private String description;

    @Schema(description = "Release date of the album", example = "1973-03-01")
    @Column(name = "release_date")
    private LocalDate releaseDate;

    @Schema(description = "List of songs in the album")
    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "album_songs",
            joinColumns        = @JoinColumn(name = "album_id"),
            inverseJoinColumns = @JoinColumn(name = "song_id"))
    private List<Song> songs;
}
