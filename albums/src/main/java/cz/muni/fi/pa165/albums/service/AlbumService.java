package cz.muni.fi.pa165.albums.service;

import cz.muni.fi.pa165.albums.data.model.Album;
import cz.muni.fi.pa165.albums.security.Context;
import cz.muni.fi.pa165.common.exception.OwnerValidationFailedException;
import cz.muni.fi.pa165.common.exception.ResourceNotFoundException;
import cz.muni.fi.pa165.albums.repository.AlbumRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AlbumService {
    private final AlbumRepository albumRepository;

    private final Context context;

    @Autowired
    public AlbumService(AlbumRepository albumRepository, Context context) {
        this.albumRepository = albumRepository;
        this.context = context;
    }

    @Transactional(readOnly = true)
    public Album findById(String id) {
        return albumRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Album with id: " + id + " was not found."));
    }


    @Transactional(readOnly = true)
    public List<Album> findByBand(String bandId) {
        return albumRepository.findByBand(bandId);
    }

    @Transactional
    public Album create(Album toCreate) {
        String userId = context.getLoggedInUserId();
        toCreate.setOwnerId(userId);

        return albumRepository.save(toCreate);
    }

    @Transactional
    public Album update(String id, Album toUpdate) {
        findById(id);
        toUpdate.setId(id);

        return albumRepository.save(toUpdate);
    }

    @Transactional
    public void delete(String id) {
        Album album = findById(id);

        albumRepository.delete(album);
    }
}
