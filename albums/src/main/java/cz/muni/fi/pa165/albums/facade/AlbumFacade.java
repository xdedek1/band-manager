package cz.muni.fi.pa165.albums.facade;

import cz.muni.fi.pa165.common.data.dto.album.AlbumCreateDto;
import cz.muni.fi.pa165.common.data.dto.album.AlbumDetailDto;
import cz.muni.fi.pa165.common.data.dto.album.AlbumUpdateDto;
import cz.muni.fi.pa165.albums.data.model.Album;
import cz.muni.fi.pa165.albums.mappers.AlbumMapper;
import cz.muni.fi.pa165.albums.service.AlbumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional

public class AlbumFacade {
    private final AlbumService service;
    private final AlbumMapper mapper;

    @Autowired
    public AlbumFacade(AlbumService albumService, AlbumMapper albumMapper) {
        this.service = albumService;
        this.mapper = albumMapper;
    }

    @Transactional(readOnly = true)
    public AlbumDetailDto findById(String id) {
        return mapper.mapToDetailDto(service.findById(id));
    }

    @Transactional(readOnly = true)
    public List<AlbumDetailDto> findByBand(String bandId) {
        return service.findByBand(bandId).stream()
                .map(mapper::mapToDetailDto)
                .toList();
    }

    @Transactional
    public AlbumDetailDto create(AlbumCreateDto createDto) {
        Album toCreate = mapper.mapToEntity(createDto);
        return mapper.mapToDetailDto(service.create(toCreate));
    }

    @Transactional
    public AlbumDetailDto update(String id, AlbumUpdateDto updateDto) {
        Album toUpdate = mapper.mapToEntity(updateDto);
        return mapper.mapToDetailDto(service.update(id, toUpdate));
    }

    @Transactional
    public void delete(String id) {
        service.delete(id);
    }

}
