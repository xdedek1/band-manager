package cz.muni.fi.pa165.albums.data.model;

import cz.muni.fi.pa165.common.data.dto.enums.MusicStyle;
import cz.muni.fi.pa165.common.data.BasicEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Schema(description = "Song entity")
@Getter
@Setter
@Entity
@Table(name = "song")
public class Song extends BasicEntity {

    @Schema(description = "Title of the song", example = "Time")
    @Column(name = "title", length = 100)
    private String title;

    @Schema(description = "Duration of the song in seconds", example = "300")
    @Column(name = "duration")
    private int duration; // Assuming duration is in seconds

    @Schema(description = "Genre of the song", example = "ROCK")
    @Column(name = "genre")
    private MusicStyle genre;
}