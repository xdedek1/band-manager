package cz.muni.fi.pa165.users.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.common.data.dto.user.UserCreateDto;
import cz.muni.fi.pa165.common.data.dto.user.UserDetailDto;
import cz.muni.fi.pa165.common.data.dto.user.UserUpdateDto;
import cz.muni.fi.pa165.common.data.factory.UserDtoFactory;
import cz.muni.fi.pa165.common.exception.ResourceNotFoundException;
import cz.muni.fi.pa165.users.facade.UserFacade;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static cz.muni.fi.pa165.users.utils.UserDataFactory.TEST_USER_ID;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class UserControllerTest {

    private final UserDetailDto TEST_DETAIL_DTO = UserDtoFactory.createUserDetailDto();
    private final UserCreateDto TEST_CREATE_DTO = UserDtoFactory.createUserCreateDto();
    private final UserUpdateDto TEST_UPDATE_DTO = UserDtoFactory.createUserUpdateDto();
    private final UserDetailDto TEST_UPDATED_DETAIL_DTO = UserDtoFactory.createUserDetailDto();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserFacade userFacade;

    @InjectMocks
    private UserController userController;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void findById_userFound_returnsUserDetailDto() throws Exception {
        // Arrange
        UserDetailDto expectedDto = TEST_DETAIL_DTO;

        when(userFacade.findById(TEST_USER_ID)).thenReturn(expectedDto);

        // Act
        ResultActions result = mockMvc.perform(get("/users/{id}", TEST_USER_ID).contentType(MediaType.APPLICATION_JSON));

        // Assert
        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(expectedDto.getId()))
                .andExpect(jsonPath("$.firstName").value(expectedDto.getFirstName()))
                .andExpect(jsonPath("$.lastName").value(expectedDto.getLastName()))
                .andExpect(jsonPath("$.email").value(expectedDto.getEmail()));
    }

    @Test
    public void findById_userNotFound_returnsNotFound() throws Exception {
        String nonExistingId = "non-existent-id";

        when(userFacade.findById(nonExistingId)).thenThrow(ResourceNotFoundException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/users/{id}", nonExistingId))
                .andExpect(status().isNotFound());
    }

    @Test
    public void findByEmail_userFound_returnsUserDetailDto() throws Exception {
        // Arrange
        UserDetailDto expectedDto = TEST_DETAIL_DTO;

        when(userFacade.findByEmail(expectedDto.getEmail())).thenReturn(expectedDto);

        // Act
        ResultActions result = mockMvc.perform(get("/users/email/{email}", expectedDto.getEmail()).contentType(MediaType.APPLICATION_JSON));

        // Assert
        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(expectedDto.getId()))
                .andExpect(jsonPath("$.firstName").value(expectedDto.getFirstName()))
                .andExpect(jsonPath("$.lastName").value(expectedDto.getLastName()))
                .andExpect(jsonPath("$.email").value(expectedDto.getEmail()));
    }

    @Test
    public void findByEmail_userNotFound_returnsNotFound() throws Exception {
        String nonExistingEmail = "nonexistent@example.com";

        when(userFacade.findByEmail(nonExistingEmail)).thenThrow(ResourceNotFoundException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/users/email/{email}", nonExistingEmail))
                .andExpect(status().isNotFound());
    }

    @Test
    public void create_ReturnsUserDetailDto() throws Exception {
        // Arrange
        UserCreateDto createDto = TEST_CREATE_DTO;
        UserDetailDto expectedDto = TEST_DETAIL_DTO;

        when(userFacade.create(createDto)).thenReturn(expectedDto);

        // Act
        ResultActions resultActions = mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{" +
                        "\"firstName\":\"" + createDto.getFirstName() + "\", " +
                        "\"lastName\":\"" + createDto.getLastName() + "\", " +
                        "\"email\":\"" + createDto.getEmail() + "\", " +
                        "\"id\":\"" + createDto.getId() + "\"" +
                        "}")
        );

        // Assert
        resultActions.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(expectedDto.getId()))
                .andExpect(jsonPath("$.firstName").value(expectedDto.getFirstName()))
                .andExpect(jsonPath("$.lastName").value(expectedDto.getLastName()))
                .andExpect(jsonPath("$.email").value(expectedDto.getEmail()));
    }

    @Test
    public void create_invalidInput_returnsBadRequest() throws Exception {
        // Arrange
        when(userFacade.create(any()))
                .thenReturn(null);

        // Act & Assert
        mockMvc.perform(post("/users")
                        .param("userId", "test-user-id")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void updateLoggedInUser_validInput_returnsUpdatedUserDetailDto() throws Exception {
        // Arrange
        UserUpdateDto updateDto = TEST_UPDATE_DTO;
        UserDetailDto expectedDto = TEST_UPDATED_DETAIL_DTO;

        when(userFacade.updateLoggedInUser(updateDto)).thenReturn(expectedDto);

        // Act
        ResultActions resultActions = mockMvc.perform(put("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{" +
                        "\"firstName\":\"" + updateDto.getFirstName() + "\", " +
                        "\"lastName\":\"" + updateDto.getLastName() + "\", " +
                        "\"email\":\"" + updateDto.getEmail() + "\"" +
                        "}")
        );

        // Assert
        resultActions.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(expectedDto.getId()))
                .andExpect(jsonPath("$.firstName").value(expectedDto.getFirstName()))
                .andExpect(jsonPath("$.lastName").value(expectedDto.getLastName()))
                .andExpect(jsonPath("$.email").value(expectedDto.getEmail()));
    }

    @Test
    public void update_invalidInput_returnsBadRequest() throws Exception {
        // Arrange
        UserUpdateDto invalidUpdateDto = new UserUpdateDto();
        invalidUpdateDto.setFirstName("");

        when(userFacade.updateLoggedInUser(any())).
                thenReturn(TEST_UPDATED_DETAIL_DTO);

        // Act & Assert
        mockMvc.perform(put("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(invalidUpdateDto)))
                .andExpect(status().isBadRequest());
    }

//    @Test
//    void delete_userDeleted_returnsNoContent() {
//        // Act
//        ResponseEntity<Void> response = userController.deleteLoggedInUser();
//
//        // Assert
//        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
//    }

}
