package cz.muni.fi.pa165.users.repository;

import cz.muni.fi.pa165.users.data.model.User;
import cz.muni.fi.pa165.users.utils.UserDataFactory;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@DataJpaTest
@EnableJpaRepositories
@ExtendWith(MockitoExtension.class)
public class UserRepositoryTest {

    @Mock
    private UserRepository userRepository;

    @Test
    void findById_UserExists_ReturnsOptionalOfUser() {
        // Arrange
        String userId = "1";
        User user = UserDataFactory.createUser();
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));

        // Act
        Optional<User> result = userRepository.findById(userId);

        // Assert
        assertThat(result).isPresent();
        assertThat(result.get()).isEqualTo(user);
    }

    @Test
    void findById_UserDoesNotExist_ReturnsEmptyOptional() {
        // Arrange
        String userId = "1";
        when(userRepository.findById(userId)).thenReturn(Optional.empty());

        // Act
        Optional<User> result = userRepository.findById(userId);

        // Assert
        assertThat(result).isEmpty();
    }
}