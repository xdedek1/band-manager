package cz.muni.fi.pa165.users.service;

import cz.muni.fi.pa165.common.client.IntegrationClient;
import cz.muni.fi.pa165.users.data.model.User;
import cz.muni.fi.pa165.users.repository.UserRepository;
import cz.muni.fi.pa165.users.security.Context;
import cz.muni.fi.pa165.users.utils.UserDataFactory;
import cz.muni.fi.pa165.common.exception.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static cz.muni.fi.pa165.users.utils.UserDataFactory.TEST_USER_ID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    private final User TEST_USER = UserDataFactory.createUser();

    @Mock
    private UserRepository userRepository;

    @Mock
    private Context context;

    @InjectMocks
    private UserService userService;

    @Test
    public void testFindById_ExistingId_ReturnsUser() {
        // Arrange
        User mockUser = TEST_USER;
        mockUser.setId(TEST_USER_ID);
        when(userRepository.findById(TEST_USER_ID)).thenReturn(Optional.of(mockUser));
        when(userRepository.existsById(TEST_USER_ID)).thenReturn(true);


        // Act
        User foundUser = userService.findById(TEST_USER_ID);

        // Assert
        assertNotNull(foundUser);
        assertThat(foundUser).isEqualTo(TEST_USER);
    }

    @Test
    public void testFindById_NonExistingId_ThrowsException() {
        // Arrange
        String id = "non-existing-id";

        // Act & Assert
        assertThrows(ResourceNotFoundException.class, () -> userService.findById(id));
    }

    @Test
    public void testCreate_ValidUser_ReturnsCreatedUser() {
        // Arrange
        User toCreate = TEST_USER;
        when(userRepository.save(toCreate)).thenReturn(toCreate);

        // Act
        User createdUser = userService.create(toCreate);

        // Assert
        assertNotNull(createdUser);
        assertEquals(toCreate, createdUser);
    }

    @Test
    void testUpdate_userExists_returnsUpdatedUser() {
        // Arrange
        when(userRepository.save(TEST_USER)).thenReturn(TEST_USER);
        when(context.getLoggedInUserId()).thenReturn(TEST_USER_ID);

        // Act
        User updatedUser = userService.updateLoggedInUser(TEST_USER);

        // Assert
        assertThat(updatedUser).isEqualTo(TEST_USER);
    }

    @Test
    public void testDelete_ExistingId_DeletesUser() {
        // Arrange
        when(context.getLoggedInUserId()).thenReturn(TEST_USER_ID);

        // Act
        userService.deleteLoggedInUser();

        // Assert
        verify(userRepository, times(1)).deleteById(TEST_USER_ID);
    }
}
