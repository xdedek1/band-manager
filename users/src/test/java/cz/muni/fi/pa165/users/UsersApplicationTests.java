package cz.muni.fi.pa165.users;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = UsersApplication.class)
class UsersApplicationTests {

    @Test
    void contextLoads() {
    }

}
