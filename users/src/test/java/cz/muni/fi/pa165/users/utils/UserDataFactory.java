package cz.muni.fi.pa165.users.utils;

import cz.muni.fi.pa165.common.data.dto.enums.UserRole;
import cz.muni.fi.pa165.users.data.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserDataFactory {
    public static final String TEST_USER_ID = "7c0dc892-68d0-4e0b-9aeb-dec52c353099";

    public static User createUser() {
        User user = new User();
        user.setId("7c0dc892-68d0-4e0b-9aeb-dec52c353099");
        user.setFirstName("First");
        user.setLastName("Last");
        user.setEmail("example@example.com");
        user.getRoles().add(UserRole.USER);
        return user;
    }
}
