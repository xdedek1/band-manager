package cz.muni.fi.pa165.users.facade;

import cz.muni.fi.pa165.users.data.model.User;
import cz.muni.fi.pa165.users.mappers.UserMapper;
import cz.muni.fi.pa165.users.service.UserService;
import cz.muni.fi.pa165.users.utils.UserDataFactory;
import cz.muni.fi.pa165.common.data.dto.user.UserCreateDto;
import cz.muni.fi.pa165.common.data.dto.user.UserDetailDto;
import cz.muni.fi.pa165.common.data.dto.user.UserUpdateDto;
import cz.muni.fi.pa165.common.data.factory.UserDtoFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserFacadeTest {

    @Mock
    private UserService userService;

    @Mock
    private UserMapper userMapper;

    @InjectMocks
    private UserFacade userFacade;

    private final User TEST_USER = UserDataFactory.createUser();
    private final UserDetailDto TEST_DETAIL_DTO = UserDtoFactory.createUserDetailDto();
    private final UserCreateDto TEST_CREATE_DTO = UserDtoFactory.createUserCreateDto();
    private final UserUpdateDto TEST_UPDATE_DTO = UserDtoFactory.createUserUpdateDto();


    @Test
    void findById_userFound_returnsUserDetailDto() {
        // Arrange
        when(userService.findById(TEST_USER.getId())).thenReturn(TEST_USER);
        when(userMapper.mapToDetailDto(TEST_USER)).thenReturn(TEST_DETAIL_DTO);

        // Act
        UserDetailDto foundDto = userFacade.findById(TEST_USER.getId());

        // Assert
        assertThat(foundDto).isEqualTo(TEST_DETAIL_DTO);
    }

    @Test
    void findByEmail_userFound_returnsUserDetailDto() {
        // Arrange
        when(userService.findByEmail(TEST_USER.getEmail())).thenReturn(TEST_USER);
        when(userMapper.mapToDetailDto(TEST_USER)).thenReturn(TEST_DETAIL_DTO);

        // Act
        UserDetailDto foundDto = userFacade.findByEmail(TEST_USER.getEmail());

        // Assert
        assertThat(foundDto).isEqualTo(TEST_DETAIL_DTO);
    }

    @Test
    void create_userCreated_returnsUserDetailDto() {
        // Arrange
        when(userMapper.mapToEntity(TEST_CREATE_DTO)).thenReturn(TEST_USER);
        when(userService.create(TEST_USER)).thenReturn(TEST_USER);
        when(userMapper.mapToDetailDto(TEST_USER)).thenReturn(TEST_DETAIL_DTO);

        // Act
        UserDetailDto createdDto = userFacade.create(TEST_CREATE_DTO);

        // Assert
        assertThat(createdDto).isEqualTo(TEST_DETAIL_DTO);
    }

    @Test
    void updateLoggedInUser_userUpdated_returnsUserDetailDto() {
        // Arrange
        when(userMapper.mapToEntity(TEST_UPDATE_DTO)).thenReturn(TEST_USER);
        when(userService.updateLoggedInUser(TEST_USER)).thenReturn(TEST_USER);
        when(userMapper.mapToDetailDto(TEST_USER)).thenReturn(TEST_DETAIL_DTO);

        // Act
        UserDetailDto updatedDto = userFacade.updateLoggedInUser(TEST_UPDATE_DTO);

        // Assert
        assertThat(updatedDto).isEqualTo(TEST_DETAIL_DTO);
    }

    @Test
    void deleteLoggedInUser_userDeleted_returnsVoid() {
        // Act
        userFacade.deleteLoggedInUser();

        // Assert
        verify(userService).deleteLoggedInUser();
    }

    @Test
    void markAsManager_userMarked_returnsVoid() {
        // Act
        userFacade.markAsManager(TEST_USER.getId());

        // Assert
        verify(userService).markAsManager(TEST_USER.getId());
    }
}

