package cz.muni.fi.pa165.users.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.common.data.dto.user.UserCreateDto;
import cz.muni.fi.pa165.common.data.dto.user.UserDetailDto;
import cz.muni.fi.pa165.common.data.dto.user.UserUpdateDto;
import cz.muni.fi.pa165.common.data.factory.UserDtoFactory;
import cz.muni.fi.pa165.users.data.model.User;
import cz.muni.fi.pa165.users.repository.UserRepository;
import cz.muni.fi.pa165.users.security.Context;
import cz.muni.fi.pa165.users.utils.UserDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class UserControllerIT {
    public static final User TEST_USER = UserDataFactory.createUser();
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserRepository userRepository;

    @MockBean
    private Context context;

    @BeforeEach
    public void setUp() {
        userRepository.save(TEST_USER);
        when(context.getLoggedInUserId()).thenReturn(TEST_USER.getId());
    }

    @Test
    public void createUser_validInput_returnsUser() throws Exception {
        UserCreateDto userCreateDto = UserDtoFactory.createUserCreateDto();

        String response = mockMvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(userCreateDto)))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        UserDetailDto userDetailDto = objectMapper.readValue(response, UserDetailDto.class);

        assertThat(userDetailDto.getEmail()).isEqualTo(userCreateDto.getEmail());
        assertThat(userDetailDto.getFirstName()).isEqualTo(userCreateDto.getFirstName());
        assertThat(userDetailDto.getLastName()).isEqualTo(userCreateDto.getLastName());
    }

    @Test
    public void createUser_invalidInput_returnsUser() throws Exception {
        mockMvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void findById_userFound_returnsUser() throws Exception {

        String result = mockMvc.perform(get("/users/{id}", TEST_USER.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        UserDetailDto response = objectMapper.readValue(result, UserDetailDto.class);

        assertThat(response.getId()).isEqualTo(TEST_USER.getId());
        assertThat(response.getEmail()).isEqualTo(TEST_USER.getEmail());
        assertThat(response.getFirstName()).isEqualTo(TEST_USER.getFirstName());
        assertThat(response.getLastName()).isEqualTo(TEST_USER.getLastName());
    }

    @Test
    public void findById_userNotFound_returnsNotFound() throws Exception {
        mockMvc.perform(get("/users/{id}", "dummy-id")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateUser_validInput_returnsUser() throws Exception {
        UserUpdateDto userUpdateDto = UserDtoFactory.createUserUpdateDto();

        String response = mockMvc.perform(put("/users", TEST_USER.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(userUpdateDto)))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        UserDetailDto userDetailDto = objectMapper.readValue(response, UserDetailDto.class);

        assertThat(userDetailDto.getEmail()).isEqualTo(userUpdateDto.getEmail());
        assertThat(userDetailDto.getFirstName()).isEqualTo(userUpdateDto.getFirstName());
        assertThat(userDetailDto.getLastName()).isEqualTo(userUpdateDto.getLastName());
    }

    @Test
    public void updateUser_invalidInput_returnsUser() throws Exception {
        mockMvc.perform(put("/users", TEST_USER.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void markUserAsAdmin_userFound_returnsUser() throws Exception {
        mockMvc.perform(put("/users/{id}/mark-as-manager", TEST_USER.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void markUserAsAdmin_userNotFound_returnsUser() throws Exception {
        mockMvc.perform(put("/users/{id}/mark-as-manager", "dummy-id")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteUser_userFound_returnsNoContent() throws Exception {
        mockMvc.perform(delete("/users")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
