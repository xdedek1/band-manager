package cz.muni.fi.pa165.users.data;

import cz.muni.fi.pa165.common.data.dto.enums.UserRole;
import cz.muni.fi.pa165.users.data.model.User;
import cz.muni.fi.pa165.users.repository.UserRepository;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;


@Service
public class UserInitializer {

    private final UserRepository userRepository;

    @Autowired
    public UserInitializer(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PostConstruct
    public void insertDummyData() {
        userRepository.saveAll(createDummyUsers());
    }

    private List<User> createDummyUsers() {
        return List.of(
                createDummyUser(
                "subject-id1@pa165.cz",
                        "manager@pa165.dummy.cz",
                        "Dummy",
                        "Manager",
                        Set.of(UserRole.MANAGER, UserRole.USER)),
                createDummyUser(
                        "subject-id2@pa165.cz",
                        "user@pa165.dummy.cz",
                        "Dummy",
                        "User",
                        Set.of(UserRole.USER)),
                createDummyUser(
                        "subject-id3@pa165.cz",
                        "human@pa165.human.muni",
                        "Human",
                        "Burger",
                        Set.of(UserRole.USER))
        );
    }

    private User createDummyUser(String id, String email, String firstName, String lastName, Set<UserRole> roles) {
        User user = new User();
        user.setId(id);
        user.setEmail(email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.getRoles().addAll(roles);
        return user;
    }
}
