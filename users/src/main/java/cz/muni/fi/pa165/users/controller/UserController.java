package cz.muni.fi.pa165.users.controller;

import cz.muni.fi.pa165.common.data.dto.user.UserCreateDto;
import cz.muni.fi.pa165.common.data.dto.user.UserDetailDto;
import cz.muni.fi.pa165.common.data.dto.user.UserUpdateDto;
import cz.muni.fi.pa165.common.exception.OwnerValidationFailedException;
import cz.muni.fi.pa165.common.exception.ResourceNotFoundException;
import cz.muni.fi.pa165.users.facade.UserFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserFacade userFacade;

    @Autowired
    public UserController(UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    @Operation(summary = "Get a user by its ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the user"),
            @ApiResponse(responseCode = "404", description = "User not found"),
    })
    @GetMapping(path = "/{id}")
    public ResponseEntity<UserDetailDto> findById(@PathVariable(value = "id") String id) {
        return ResponseEntity.ok(userFacade.findById(id));
    }

    @Operation(summary = "Check if user with given email exists")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User exists"),
            @ApiResponse(responseCode = "404", description = "User not found"),
    })
    @GetMapping(path = "email/{email}")
    public ResponseEntity<UserDetailDto> findByEmail(@PathVariable(value = "email") @Email String email) {
        return ResponseEntity.ok(userFacade.findByEmail(email));
    }

    @Operation(summary = "Create a new user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Created the user"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
    })
    @PostMapping
    public ResponseEntity<UserDetailDto> create(@RequestBody @Valid UserCreateDto user) {
        return ResponseEntity.ok(userFacade.create(user));
    }

    @Operation(summary = "Update an existing user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated the user"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
            @ApiResponse(responseCode = "404", description = "User not found"),
    })
    @PutMapping
    public ResponseEntity<UserDetailDto> updateLoggedInUser(@RequestBody @Valid UserUpdateDto user) {
        return ResponseEntity.ok(userFacade.updateLoggedInUser(user));
    }

    @Operation(summary = "Delete a user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Deleted the user"),
            @ApiResponse(responseCode = "404", description = "User not found"),
    })
    @DeleteMapping
    public ResponseEntity<Void> deleteLoggedInUser() {
        userFacade.deleteLoggedInUser();
        return ResponseEntity.ok().build();
    }

    @Operation(summary = "Adds MANAGER role to user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "MANAGER role added"),
            @ApiResponse(responseCode = "404", description = "User not found"),
    })
    @PutMapping(path = "/{id}/mark-as-manager")
    public ResponseEntity<Void> markAsManager(@PathVariable(value = "id") String id) {
        userFacade.markAsManager(id);
        return ResponseEntity.ok().build();
    }

    @Operation(summary = "Returns if the logged in user is manager")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200"),
    })
    @GetMapping(path = "/is-logged-in-manager")
    public ResponseEntity<Boolean> isLoggedInUserBandManager() {
        return ResponseEntity.ok(userFacade.isLoggedInUserManager());
    }


    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<Void> handleResourceNotFoundException() {
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler(OwnerValidationFailedException.class)
    public ResponseEntity<Void> handleOwnerValidationFailedException() {
        return ResponseEntity.badRequest().build();
    }
}
