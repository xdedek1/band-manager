package cz.muni.fi.pa165.users.service;

import cz.muni.fi.pa165.common.data.dto.enums.UserRole;
import cz.muni.fi.pa165.common.exception.OwnerValidationFailedException;
import cz.muni.fi.pa165.common.exception.ResourceNotFoundException;
import cz.muni.fi.pa165.users.data.model.User;
import cz.muni.fi.pa165.users.repository.UserRepository;
import cz.muni.fi.pa165.users.security.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {

    private final UserRepository repository;
    private final Context context;
    private final Validations validations = new Validations();

    @Autowired
    public UserService(UserRepository repository, Context context) {
        this.repository = repository;
        this.context = context;
    }

    @Transactional
    public User create(User user) {
        user.getRoles().add(UserRole.USER);
        return repository.save(user);
    }

    @Transactional
    public User updateLoggedInUser(User user) {
        String userId = context.getLoggedInUserId();
        user.setId(userId);

        return repository.save(user);
    }

    @Transactional
    public void deleteLoggedInUser() {
        String userId = context.getLoggedInUserId();
        repository.deleteById(userId);
    }

    public User findById(String id) {
        validations.checkIfUserExists(id);

        return repository.findById(id).orElse(null);
    }

    public User findByEmail(String email) {
        User user =  repository.findByEmail(email);
        if (user == null) {
            throw new ResourceNotFoundException("User with email: " + email + " was not found.");
        }

        return user;
    }

    public void markAsManager(String userId) {
        validations.checkIfUserExists(userId);
        User user = findById(userId);
        user.getRoles().add(UserRole.MANAGER);
        repository.save(user);
    }

    public boolean isLoggedInUserManager() {
        String userId = context.getLoggedInUserId();
        return repository.findByIdAndManagerRole(userId) > 0;
    }

    private class Validations {
        public void checkIfUserExists(String id) {
            if (!repository.existsById(id)) { //TODO - maybe do not call DB again
                throw new ResourceNotFoundException("User with id: " + id + " was not found.");
            }
        }
    }
}
