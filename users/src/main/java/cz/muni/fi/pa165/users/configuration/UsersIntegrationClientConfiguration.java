package cz.muni.fi.pa165.users.configuration;

import cz.muni.fi.pa165.common.client.IntegrationClientConfiguration;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UsersIntegrationClientConfiguration extends IntegrationClientConfiguration {
}
