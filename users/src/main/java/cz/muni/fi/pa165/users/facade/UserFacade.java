package cz.muni.fi.pa165.users.facade;

import cz.muni.fi.pa165.common.data.dto.user.UserCreateDto;
import cz.muni.fi.pa165.common.data.dto.user.UserDetailDto;
import cz.muni.fi.pa165.common.data.dto.user.UserUpdateDto;
import cz.muni.fi.pa165.users.data.model.User;
import cz.muni.fi.pa165.users.mappers.UserMapper;
import cz.muni.fi.pa165.users.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserFacade {

    private final UserService service;
    private final UserMapper mapper;

    @Autowired
    public UserFacade(UserService service, UserMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    public UserDetailDto create(UserCreateDto dto) {
        User user = mapper.mapToEntity(dto);
        return mapper.mapToDetailDto(service.create(user));
    }

    public UserDetailDto updateLoggedInUser(UserUpdateDto dto) {
        User user = mapper.mapToEntity(dto);

        return mapper.mapToDetailDto(service.updateLoggedInUser(user));
    }

    public void deleteLoggedInUser() {
        service.deleteLoggedInUser();
    }

    public UserDetailDto findById(String id) {
        return mapper.mapToDetailDto(service.findById(id));
    }

    public UserDetailDto findByEmail(String email) {
        return mapper.mapToDetailDto(service.findByEmail(email));
    }

    public void markAsManager(String userId) {
        service.markAsManager(userId);
    }

    public boolean isLoggedInUserManager() {
        return service.isLoggedInUserManager();
    }
}
