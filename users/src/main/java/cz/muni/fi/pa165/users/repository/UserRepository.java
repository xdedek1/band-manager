package cz.muni.fi.pa165.users.repository;

import cz.muni.fi.pa165.users.data.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    @Query("SELECT u FROM User u WHERE u.email = ?1")
    User findByEmail(String email);

    @Query("SELECT COUNT(u) FROM User u WHERE u.id = ?1 AND 'MANAGER' IN (SELECT r FROM u.roles r)")
    long findByIdAndManagerRole(String userId);
}
