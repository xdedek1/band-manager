package cz.muni.fi.pa165.users.data.model;

import cz.muni.fi.pa165.common.data.BasicEntity;
import cz.muni.fi.pa165.common.data.dto.enums.UserRole;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "band_user")
public class User extends BasicEntity {

    @Schema(description = "The first name of the user", example = "John")
    @Column(name = "first_name", length = 256)
    private String firstName;

    @Schema(description = "The last name of the user", example = "Doe")
    @Column(name = "last_name", length = 256)
    private String lastName;

    @Schema(description = "The email of the user", example = "em@email.com")
    @Column(unique = true, nullable = false)
    private String email;

    @Enumerated(EnumType.STRING)
    @ElementCollection
    @JoinTable(name = "band_user_roles", joinColumns = @JoinColumn(name = "band_user_id"))
    private Set<UserRole> roles = new HashSet<>();
}
