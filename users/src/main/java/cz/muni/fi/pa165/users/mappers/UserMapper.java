package cz.muni.fi.pa165.users.mappers;

import cz.muni.fi.pa165.common.data.dto.user.UserCreateDto;
import cz.muni.fi.pa165.common.data.dto.user.UserDetailDto;
import cz.muni.fi.pa165.common.data.dto.user.UserUpdateDto;
import cz.muni.fi.pa165.users.data.model.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserDetailDto mapToDetailDto(User user);
    User mapToEntity(UserCreateDto createDto);
    User mapToEntity(UserUpdateDto updateDto);
}
