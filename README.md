# Music Band Management Web Application

The web application allows one music band to manage their activities. Managers can create a new band and start hiring team members from a catalogue. They can set for the band the musical style (e.g. rock, pop, etc…), a name, and a logo. Band managers can add new albums and songs to a band. Each Song has a name and a duration. Managers can also schedule tours for the bands, in terms of dates and cities visited.
Team members can login to the system and see the offers made by the band managers. They can accept or reject to be part of a band. After they are part of a band, they can see all the activities that are planned and the profiles of the other band members.

## Authors

Oliver Bajus, Radomír Dedek, Frederik Fedorko, Tamara Jadušová

## Services

### Activity Service

The Activity Service provides functionality related to managing activities within the music band management system. It facilitates the creation, retrieval, updating, and deletion of activities associated with bands.

__Functionality__:

- __Find Activity by ID__: Retrieves details of an activity by its unique identifier.
- __Find Activities by Band__: Retrieves a list of activities associated with a specific music band.
- __Find Activities by Owner__: Retrieves activities associated with the specified owner, typically a band manager.
- __Create Activity__: Allows the creation of a new activity within the system. This includes specifying details such as the type of activity, date, location, etc.
- __Update Activity__: Enables updating the details of an existing activity. This may include modifying the date, location, or any other relevant information.
- __Delete Activity__: Permanently removes an activity from the system.

These functionalities collectively provide band managers and team members with the tools necessary to effectively manage and track various activities such as tours, album releases, and other events related to the music band.

### Album Service

The Album Service provides functionality related to managing albums within the music band management system. It facilitates the creation, retrieval, updating, and deletion of albums associated with bands.

__Functionality__:

- __Find Album by ID__: Retrieves details of an album by its unique identifier.
- __Find Albums by Band__: Retrieves a list of albums associated with a specific music band.
- __Create Album__: Allows the creation of a new album within the system. This includes specifying details such as the album title, release date, genre, etc.
- __Update Album__: Enables updating the details of an existing album. This may include modifying the title, release date, genre, or any other relevant information.
- __Delete Album__: Permanently removes an album from the system.

These functionalities collectively provide band managers and team members with the tools necessary to effectively manage and track albums  and songs associated with the music band.

### Band Service

The Band Service provides functionality related to managing bands within the music band management system. It facilitates the creation, retrieval, updating, and deletion of bands along with associated operations.

__Functionality__:

- __Find Band by ID__: Retrieves details of a band by its unique identifier.
- __Find Bands by Manager__: Retrieves a list of bands associated with a specific manager. This endpoint is typically used by managers to view the bands they are managing.
- __Create Band__: Allows the creation of a new band within the system. This includes specifying details such as the band name, musical style, logo, etc.
- __Update Band__: Enables updating the details of an existing band. This may include modifying the band name, musical style, logo, or any other relevant information.
- __Delete Band__: Permanently removes a band from the system.

These functionalities collectively provide band managers and team members with the tools necessary to effectively manage and track bands within the music band management system.

### User Service

The User Service provides functionality related to managing users within the music band management system. It facilitates the creation, retrieval, updating, and deletion of user accounts along with associated operations.

__Functionality__:

- __Find User by ID__: Retrieves details of a user by their unique identifier.
- __Create User__: Allows the creation of a new user account within the system. This includes specifying details such as the username, email, password, etc.
- __Update User__: Enables updating the details of an existing user account. This may include modifying the username, email, password, or any other relevant information.
- __Delete User__: Permanently removes a user account from the system.
- __List User's Bands__: Retrieves a list of bands associated with a specific user. This endpoint is typically used by users to view the bands they are part of.
- __Create Activity__: Allows users to create activities within the system. This includes specifying details such as the activity type, date, location, etc.

These functionalities collectively provide users with the tools necessary to manage their accounts, view associated bands, and create activities within the music band management system.
## User Roles

### Band Managers
- Create and manage bands.
- Hire team members.
- Add albums, songs, and schedule activities.

### Team Members
- Browse offers made by band managers.
- Accept or reject offers to join a band.
- Access band activities and view profiles of other members.

## Installation and Setup

1. Clone the repository: `git clone https://gitlab.fi.muni.cz/xdedek1/band-manager.git`
2. Change the directory: `cd band-manager`
2. Install dependencies: `mvn clean install`
3. Start the server: `docker compose up --build`

Open-api specification is available at `http://localhost:8080/band-manager/{service-name}/api/v3/api-docs`

## Security
- go to root-url/login (i.e. http://localhost:8080/login). You will be provided with "MUNI Unified Login" buton. Click on it.
- You will be redirected to MUNI login page. Enter your credentials.
- After successful login, you will be redirected back to the application. You will see your user information, mainly *TOKEN* value.
- For next communication with application, you must provide Cookie header with value `JSESSIONID={TOKEN_VALUE}`. 
- Here is an example, how it looks in postman
- ![img_1.png](img_1.png)

- When users logs in for the first time, he is registered in the db. The User will contain first name, last name, email, and mainly id, which corresponds to the oauth subject identifier.
- For checking user in code, there are `Context` classes with only one method `getCurrentPrincipal()`. It returns `OAuth2IntrospectionAuthenticatedPrincipal`.
- By calling `OAuth2IntrospectionAuthenticatedPrincipal.getSubject()`, you will get users subject identifier. The subject identifier is unique to the user, and it should be the same as `User.id`. Example below:
- ```java
  public Activity update(String id, Activity toUpdate) {
        String oauthUserId = context.getCurrentPrincipal().getSubject();
        if (!toUpdate.getOwnerId().equals(oauthUserId)) {
            // Throw exception, that the activity does not belong to vurrent user, so he can not update it
        }
        // Continue...
  ```
  
## Showcase
Users can follow their favourite bands.
As bands follower, band manager can send you an invitation to certain activities, like 
concerts or autograph sessions.

Goal: Get all invitations for user's followed bands.

1. User can become a manager by calling `PUT /users/{userId}/mark-as-manager`. With manager role comes great responsibility!
2. Manager can create a band by calling `POST /bands`.
```json
{
    "name": "The Beatles",
    "description": "You know them, right?",
    "musicStyle": "ROCK"
}
```
3. Manager can create an album for the band by calling `POST /albums`.
```json
{
    "bandId": "band-id-of-band",
    "ownerId": "owner-id",
    "name": "Amazing album",
    "description": "This is really an amazing album!",
    "releaseDate": "2023-12-24",
    "songs": [
        {
            "title": "Happy song",
            "duration": 180,
            "genre": "ROCK"
        },
        {
            "title": "Sad song",
            "duration": 200,
            "genre": "POP"
        }]
}
```
4. Manager can create an activity for the band by calling `POST /activities`.
```json
{
    "bandId": "band-id-of-band",
    "name": "First album!",
    "description": "We just released out first album! Listen at https://youtu.be/dQw4w9WgXcQ",
    "startTime": "2024-01-01T12:00:00",
    "endTime": "2024-01-01T13:00:00"
}
```
5. User can follow the band by calling `POST /bands/{bandId}/follow`.
6. Manager can invite followers to the activity by calling `POST /activities/{activityId}/invitations`.
```json
{
    "text": "We are happy to invite you to our first album release!"
}
```
7. User can get all invitations by calling `GET /activities/invitations`.
## Use case diagram

<img alt="Service test diagram" src="./assets/band-manager-use-case-diagram.png" width="70%"/>

## Class diagram

<img alt="Service test diagram" src="./assets/band-manager-class-diagram.png" width="20%"/>


